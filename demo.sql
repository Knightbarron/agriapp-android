-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema rsmYpo6a4O
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema rsmYpo6a4O
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rsmYpo6a4O` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`specialist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`specialist` (
  `specialist_id` INT(11) NOT NULL AUTO_INCREMENT,
  `specialist_full_name` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `specialist_email` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `specialist_password` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `specialist_address` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `specialist_phone` VARCHAR(25) CHARACTER SET 'utf8' NOT NULL,
  `specialist_image_path` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `specialist_organization` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `specialist_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `specialist_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`specialist_id`),
  UNIQUE INDEX `specialist_phone` (`specialist_phone` ASC),
  UNIQUE INDEX `specialist_email` (`specialist_email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 57
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `mydb`.`specialization`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`specialization` (
  `specialization_id` INT NOT NULL AUTO_INCREMENT,
  `specialization_name` VARCHAR(45) NULL,
  `specialist_specialist_id` INT(11) NOT NULL,
  `specialization_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT '				',
  `specialization_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`specialization_id`),
  INDEX `fk_specialization_specialist_idx` (`specialist_specialist_id` ASC),
  CONSTRAINT `fk_specialization_specialist`
    FOREIGN KEY (`specialist_specialist_id`)
    REFERENCES `rsmYpo6a4O`.`specialist` (`specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`farmer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`farmer` (
  `farmer_id` INT(11) NOT NULL AUTO_INCREMENT,
  `farmer_full_name` VARCHAR(100) CHARACTER SET 'utf8' NOT NULL,
  `farmer_email` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `farmer_password` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `farmer_address` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `farmer_phone` VARCHAR(25) CHARACTER SET 'utf8' NOT NULL,
  `farmer_image_path` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `farmer_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `farmer_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`farmer_id`),
  UNIQUE INDEX `farmer_phone` (`farmer_phone` ASC),
  UNIQUE INDEX `farmer_email` (`farmer_email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 45
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`query`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`query` (
  `query_id` INT(11) NOT NULL AUTO_INCREMENT,
  `query_question` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `query_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `query_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `farmer_farmer_id` INT(11) NOT NULL,
  PRIMARY KEY (`query_id`),
  INDEX `fk_queries_farmer1_idx` (`farmer_farmer_id` ASC),
  CONSTRAINT `fk_queries_farmer1`
    FOREIGN KEY (`farmer_farmer_id`)
    REFERENCES `rsmYpo6a4O`.`farmer` (`farmer_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `mydb`.`query_specialist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`query_specialist` (
  `query_specialist_id` INT NOT NULL,
  `query_query_id` INT(11) NOT NULL,
  `specialist_specialist_id` INT(11) NOT NULL,
  `query_specialist_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `query_specialist_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`query_specialist_id`),
  INDEX `fk_query_specialist_query1_idx` (`query_query_id` ASC),
  INDEX `fk_query_specialist_specialist1_idx` (`specialist_specialist_id` ASC),
  CONSTRAINT `fk_query_specialist_query1`
    FOREIGN KEY (`query_query_id`)
    REFERENCES `rsmYpo6a4O`.`query` (`query_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_query_specialist_specialist1`
    FOREIGN KEY (`specialist_specialist_id`)
    REFERENCES `rsmYpo6a4O`.`specialist` (`specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

USE `rsmYpo6a4O` ;

-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`business`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`business` (
  `business_id` INT(11) NOT NULL AUTO_INCREMENT,
  `business_name` VARCHAR(100) CHARACTER SET 'utf8' NOT NULL,
  `business_email` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `business_password` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `business_address` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `business_phone` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `business_image_path` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `business_organization` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `business_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `business_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`business_id`),
  UNIQUE INDEX `business_phone` (`business_phone` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`ad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`ad` (
  `ad_id` INT(11) NOT NULL AUTO_INCREMENT,
  `ad_title` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ad_content` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ad_image_path` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `ad_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `ad_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `business_business_id` INT(11) NULL DEFAULT NULL,
  `farmer_farmer_id` INT(11) NULL DEFAULT NULL,
  `specialist_specialist_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ad_id`),
  INDEX `fk_ad_business1_idx` (`business_business_id` ASC),
  INDEX `fk_ad_farmer1_idx` (`farmer_farmer_id` ASC),
  INDEX `fk_ad_specialist1_idx` (`specialist_specialist_id` ASC),
  CONSTRAINT `fk_ad_business1`
    FOREIGN KEY (`business_business_id`)
    REFERENCES `rsmYpo6a4O`.`business` (`business_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ad_farmer1`
    FOREIGN KEY (`farmer_farmer_id`)
    REFERENCES `rsmYpo6a4O`.`farmer` (`farmer_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ad_specialist1`
    FOREIGN KEY (`specialist_specialist_id`)
    REFERENCES `rsmYpo6a4O`.`specialist` (`specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`package`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`package` (
  `package_id` INT(11) NOT NULL AUTO_INCREMENT,
  `package_title` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL,
  `package_details` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `package_price` DOUBLE NULL DEFAULT NULL,
  `package_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `package_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `specialist_specialist_id` INT(11) NOT NULL,
  PRIMARY KEY (`package_id`),
  INDEX `fk_package_specialist_idx` (`specialist_specialist_id` ASC),
  CONSTRAINT `fk_package_specialist`
    FOREIGN KEY (`specialist_specialist_id`)
    REFERENCES `rsmYpo6a4O`.`specialist` (`specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`booking` (
  `booking_id` INT(11) NOT NULL AUTO_INCREMENT,
  `booking_meeting_date` DATETIME NULL DEFAULT NULL,
  `booking_confirm_status` TINYINT(1) NULL DEFAULT NULL,
  `booking_price` DOUBLE NULL DEFAULT NULL,
  `booking_location` VARCHAR(100) CHARACTER SET 'utf8' NOT NULL,
  `booking_payment_status` TINYINT(1) NULL DEFAULT NULL,
  `booking_completion_status` TINYINT(1) NULL DEFAULT NULL,
  `booking_payment_transaction_id` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `farmer_farmer_id` INT(11) NOT NULL,
  `specialist_specialist_id` INT(11) NOT NULL,
  `booking_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `booking_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `package_package_id` INT(11) NOT NULL,
  `package_package_id1` INT(11) NOT NULL,
  PRIMARY KEY (`booking_id`),
  INDEX `fk_booking_farmer_idx` (`farmer_farmer_id` ASC),
  INDEX `fk_booking_specialist1_idx` (`specialist_specialist_id` ASC),
  INDEX `fk_booking_package` (`package_package_id` ASC),
  INDEX `fk_booking_package1_idx` (`package_package_id1` ASC),
  CONSTRAINT `fk_booking_farmer`
    FOREIGN KEY (`farmer_farmer_id`)
    REFERENCES `rsmYpo6a4O`.`farmer` (`farmer_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_package1`
    FOREIGN KEY (`package_package_id1`)
    REFERENCES `rsmYpo6a4O`.`package` (`package_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_booking_specialist1`
    FOREIGN KEY (`specialist_specialist_id`)
    REFERENCES `rsmYpo6a4O`.`specialist` (`specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`fcm_token`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`fcm_token` (
  `fcm_token_id` INT(11) NOT NULL AUTO_INCREMENT,
  `fcm_token_token` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `specialist_specialist_id` INT(11) NULL DEFAULT NULL,
  `farmer_farmer_id` INT(11) NULL DEFAULT NULL,
  `fcm_token_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `fcm_token_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`fcm_token_id`),
  UNIQUE INDEX `fcm_token_token` (`fcm_token_token` ASC),
  INDEX `fk_fcm_token_specialist_idx` (`specialist_specialist_id` ASC),
  INDEX `fk_fcm_token_farmer1_idx` (`farmer_farmer_id` ASC),
  CONSTRAINT `fk_fcm_token_farmer1`
    FOREIGN KEY (`farmer_farmer_id`)
    REFERENCES `rsmYpo6a4O`.`farmer` (`farmer_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_fcm_token_specialist`
    FOREIGN KEY (`specialist_specialist_id`)
    REFERENCES `rsmYpo6a4O`.`specialist` (`specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`feed`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`feed` (
  `feed_id` INT(11) NOT NULL AUTO_INCREMENT,
  `feed_title` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `feed_content` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `feed_image_path` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `feed_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `feed_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `farmer_farmer_id` INT(11) NULL DEFAULT NULL,
  `specialist_specialist_id` INT(11) NULL DEFAULT NULL,
  `business_business_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`feed_id`),
  INDEX `fk_feed_farmer_idx` (`farmer_farmer_id` ASC),
  INDEX `fk_feed_specialist1_idx` (`specialist_specialist_id` ASC),
  INDEX `fk_feed_business1_idx` (`business_business_id` ASC),
  CONSTRAINT `fk_feed_business1`
    FOREIGN KEY (`business_business_id`)
    REFERENCES `rsmYpo6a4O`.`business` (`business_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_feed_farmer`
    FOREIGN KEY (`farmer_farmer_id`)
    REFERENCES `rsmYpo6a4O`.`farmer` (`farmer_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_feed_specialist1`
    FOREIGN KEY (`specialist_specialist_id`)
    REFERENCES `rsmYpo6a4O`.`specialist` (`specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `rsmYpo6a4O`.`query_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rsmYpo6a4O`.`query_comment` (
  `query_comment_id` INT(11) NOT NULL AUTO_INCREMENT,
  `query_comment_content` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `farmer_farmer_id` INT(11) NULL DEFAULT NULL,
  `specialist_specialist_id` INT(11) NULL DEFAULT NULL,
  `query_comment_date_created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `query_comment_date_modified` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `query_specialist_query_specialist_id` INT NOT NULL,
  PRIMARY KEY (`query_comment_id`),
  INDEX `fk_query_comment_farmer_idx` (`farmer_farmer_id` ASC),
  INDEX `fk_query_comment_specialist1_idx` (`specialist_specialist_id` ASC),
  INDEX `fk_query_comment_query_specialist1_idx` (`query_specialist_query_specialist_id` ASC),
  CONSTRAINT `fk_query_comment_farmer`
    FOREIGN KEY (`farmer_farmer_id`)
    REFERENCES `rsmYpo6a4O`.`farmer` (`farmer_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_query_comment_specialist1`
    FOREIGN KEY (`specialist_specialist_id`)
    REFERENCES `rsmYpo6a4O`.`specialist` (`specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_query_comment_query_specialist1`
    FOREIGN KEY (`query_specialist_query_specialist_id`)
    REFERENCES `mydb`.`query_specialist` (`query_specialist_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
