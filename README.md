## AGRI APP

### API GUIDE

#### routes

#### link : /api/v1/registration/farmer

```
        method : POST
        data   :    farmer_full_name,
                    farmer_email,
                    farmer_password,
                    farmer_address,
                    farmer_phone
        response : status: 201 message : Successfully registered as Farmer
                 : status: 500 THIS IS A SERVER ERROR

```

#### link : /api/v1/registration/specialist

```
        method : POST
        data   :          specialist_full_name,
                          specialist_email,
                          specialist_password,
                          specialist_address,
                          specialist_phone
        response : status: 201 message : Successfully registered as Specialist
                 : status: 500 THIS IS A SERVER ERROR

```

#### link : /api/v1/login/farmer

```
        method : POST
        data   :    farmer_phone,
                    farmer_password,
        response : status: 204 message : No user found
                 : status: 500 THIS IS A SERVER ERROR
                 : status: 200 {message : "auth success", token, phone}
                 : status: 401 {message : "passwords do not match"}

```

#### link : /api/v1/login/specialist

```
        method : POST
        data   :    farmer_phone,
                    farmer_password,
        response    : status: 204 message : No user found
                    : status: 500 THIS IS A SERVER ERROR
                    :status: 200 {message : "auth success", token, phone}
                    :status: 401 {message : "passwords do not match"}

```

#### link : /api/v1/verify

```
        method : POST
        headerdata: Bearer token (accepts both specialist and farmer token)
        response    :status: 200 {phone,id,iat,exp,type}
                    :status: 401 {message : "Authentication Failed"}

```

#### link : /api/v1/verify/farmer

```
        method : POST
        headerdata: Bearer token (accepts farmer token only)
        response    :status: 200 {phone,id,iat,exp,type}
                    :status: 401 {message : "Farmer Authentication Failed"}

```

#### link : /api/v1/verify/specialist

```
        method : POST
        headerdata: Bearer token (accepts specialist token only)
        response    :status: 200 {phone,id,iat,exp,type}
                    :status: 401 {message : "Farmer Authentication Failed"}

```

#### link : /api/v1/otp/new/{:PHONE_NUMBER}

```
        method : POST
        response        :status: 200 {Status, Details}
                        : Status : "Success" && "Details" is your session id . Save it temporarily to verify otp
                    :status: 400 {Status, Details}
                        : "Status" : "Error" && "Details" is your error Details

```

#### link : /api/v1/otp/verify

```
        method  : POST
        data    : session_id (String)
                : otp (String)

        response:        :status: 200 {Status, Details}
                        : Status : "Success" && "Details" : "OTP VERIFIED"
                    :status: 400 {Status, Details}
                        : "Status" : "Error" && "Details" is your error Details

```
