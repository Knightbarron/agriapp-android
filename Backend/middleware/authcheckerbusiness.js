const jwt = require("jsonwebtoken");
const config = require("../constants/config");

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, config.JWT_KEY_BUSINESS);
    req.userData = decoded;
    next();
  } catch (error) {
    return res.status(401).json({
      message: "Business Authentication failed"
    });
  }
};
