package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;

import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;

import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.answers.ExpertAnswersFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.base.ExpertBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.expertCompleteProfile.ExpertCompleteProfile;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.expertCreatePost.ExpertCreatePostFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.ExpertHomeFeed;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.notifications.ExpertNotificationsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.profile.ExpertProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.uploadDocuments.ExpertUploadDocuments;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsActivity;
import com.jaeger.library.StatusBarUtil;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class HomeExpertActivity extends AppCompatActivity  implements HasSupportFragmentInjector, HomeExpertCallback{


    @Inject
    DispatchingAndroidInjector<android.support.v4.app.Fragment> dispatchingAndroidInjector;


    private static final String TAG = "HomeExpertActivity";

    @Inject
    HomeExpertViewModel viewModel;


    @Inject
    FragmentManager fm;

//    fragments

    @Inject
    ExpertCompleteProfile expertCompleteProfile;

    @Inject
    ExpertCreatePostFragment expertCreatePostFragment;

    @Inject
    ExpertUploadDocuments expertUploadDocuments;


    @Inject
    ExpertProfileFragment expertProfileFragment;

    @Inject
    ExpertNotificationsFragment expertNotificationsFragment;

    @Inject
    ExpertHomeFeed expertHomeFeed;

    @Inject
    ExpertBaseFragment expertBaseFragment;

    @Inject
    ExpertAnswersFragment expertAnswersFragment;

    Toolbar toolbar;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    initializeFragments(fm,expertBaseFragment);
                    setToolBar(R.drawable.agspert_logo_regular);
                    return true;
                case R.id.navigation_notifications:
                    initializeFragments(fm,expertHomeFeed);
                    setToolBar(R.drawable.feedpage_logo);
                    return true;
                case R.id.navigation_base:
                    initializeFragments(fm,expertNotificationsFragment);
                    setToolBar(R.drawable.notifications_logo);
                    return true;
                case R.id.navigation_my_answers:
                    initializeFragments(fm,expertAnswersFragment);


                    return true;
                case R.id.navigation_my_profile:
                    initializeFragments(fm,expertProfileFragment);
                    setToolBar(R.drawable.my_profile_logo);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);

        StatusBarUtil.setTransparent(this);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView navView = findViewById(R.id.nav_view);

        initializeFragments(fm,expertCompleteProfile);

        changeIconSize(navView);

        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setToolBar(R.drawable.agspert_logo_regular);
        goToSettings();



        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }
    public void goToSettings(){
        ImageView ic = findViewById(R.id.ig_icon);
        ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeExpertActivity.this, SettingsActivity.class);
                intent.putExtra("CustomerType","Expert");
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void setToolBar(int drawable){
        getSupportActionBar().setIcon(drawable);
        getSupportActionBar().setTitle("");
    }


    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    public void initializeFragments(FragmentManager fm, Fragment frag){
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, frag);
        ft.commit();
    }



    @Override
    public void onUploadSuccess() {
        Log.d(TAG, "onUploadSuccess: ");
       try {
           expertUploadDocuments.showErrorMessage("upload success");
           initializeFragments(fm,expertBaseFragment);

       }catch (Exception e){
           Log.e(TAG, "onUploadSuccess: " );
       }

    }

    @Override
    public void onUploadFailure() {
        Log.d(TAG, "onUploadFailure: ");
        try {
            expertUploadDocuments.showErrorMessage("upload failed");
        }catch (Exception e){
            Log.e(TAG, "onUploadFailure: " );
        }
    }

    private void changeIconSize(BottomNavigationView navView) {

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
        final View iconView = menuView.getChildAt(2).findViewById(android.support.design.R.id.icon);
        final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
        final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, displayMetrics);
        layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, displayMetrics);
        iconView.setLayoutParams(layoutParams);

    }


    @Override
    public void onFetchCurrentExpertSuccess(ExpertProfileResponse data) {
//        update fragment ui
        try {
            expertProfileFragment.updateProfileData(data);
        }catch (Exception e){
            Log.e(TAG, "onFetchCurrentExpertSuccess: " );
        }
    }

    @Override
    public void onFetchCurrentExpertFailure() {
//        update fragment ui
//        TODO remove this
       try{
           expertProfileFragment.notifyError("An error occured while fetching expert data");
       }catch (Exception e){
           Log.e(TAG, "onFetchCurrentExpertFailure: " );
       }

    }

    @Override
    public void onFetchCurrentExpertPackagesSuccess(ExpertPackageResponse data) {
            try {
                expertProfileFragment.updatePackageData(data);
            }catch (Exception e){
                Log.e(TAG, "onFetchCurrentExpertPackagesSuccess: " );
            }
    }

    @Override
    public void onFetchCurrentExpertPackagesFailure() {
//        TODO REMOVE THIS
      try {
          expertProfileFragment.notifyError("Error fetching expert packages");
      }catch (Exception e){
          Log.e(TAG, "onFetchCurrentExpertPackagesFailure: FRAGMENT MIGHT HAVE BEEN DESTROYED" );
      }
    }

    @Override
    public void onProfilePictureUpdateSuccess(DefaultResponse data) {


        try{
            if(!TextUtils.isEmpty(data.getImagePath())){
                expertProfileFragment.loadImageInCircleView(data.getImagePath());
            }
        }catch (Exception e){
            Log.e(TAG, "onProfilePictureUpdateFailure: FRAGMENT MIGHT HAVE BEEN DESTROYED." );
        }
    }

    @Override
    public void onProfilePictureUpdateFailure() {
            try{
                expertProfileFragment.notifyError("Error updating profile image");
            }catch (Exception e){
                Log.e(TAG, "onProfilePictureUpdateFailure: FRAGMENT MIGHT HAVE BEEN DESTROYED.");
            }
    }
}
