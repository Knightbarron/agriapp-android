package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.profile;


import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessViewModel;
import com.androidpopcorn.tenx.freshciboapp.utils.FileUtil;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessProfileFragment extends Fragment {


    @Inject
    HomeBusinessViewModel viewModel;

    public static final int RESULT_CODE_IMAGE = 1022;

    File imageFile = null;


    private static final String TAG = "BusinessProfileFragment";
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.agspert_certified)
    ImageView agspertCertified;
    @BindView(R.id.organic_certified)
    ImageView organicCertified;
    @BindView(R.id.profile_name)
    TextView profileName;
    @BindView(R.id.profile_specialization)
    TextView profileSpecialization;
    @BindView(R.id.profile_workplace)
    TextView profileWorkplace;
    @BindView(R.id.et_bio)
    TextView etBio;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.add_new_package)
    ImageView addNewPackage;
    Unbinder unbinder;

    String categoryType;
    String durationType;
    String selectedType;
    Boolean selectedPartnershipStatus;


    ArrayList<String> categoryData;
    ArrayList<String> packageName;

    int categoryID;
    HashMap<Integer, String> category;

    CircleImageView civDemo;
    @BindView(R.id.profile_specialization_tv)
    TextView profileSpecializationTv;
    @BindView(R.id.profile_workplace_tv)
    TextView profileOrganisation;

    String business_bio;
    String imagePath;


    @Inject
    public BusinessProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_profile, container, false);
        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");
        unbinder = ButterKnife.bind(this, view);

        categoryData = new ArrayList<>();
        category = new HashMap<>();
        packageName = new ArrayList<>();

        viewModel.businessPackages();
        viewModel.getProfileData();


        //TODO add the observer status logic

        observerForBusinessCategory();
        observerForProfilePicture();
        observeForBusinessPackages();
        observerForBusinessProfile();


        for (String s : packageName)
            Log.d(TAG, "onCreateView: " + s);

        addNewPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClicked();
            }
        });

        return view;
    }

    private void observerForBusinessProfile() {

        viewModel.getBusinessProfileData().observe(this, business -> {
                updateProfile(business);
            imagePath = business.getImage_path();
            Log.d(TAG, "onChanged: VIEWMODEL BIO:::::" + business.getBusiness_bio());
        });
    }

    public void updateProfile(BusinessProfileResponse.Business data) {

        if (!TextUtils.isEmpty(data.getName())) {
            profileName.setText(data.getName());
        }
        if (!TextUtils.isEmpty(data.getImage_path())) {
            loadImageInCircleView(data.getImage_path());
        }
        if (!TextUtils.isEmpty(data.getOrganization())){
            profileOrganisation.setText(data.getOrganization());
        }
        if (!TextUtils.isEmpty(data.getAddress())){
            profileSpecializationTv.setText(data.getAddress());
        }
        if (data.getBusinessCart().isEmpty()){
            Log.d(TAG, "updateProfile: Empty BCART");
            agspertCertified.setVisibility(View.INVISIBLE);

        }
        if (!data.getBusiness_bio().isEmpty()){
            etBio.setText(data.getBusiness_bio());
        }

    }
    public void loadImageInCircleView(String image_path) {
        Glide.with(getActivity()).load(ApiService.BASE_URL + "/" + image_path).into(profilePic);
    }


    private void observeForBusinessPackages() {

        viewModel.getBusinessPackages().observe(this, new Observer<ArrayList<BusinessPackageResponse.BusinessPackageResponseBody>>() {
            @Override
            public void onChanged(@Nullable ArrayList<BusinessPackageResponse.BusinessPackageResponseBody> businessPackageResponseBodies) {

                Log.d(TAG, "Size of the package is give here :::::::: " + businessPackageResponseBodies.size());

                for (BusinessPackageResponse.BusinessPackageResponseBody c : businessPackageResponseBodies) {
                    packageName.add(c.getName());

                    Log.d(TAG, "onChanged:PAckages : ::::::: " + c.getName());
                }
            }
        });
    }


    private void observerForProfilePicture() {

        viewModel.getStatusUploadPic().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    Toast.makeText(getActivity(), "Profile Picture updated", Toast.LENGTH_SHORT).show();
                } else {
                    //TODO add progressbar
                }
            }
        });
    }

    private void observerForBusinessCategory() {
        viewModel.getBusinessCategoryItems().observe(this, new Observer<ArrayList<BusinessCategoryResponse.CategoryItem>>() {
            @Override
            public void onChanged(@Nullable ArrayList<BusinessCategoryResponse.CategoryItem> categoryItems) {

                for (BusinessCategoryResponse.CategoryItem c : categoryItems) {
                    categoryData.add(c.getName());
                    category.put(c.getCategory_id(), c.getName());
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @OnClick(R.id.profile_pic)
    public void showUpdateProfilePicDialog() {

        Log.d(TAG, "showUpdateProfilePicDialog: Button Clicked");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = this.getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.custom_dialog_update_profileimage, null);
        builder.setView(dialogView);

        Button btnUpload = (Button) dialogView.findViewById(R.id.btn_upload);
        civDemo = dialogView.findViewById(R.id.civ_profile_demo);

        Glide.with(getActivity()).load(ApiService.BASE_URL + "/" + imagePath).into(civDemo);


        Button btnselectphoto = (Button) dialogView.findViewById(R.id.btn_select_photo);
        btnselectphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchImage();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageFile == null) {
                    Toast.makeText(getActivity(), "Select a image first", Toast.LENGTH_SHORT).show();
                    return;
                }
                viewModel.uploadProfilePicture(imageFile);
                builder.create().dismiss();
            }
        });


        builder.create().show();


    }

    private void searchImage() {
        Dexter.withActivity(getActivity()).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select a photo"), RESULT_CODE_IMAGE);

            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(getActivity(), "Enable Storage read permission", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }).check();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void onButtonClicked() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_business_package, null);
        dialogBuilder.setView(dialogView);


        EditText businessProduct = dialogView.findViewById(R.id.add_a_product);
        TextView categoryTv = dialogView.findViewById(R.id.category_tv);
        Spinner spinnerCategory = dialogView.findViewById(R.id.spinner_category);
        EditText productDescription = dialogView.findViewById(R.id.produce_description);
        EditText priceEt = dialogView.findViewById(R.id.et_price);
        TextView durationTv = dialogView.findViewById(R.id.duration_tv);
        Spinner spinnerDuration = dialogView.findViewById(R.id.spinner_duration);
        ImageView iv1 = dialogView.findViewById(R.id.iv1);
        ImageView iv2 = dialogView.findViewById(R.id.iv2);
        ImageView iv3 = dialogView.findViewById(R.id.iv3);
        RadioGroup radioGroup = dialogView.findViewById(R.id.radioGroup);
        Button addPackage = dialogView.findViewById(R.id.btn_save);
        RadioGroup radioGroup2 = dialogView.findViewById(R.id.radioGroup2);


        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, categoryData);

        Log.d(TAG, "onButtonClicked: " + categoryData);

        spinnerCategory.setAdapter(adapter);

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                categoryType = spinnerCategory.getSelectedItem().toString();

                Log.d(TAG, "initSpinners: Qualification : " + categoryType);

                categoryTv.setText(categoryType);

                for (Map.Entry m : category.entrySet()) {
                    System.out.println(m.getKey() + " " + m.getValue());
                    if (m.getValue().equals(categoryType)) {
                        categoryID = (int) m.getKey();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String[] durationData = new String[]{"month", "Year", "Day"};
        ArrayAdapter adapter1 = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, durationData);
        spinnerDuration.setAdapter(adapter1);

        spinnerDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                durationType = spinnerDuration.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Qualification : " + durationType);
                durationTv.setText(durationType);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //TODO image part

        AlertDialog b = dialogBuilder.create();


        addPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedProductCat = radioGroup.getCheckedRadioButtonId();
                RadioButton productType = dialogView.findViewById(selectedProductCat);
                if (productType.getText().toString().toLowerCase().equals("product")) {
                    selectedType = "Product";
                } else {
                    selectedType = "Service";
                }

                int selectedPartnership = radioGroup2.getCheckedRadioButtonId();
                RadioButton partnerShip = dialogView.findViewById(selectedPartnership);
                if (partnerShip.getText().toString().toLowerCase().equals("yes")) {
                    selectedPartnershipStatus = true;
                } else {
                    selectedPartnershipStatus = false;
                }


                String product = businessProduct.getText().toString();
                String description = productDescription.getText().toString();
                String price = priceEt.getText().toString();
                price = price + " " + durationType;


                Log.d(TAG, "onClick: Selected  Type : " + selectedType + " Product name : " + product + " Description : " + description + "Price : " + price +
                        " Category type: " + categoryType
                        + " Partnership status : " + selectedPartnershipStatus);

                Log.d(TAG, "onClick: Product tye" + selectedType);


                viewModel.saveBusinessPackageDetails(new BusinessPackageBody(selectedType, product, description, selectedPartnershipStatus, price, categoryID));
                b.dismiss();
            }
        });

        b.show();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == RESULT_CODE_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                imageFile = getFile(uri);

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                    if (civDemo != null) {
                        civDemo.setImageBitmap(bitmap);
                    }
                } catch (FileNotFoundException e) {
                    throw new Error("File not found");
                } catch (IOException e) {
                    throw new Error("IO exception");
                }
            }
        }

    }

    public File getFile(Uri uri) {
        try {
            File file = FileUtil.from(getActivity(), uri);
            Log.d("file", "getFile: uri : " + file.getPath() + " file - " + file + " : " + file.exists());

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @OnClick(R.id.et_bio)
    public void onClickBio() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_bio, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        edt.setSingleLine(false);

        dialogBuilder.setTitle("Write something about your organization");

        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                business_bio = edt.getText().toString();
                Log.d(TAG, "onClick: " + business_bio);

                //TODO Go for api call
                viewModel.patchBusinessProfile(new BusinessProfileBody(business_bio));
                //observerForBusinessProfile();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();


    }


}
