package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.profile.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.base.adapter.FarmerCategoryData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ProduceViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";

    List<FarmerProduceResponse.FarmerProducePackBody> mList;

    @Inject
    public RecyclerViewAdapter() {
        mList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ProduceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem_farmer_produce,viewGroup,false);
        return  new ProduceViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ProduceViewHolder produceViewHolder, int i) {

        Log.d(TAG, "onBindViewHolder: " + mList.get(i).getFarmer_produce_name());
        produceViewHolder.name.setText(mList.get(i).getFarmer_produce_name());
        produceViewHolder.quantity.setText(mList.get(i).getFarmer_produce_quantity());
        produceViewHolder.details.setText(mList.get(i).getFarmer_produce_details());
        produceViewHolder.harvesting.setText(mList.get(i).getFarmer_produce_harvest_time());
        produceViewHolder.land.setText(mList.get(i).getFarmer_produce_land_area());
        produceViewHolder.price.setText(mList.get(i).getFarmer_produce_expected_price());
        produceViewHolder.tv1.setText("Produce Name : ");
        produceViewHolder.tv2.setText("Produce Quantity : ");
        produceViewHolder.tv3.setText("Produce Details : ");
        produceViewHolder.tv4.setText("Harvesting time : ");
        produceViewHolder.tv5.setText("Land Area : ");
        produceViewHolder.tv6.setText("Expected Time : ");



    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void updateListItems(List<FarmerProduceResponse.FarmerProducePackBody> data) {
        mList.clear();
        this.mList.addAll(data);
        notifyDataSetChanged();

    }

    public class ProduceViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        TextView name,quantity,details,harvesting,land,price,tv1,tv2,tv3,tv4,tv5,tv6;
        ImageView textViewOptions;
        CardView cardView;

        public ProduceViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.product_name);
            quantity = itemView.findViewById(R.id.product_quantity);
            details = itemView.findViewById(R.id.product_details);
            harvesting = itemView.findViewById(R.id.product_harvesting);
            land = itemView.findViewById(R.id.product_land);
            price = itemView.findViewById(R.id.product_price);
            tv1= itemView.findViewById(R.id.tv1);
            tv2= itemView.findViewById(R.id.tv2);
            tv3= itemView.findViewById(R.id.tv3);
            tv4= itemView.findViewById(R.id.tv4);
            tv5= itemView.findViewById(R.id.tv5);
            tv6= itemView.findViewById(R.id.tv6);
            textViewOptions =itemView.findViewById(R.id.textViewOptions);
            cardView = itemView.findViewById(R.id.card_view);

            textViewOptions.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.add(this.getAdapterPosition(),121,0,"Edit this package");
           // menu.add(this.getAdapterPosition(),122,1,"Delete this package");
        }
    }
}
