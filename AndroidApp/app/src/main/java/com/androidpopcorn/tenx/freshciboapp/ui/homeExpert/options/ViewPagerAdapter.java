package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {


    private static final String TAG = "ViewPagerAdapter";

    private List<Fragment> fragList;



    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        Log.d(TAG, "ViewPagerAdapter: ViewPager Called");
        fragList = new ArrayList<>();
        fragList.add(new ExpertToCreatePostFragment());
        fragList.add(new ExpertToAddNewService());
        fragList.add(new ExpertToCollaborateFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return fragList.get(position);
    }

    @Override
    public int getCount() {

        Log.d(TAG, "getCount: size frags : "+fragList.size());
        return fragList.size(); //three fragments
    }
}