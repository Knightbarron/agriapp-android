package com.androidpopcorn.tenx.freshciboapp.ui.main;


import android.app.Application;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;
import com.androidpopcorn.tenx.freshciboapp.base.ViewModelFactory;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityScope;
import com.androidpopcorn.tenx.freshciboapp.ui.main.callbacks.AuthStateHandler;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {


    @Provides
    @ActivityContext
    @ActivityScope
    Context provideContext(MainActivity mainActivity){
        return mainActivity;
    }


    @Provides
    @ActivityScope
    MainViewModel provideVM(@ActivityContext Context context, Application application, AppDataManager appDataManager){

        MainViewModel vm = new MainViewModel(appDataManager, application, (AuthStateHandler) context);
        ViewModelFactory<MainViewModel> factory = new ViewModelFactory<>(vm,appDataManager,application);
        return ViewModelProviders.of((MainActivity) context, factory).get(MainViewModel.class);
    }



}
