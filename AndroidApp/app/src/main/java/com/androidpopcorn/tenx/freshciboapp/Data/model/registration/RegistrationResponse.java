package com.androidpopcorn.tenx.freshciboapp.Data.model.registration;

import com.google.gson.annotations.SerializedName;

public class RegistrationResponse {
    @SerializedName("content")
    private String message;


    public RegistrationResponse() {
    }

    public RegistrationResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
