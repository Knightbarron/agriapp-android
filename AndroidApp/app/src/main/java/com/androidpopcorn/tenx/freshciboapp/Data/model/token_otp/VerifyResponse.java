package com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp;

import com.google.gson.annotations.SerializedName;

public class VerifyResponse {

    @SerializedName("phone")
    private String phone ;


    @SerializedName("type")
    private String type;



    @SerializedName("id")
    private String id;


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
