package com.androidpopcorn.tenx.freshciboapp.ui.main;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;


import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;

import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.login.LoginResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.OtpResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyBusiness;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyExpert;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyFarmer;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.TokenResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.VerifyResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.verifyUser.UserObject;
import com.androidpopcorn.tenx.freshciboapp.base.BaseViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.main.callbacks.AuthStateHandler;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class MainViewModel extends BaseViewModel {


    private static final String TAG = "MainViewModel";

    private AuthStateHandler authStateHandler;

    private AppDataManager appDataManager;

    //For storing the session Id for otp request
    private MutableLiveData<String> session_id;

    //For verification of OTP
    private MutableLiveData<Boolean> isVerified;

    //For knowing the user type
    private MutableLiveData<String> userType;
    private MutableLiveData<String> userPhone;

    //For knowing the phone no status
    private MutableLiveData<Boolean> isSend;

    //For getting the authToken
    private MutableLiveData<String> tokenCode;

    //FOR getting the userType from Token
    private MutableLiveData<String> verifyUserType;

    public LiveData<String> getVerifyUserType(){
        if (verifyUserType==null){
            verifyUserType = new MutableLiveData<>();
        }
        return verifyUserType;
    }

    @Inject
    public MainViewModel(AppDataManager appDataManager, Application application, AuthStateHandler authStateHandler) {
        super(appDataManager, application);
        this.appDataManager = appDataManager;
        this.authStateHandler = authStateHandler;
    }

    public String getUserType(){
        if(userType != null){

            Log.d(TAG, "getUserType: " + userType.getValue());
            return userType.getValue();

        }
        return "";
    }
    public LiveData<Boolean> getIsSend(){
        if(isSend == null){
            isSend = new MutableLiveData<>();
        }
        return isSend;
    }
    public LiveData<String> getTokenCode(){
        if(tokenCode==null){
            tokenCode = new MutableLiveData<>();
        }
        return tokenCode;
    }
    public String getSession_id() {
        if(session_id != null){
            Log.d(TAG, "getSession_id: "+session_id.getValue());
            return session_id.getValue();
        }
        return "";
    }
    public LiveData<Boolean> getVerifiedStatus(){

        if(isVerified==null){
            isVerified = new MutableLiveData<>();
        }
        return isVerified;
    }
    //Attempt Registration Farmer
    public void doRegistrationFarmer(RegistrationBodyFarmer registrationBodyFarmer){

        appDataManager.farmerRegister(registrationBodyFarmer).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                        new Observer<Response<RegistrationResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<RegistrationResponse> registrationResponseResponse) {

                if(registrationResponseResponse.code() == 201){
                    attemptLoginFarmer(registrationBodyFarmer.getPhone(),registrationBodyFarmer.getPassword());
                    authStateHandler.onRegistrationResult(true);
                }else{
                    Log.e(TAG, "onNext: Error occured in Response " +
                            registrationResponseResponse.code());
                    authStateHandler.onRegistrationResult(false);
                    
                        // error case
                        switch (registrationResponseResponse.code()) {
                            case 404:
                                Log.d(TAG, "onNext: Not Found");
                                break;
                            case 500:
                                Log.d(TAG, "onNext: Internal server Error");
                                break;
                            default:
                                Log.d(TAG, "onNext: Unknown error");
                                break;
                    }

                }

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: RegisterViewModel ", e);

            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: RegisterViewModel : ");
            }
        });
    }

    //Attempt Registration Business
    public void doRegistrationBusiness(RegistrationBodyBusiness bodyBusiness){

        appDataManager.businessRegister(bodyBusiness).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                new Observer<Response<RegistrationResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<RegistrationResponse> registrationResponseResponse) {

                        if(registrationResponseResponse.code() == 201){
                            attemptLoginBusiness(bodyBusiness.getPhone(),bodyBusiness.getPassword());
                            authStateHandler.onRegistrationResult(true);
                        }else{
                            Log.e(TAG, "onNext: Error occured in Response " +
                                    registrationResponseResponse.code());
                            authStateHandler.onRegistrationResult(false);

                            // error case
                            switch (registrationResponseResponse.code()) {
                                case 404:
                                    Log.d(TAG, "onNext: Not Found");
                                    break;
                                case 500:
                                    Log.d(TAG, "onNext: Internal server Error");
                                    break;
                                default:
                                    Log.d(TAG, "onNext: Unknown error");
                                    break;
                            }

                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: RegisterViewModel ", e);

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: RegisterViewModel : ");
                    }
                });
    }
    //Attempt Registration Specialist
    public void doRegistrationSpecialist(RegistrationBodyExpert registrationBodyExpert){

        appDataManager.specialistRegister(registrationBodyExpert).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                        new Observer<Response<RegistrationResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<RegistrationResponse> registrationResponseResponse) {

                if(registrationResponseResponse.code() == 201){
                    authStateHandler.onRegistrationResult(true);
                    attemptLoginExpert(registrationBodyExpert.getPhone(),registrationBodyExpert.getPassword());


                }else{
                    Log.d(TAG, "onNext: Error occured . Code" +
                            registrationResponseResponse.code() +
                            registrationResponseResponse.message());
                    authStateHandler.onRegistrationResult(false);
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: RegisterViewModel ", e);

            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: RegisterViewModel : " );
            }
        });
    }

    //Attempt login Farmer
    public void attemptLoginFarmer(final String phone, String password){
        Log.d(TAG, "attemptLogin: Attempting login");

        Observable<Response<LoginResponse>> observable = appDataManager.loginFarmer(
                phone,password);

        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LoginResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<LoginResponse> loginResponseResponse) {
                        if(loginResponseResponse.code()==200){
                            try{
                                appDataManager.setAccessToken(loginResponseResponse.body().getToken());
                                appDataManager.setEmail(phone);

                                Log.d(TAG, "onNext: Logged In");
                                authStateHandler.onAuthStateChanged(true);
                            }catch (NullPointerException e){
                                Log.e(TAG, "onNext: null token", e);
                            }
                        }
                        else{
                            Log.d(TAG, "onNext: response code error" + loginResponseResponse.code());
                            authStateHandler.onAuthStateChanged(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Error fetching data",e );
                        authStateHandler.onAuthStateChanged(false);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete:");
                    }
                });
    }


    //Attempt login Business
    public void attemptLoginBusiness(final String phone, String password){
        Log.d(TAG, "attemptLogin: Attempting login");

        Observable<Response<LoginResponse>> observable = appDataManager.loginBusiness(
                phone,password);

        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LoginResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<LoginResponse> loginResponseResponse) {
                        if(loginResponseResponse.code()==200){
                            try{
                                appDataManager.setAccessToken(loginResponseResponse.body().getToken());
                                appDataManager.setEmail(phone);
                                Log.d(TAG, "onNext: Logged In");
                                authStateHandler.onAuthStateChanged(true);
                            }catch (NullPointerException e){
                                Log.e(TAG, "onNext: null token", e);
                            }
                        }
                        else{
                            Log.d(TAG, "onNext: response code error" + loginResponseResponse.code());
                            authStateHandler.onAuthStateChanged(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Error fetching data",e );
                        authStateHandler.onAuthStateChanged(false);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete:");
                    }
                });
    }

    //Attempt login Expert
    public void attemptLoginExpert(final String phone, String password){
        Log.d(TAG, "attemptLogin: Attempting login");

        Observable<Response<LoginResponse>> observable = appDataManager.loginExpert(
                phone,password);

        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LoginResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<LoginResponse> loginResponseResponse) {
                        if(loginResponseResponse.code()==200){
                            try{
                                appDataManager.setAccessToken(loginResponseResponse.body().getToken());
                                appDataManager.setEmail(phone);
                                Log.d(TAG, "onNext: Logged in Expert");
                                authStateHandler.onAuthStateChanged(true);
                            }catch (NullPointerException e){
                                Log.e(TAG, "onNext: null token", e);
                            }
                        }
                        else{
                            Log.d(TAG, "onNext: response code error" + loginResponseResponse.code());
                            authStateHandler.onAuthStateChanged(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: Error fetching data",e );
                        authStateHandler.onAuthStateChanged(false);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete:");
                    }
                });
    }

    //Send Phone no to Server for getting OTP
    public void sendOtpRequest(String phone){

        Log.d(TAG, "sendOtpRequest: Method for sending the phone no to server");

        Observable<Response<OtpResponse>> observable = appDataManager.sendOtpRequest(phone);

        observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<OtpResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<OtpResponse> otpResponseResponse) {

                        if(otpResponseResponse.code()==200){

                            if(session_id == null){
                                session_id = new MutableLiveData<>();
                            }

                            try{

                                isSend.setValue(true);

                                String s =  otpResponseResponse.body().getDetails();

                                session_id.setValue(s);
                                Log.d(TAG, "onNext: id ==== "+session_id.getValue());

                                Log.d(TAG, "onNext: " + session_id);
                                Log.d(TAG, "onNext: Details: " + otpResponseResponse.body().getDetails());


                            }catch (NullPointerException e){
                                Log.d(TAG, "onNext: null token",e);
                            }

                        }
                        else {

                            isSend.setValue(false);
                            Log.d(TAG, "onNext: Response error code  : " + otpResponseResponse.code());
                            //Log.d(TAG, "onNext: Response error Details :" + otpResponseResponse.body().getDetails());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: Error fetching Data", e);
                        isSend.setValue(false);

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Phone no is send to the server");
                    }
                });
    }

    //Verify the OTP
    public void verifyOtp(String session_id,String phone){

        Observable<Response<OtpResponse>> observable = appDataManager.
                verifyOtp(session_id,phone);

        observable.subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()).subscribe(
                        new Observer<Response<OtpResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<OtpResponse> otpResponseResponse) {
                if(otpResponseResponse.code()==200){
                    Log.d(TAG, "onNext: Verification Successful");
                    isVerified.setValue(true);

                }else{
                    Log.d(TAG, "onNext: Can not be Verified");
                    isVerified.setValue(false);
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: ",e);
                isVerified.setValue(false);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Otp Verified");
            }
        });

    }



    //Validating the token code
    public void validateToken(){
            appDataManager.verifyAuth().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<VerifyResponse>>() {
                @Override
                public void onSubscribe(Disposable d) {
                    getCompositeDisposable().add(d);
                }

                @Override
                public void onNext(Response<VerifyResponse> verifyResponseResponse) {

                    Log.d(TAG, "onNext: Response code token verify : "+verifyResponseResponse.code());


                    if(verifyResponseResponse.code()==200){
                        tokenCode.setValue("200");
                        Log.d(TAG, "onNext: " + verifyResponseResponse.body().getPhone());

                    }else{
                        tokenCode.setValue("401");
                    }

                }

                @Override
                public void onError(Throwable e) {
                    Log.e(TAG, "onError: ",e );
                }

                @Override
                public void onComplete() {
                    Log.d(TAG, "onComplete: Verify Token OnCompleted reached");
                }
            });
    }


    //Save the usertype
    public void saveUserType(String userData){
        userType = new MediatorLiveData<>();
        userType.setValue(userData);
    }

    public void saveUserPhone(String phone) {
        if(userPhone == null){
            userPhone = new MutableLiveData<>();
        }

        userPhone.setValue(phone);
    }


    public String getUserPhone(){
        if(userPhone == null){
            userPhone = new MutableLiveData<>();
        }

        return userPhone.getValue();



    }


    public void saveFcmToken(String token){
        appDataManager.saveToken(token).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<TokenResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<TokenResponse> verifyResponseResponse) {

                if(verifyResponseResponse.code() == 201 || verifyResponseResponse.code() ==200){
                    Log.d(TAG, "onNext: Fcm Token saved successfully to database");
                }else {
                    Log.d(TAG, "onNext: Error saving token to database");
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: error saving token ",e );
            }

            @Override
            public void onComplete() {

            }
        });
    }


    public void getUserTypeFromToken(){
        appDataManager.verifyUserType().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<UserObject>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<UserObject> userObjectResponse) {

                if (userObjectResponse.code()==200){
                    try{

                        if (verifyUserType==null){
                            verifyUserType = new MutableLiveData<>();
                        }
                        verifyUserType.setValue(userObjectResponse.body().getUser().getType());

                    }catch (NullPointerException e){
                        Log.d(TAG, "onNext: NULL POINTER EXCEPTION>>>>>>" );
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: " , e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Of the userType Token");
            }
        });
    }



}
