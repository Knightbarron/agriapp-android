package com.androidpopcorn.tenx.freshciboapp.di.modules;


import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityScope;

import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessFragmentBuilder;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessModule;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeFragmentBuilder;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeModule;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.FarmerFragmentBuilder;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerModule;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainFragmentBuilder;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainModule;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainModuleExtra;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsFragmentBuilder;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = {MainModule.class,
            MainFragmentBuilder.class,
            MainModuleExtra.class})
    @ActivityScope
    abstract MainActivity bindMainActivity();


    @ContributesAndroidInjector(modules = {HomeModule.class,
            HomeFragmentBuilder.class,
            MainModuleExtra.class})
    @ActivityScope
    abstract HomeExpertActivity bindHomeExpertActivity();

    @ContributesAndroidInjector(modules = {HomeFarmerModule.class, FarmerFragmentBuilder.class, MainModuleExtra.class})
    @ActivityScope
    abstract HomeFarmerActivity bindHomeFarmerActivity();

    @ContributesAndroidInjector(modules = {
            HomeBusinessModule.class,
            HomeBusinessFragmentBuilder.class,
            MainModuleExtra.class
    })
    @ActivityScope
    abstract HomeBusinessActivity bindHomeBusinessActivity();

    @ContributesAndroidInjector(modules = {
            SettingsFragmentBuilder.class,
            MainModuleExtra.class,
            SettingsModule.class
    })
    @ActivityScope
    abstract SettingsActivity bindSettingsActivity();

}
