package com.androidpopcorn.tenx.freshciboapp.ui.main;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.main.callbacks.AuthStateHandler;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCompleteProfile.FarmerCompleteProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.login.LoginFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.regis.RegisFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.selection.SelectionFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.welcome.WelcomeFragment;
import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector, AuthStateHandler {


    private static final String TAG = "MainActivity2";

    @Inject
    DispatchingAndroidInjector<android.support.v4.app.Fragment> dispatchingAndroidInjector;

//  data
    @Inject
    MainViewModel viewModel;


//    views
    @Inject
    LoginFragment loginFragment;

    @Inject
    RegisFragment regisFragment;

    @Inject
    WelcomeFragment welcomeFragment;

    @Inject
    FragmentManager fm;

    @Override
    public void onRegistrationResult(Boolean isRegistered) {
// this will be called when user gets a response after trying to register
        if(isRegistered){
            Log.d(TAG, "onRegistrationResult: Registered");
            Toast.makeText(this, "Registered", Toast.LENGTH_SHORT).show();
            String userType = viewModel.getUserType();
//          success -->   user is in register fragment , redirect to login fragment
            if(userType.equals("farmer")){
                goToFarmerActivity();
            }else if(userType.equals("expert")){
                goToSpecialistActivity();
            }else if(userType.equals("business")){

                goToBusinessActivity();
            }
        }else {
            Toast.makeText(this, "NOT Registered", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onRegistrationResult: Not Registered");
//            some error occured --> show a content in registration fragment

        }
    }

    public void goToFarmerActivity(){
        Intent intent = new Intent(MainActivity.this, HomeFarmerActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToBusinessActivity(){
        Intent intent = new Intent(MainActivity.this, HomeBusinessActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToSpecialistActivity(){
        Intent intent = new Intent(MainActivity.this, HomeExpertActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onAuthStateChanged(Boolean isAuthenticated) {

//        this callback will be called when the auth state of the user changes during login
        if(isAuthenticated){
//            save fcmtoken to database
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(res -> {
                viewModel.saveFcmToken(res.getToken());

            }).addOnFailureListener(err -> {
                Log.d(TAG, "onAuthStateChanged: Failed to fetch token");
            });


//            success --> send to home fragment
            Toast.makeText(this, "Logging in", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onAuthStateChanged: Authenticated . GO to login Fragment");

            String userType = viewModel.getUserType();
            if(userType.equals("farmer")){
                goToFarmerActivity();
            }else if(userType.equals("expert")){
                goToSpecialistActivity();
            }else if(userType.equals("business")){
                goToBusinessActivity();
            }
                finish();

            }else {
//            fail -- send to login fragment
            Log.d(TAG, "onAuthStateChanged: Not authenticated. ");
            Toast.makeText(this, "Please check your phone number and password or network connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }


//    check if authenticated while starting this activity and show login or home accordingly
    @Override
    protected void onStart() {
        super.onStart();
        viewModel.validateToken();
        if (viewModel == null){
            Log.d(TAG, "onStart: VIEWMODEL IS NULL" );
        }else{
            Log.d(TAG, "onStart: VIEWMODEL IS NOT NULL");
        }
        
        viewModel.getUserTypeFromToken();
        observerForUserTypeToken();

    }

    private void observerForUserTypeToken() {
        viewModel.getVerifyUserType().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Log.d(TAG, "onChanged: USER TYPE  : "  + s);
                if(s.equals("farmer")){
                    goToFarmerActivity();
                }else if(s.equals("specialist")){
                    goToSpecialistActivity();
                }else if(s.equals("business")){
                    goToBusinessActivity();
                }
            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);


        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {

            getWindow().setStatusBarColor(Color.TRANSPARENT);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initializeFragments(fm,welcomeFragment);

        subscribeObservers();


    }

    private void subscribeObservers() {

        viewModel.getTokenCode().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                if(s.equals("200")){
//                    Toast.makeText(MainActivity.this, "Sending to home page",
//                            Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onChanged: 200 Code .. Jumping to home page");
                }
                else if(s.equals("401")){
//                    Toast.makeText(MainActivity.this, "Sending to Log in Page",
//                            Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onChanged: 401 Code... Jumping to login page");
                }

            }
        });

    }


    public void initializeFragments(FragmentManager fm, Fragment frag){
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_main, frag);
        ft.commit();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {

        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }



}
