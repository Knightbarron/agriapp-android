package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert;


import android.app.Application;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.base.ViewModelFactory;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {


    @Provides
    @ActivityContext
    @ActivityScope
    Context provideContext(HomeExpertActivity homeActivity){
        return homeActivity;
    }


    @Provides
    @ActivityScope
    HomeExpertViewModel provideVM(@ActivityContext Context context, Application application, AppDataManager appDataManager){

        HomeExpertViewModel vm = new HomeExpertViewModel(appDataManager, application,(HomeExpertCallback) context);
        ViewModelFactory<HomeExpertViewModel> factory = new ViewModelFactory<>(vm,appDataManager,application);
        return ViewModelProviders.of((HomeExpertActivity) context, factory).get(HomeExpertViewModel.class);

    }


}
