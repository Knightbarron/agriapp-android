package com.androidpopcorn.tenx.freshciboapp.Data.model.login;

public class LoginResponse {

    private String code;

    private String message;

    private String token;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "response : "+message + " code : "+code +" token : "+token;
    }
}
