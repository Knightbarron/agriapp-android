package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.answers;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.answers.adapters.QueryAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertAnswersFragment extends Fragment {

    private static final String TAG = "ExpertAnswersFragment";
    @Inject
    HomeExpertViewModel viewModel;

//    views
    @BindView(R.id.recycler_query)
    RecyclerView recyclerQuery;
    Unbinder unbinder;


    @Inject
    QueryAdapter adapter;

    @Inject
    public ExpertAnswersFragment() {
        // Required empty public constructor
    }

    public void setUpRecucler(Context mCtx){
        recyclerQuery.setLayoutManager(new LinearLayoutManager(mCtx));
        recyclerQuery.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expert_answers, container, false);

        unbinder = ButterKnife.bind(this, view);

        setUpRecucler(getActivity());


        viewModel.loadQueriesForSpecialists();
        viewModel.getOnGetSpecialistQueries().observe(getActivity(), res -> {
            Log.d(TAG, "onCreateView: on change called");
            adapter.updateList(res);
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
