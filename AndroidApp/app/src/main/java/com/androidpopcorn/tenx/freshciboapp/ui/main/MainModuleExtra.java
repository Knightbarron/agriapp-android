package com.androidpopcorn.tenx.freshciboapp.ui.main;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityScope;
import com.androidpopcorn.tenx.freshciboapp.ui.main.callbacks.AuthStateHandler;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModuleExtra {

    @Provides
    @ActivityScope
    FragmentManager provideFragmentManager(@ActivityContext Context context){
        return ((AppCompatActivity)context).getSupportFragmentManager();
    }


}
