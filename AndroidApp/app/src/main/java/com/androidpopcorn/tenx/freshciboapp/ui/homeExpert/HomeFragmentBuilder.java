package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert;


import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.answers.ExpertAnswersFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.base.ExpertBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.expertCompleteProfile.ExpertCompleteProfile;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.expertCreatePost.ExpertCreatePostFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.ExpertHomeFeed;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.notifications.ExpertNotificationsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options.ExpertToAddNewService;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options.ExpertToCollaborateFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options.ExpertToCreatePostFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.profile.ExpertProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.uploadDocuments.ExpertUploadDocuments;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HomeFragmentBuilder {


    @ContributesAndroidInjector(modules = {})
    abstract ExpertAnswersFragment bindExpertAnswersFragment();

    @ContributesAndroidInjector(modules = {})
    abstract ExpertBaseFragment bindExpertBaseFragment();

    @ContributesAndroidInjector(modules = {})
    abstract ExpertHomeFeed bindExpertHomeFragment();

    @ContributesAndroidInjector(modules = {})
    abstract ExpertNotificationsFragment bindExpertNotificationsFragment();

    @ContributesAndroidInjector(modules = {})
    abstract ExpertProfileFragment bindExpertProfileFragment();

    @ContributesAndroidInjector(modules = {})
    abstract ExpertCreatePostFragment bindExpertCreatePostFragment();

    @ContributesAndroidInjector(modules = {})
    abstract ExpertCompleteProfile bindExpertCompleteProfile();

    @ContributesAndroidInjector(modules = {})
    abstract ExpertUploadDocuments bindExpertUploadDocuments();

    @ContributesAndroidInjector
    abstract ExpertToAddNewService bindExpertAddNewServiceFragment();

    @ContributesAndroidInjector
    abstract ExpertToCollaborateFragment bindExpertToCollaborate();

    @ContributesAndroidInjector
    abstract ExpertToCreatePostFragment bindExpertCreatePost();


}
