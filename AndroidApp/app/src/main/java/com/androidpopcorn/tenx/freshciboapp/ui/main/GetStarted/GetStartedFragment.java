package com.androidpopcorn.tenx.freshciboapp.ui.main.GetStarted;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.main.otp.OtpFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.regis.RegisFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class GetStartedFragment extends Fragment {

    private static final String TAG = "GetStartedFragment";

    Unbinder unbinder;

    @Inject
    MainViewModel viewModel;
    @BindView(R.id.btn_verify)
    Button btnVerify;
    @Inject
    RegisFragment regisFragment;

    @Inject
    public GetStartedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_get_started, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: ViewModel is NULL");
        else
            Log.d(TAG, "onCreateView: ViewModel is NOT NULL");

        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @OnClick(R.id.btn_verify)
    public void onViewClicked() {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_right);
        transaction.replace(R.id.frame_main,regisFragment).commit();
    }
}
