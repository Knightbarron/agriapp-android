package com.androidpopcorn.tenx.freshciboapp.Data.model.profile;

import com.google.gson.annotations.SerializedName;

public class GetFarmerResponse {

    @SerializedName("farmer")
    private FarmerData farmerData;

    public FarmerData getFarmerData() {
        return farmerData;
    }

    public void setFarmerData(FarmerData farmerData) {
        this.farmerData = farmerData;
    }

    public class FarmerData {
        @SerializedName("farmer_id")
        private int farmerID;
        @SerializedName("farmer_full_name")
        private String farmerFullName;
        @SerializedName("farmer_email")
        private String farmerEmail;
        @SerializedName("farmer_address")
        private String farmerAddress;
        @SerializedName("farmer_phone")
        private String farmerPhone;

        @SerializedName("farmer_image_path")
        private String farmerImage;
        @SerializedName("farmer_date_created")
        private String farmerDateCreated;
        @SerializedName("farmer_date_modified")
        private String farmerDateModified;
        @SerializedName("farmer_location")
        private String farmerLocation;
        @SerializedName("farmer_nearest_market")
        private String farmerNearestMarket;
        @SerializedName("farmer_land_area")
        private String farmerLandArea;
        @SerializedName("farmer_land_owner")
        private String farmerLandOwner;


        @SerializedName("farmer_organic_farming")
        private boolean doesOrganicFarming;

        public int getFarmerID() {
            return farmerID;
        }

        public void setFarmerID(int farmerID) {
            this.farmerID = farmerID;
        }

        public String getFarmerFullName() {
            return farmerFullName;
        }

        public void setFarmerFullName(String farmerFullName) {
            this.farmerFullName = farmerFullName;
        }

        public String getFarmerEmail() {
            return farmerEmail;
        }

        public void setFarmerEmail(String farmerEmail) {
            this.farmerEmail = farmerEmail;
        }

        public String getFarmerAddress() {
            return farmerAddress;
        }

        public void setFarmerAddress(String farmerAddress) {
            this.farmerAddress = farmerAddress;
        }

        public String getFarmerPhone() {
            return farmerPhone;
        }

        public void setFarmerPhone(String farmerPhone) {
            this.farmerPhone = farmerPhone;
        }

        public String getFarmerImage() {
            return farmerImage;
        }

        public void setFarmerImage(String farmerImage) {
            this.farmerImage = farmerImage;
        }

        public String getFarmerDateCreated() {
            return farmerDateCreated;
        }

        public void setFarmerDateCreated(String farmerDateCreated) {
            this.farmerDateCreated = farmerDateCreated;
        }

        public String getFarmerDateModified() {
            return farmerDateModified;
        }

        public void setFarmerDateModified(String farmerDateModified) {
            this.farmerDateModified = farmerDateModified;
        }

        public String getFarmerLocation() {
            return farmerLocation;
        }

        public void setFarmerLocation(String farmerLocation) {
            this.farmerLocation = farmerLocation;
        }

        public String getFarmerNearestMarket() {
            return farmerNearestMarket;
        }

        public void setFarmerNearestMarket(String farmerNearestMarket) {
            this.farmerNearestMarket = farmerNearestMarket;
        }

        public String getFarmerLandArea() {
            return farmerLandArea;
        }

        public void setFarmerLandArea(String farmerLandArea) {
            this.farmerLandArea = farmerLandArea;
        }

        public String getFarmerLandOwner() {
            return farmerLandOwner;
        }

        public void setFarmerLandOwner(String farmerLandOwner) {
            this.farmerLandOwner = farmerLandOwner;
        }

        public boolean isDoesOrganicFarming() {
            return doesOrganicFarming;
        }

        public void setDoesOrganicFarming(boolean doesOrganicFarming) {
            this.doesOrganicFarming = doesOrganicFarming;
        }

    }



}
