package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.options;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options.ExpertToAddNewService;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options.ExpertToCollaborateFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options.ExpertToCreatePostFragment;

import java.util.ArrayList;
import java.util.List;

public class OptionsViewPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = "ViewPagerAdapter";

    private List<Fragment> fragList;


    public OptionsViewPagerAdapter(FragmentManager fm) {
        super(fm);
        Log.d(TAG, "ViewPagerAdapter: ViewPager Called");
        fragList = new ArrayList<>();
        fragList.add(new BusinessToCreateAd());
        fragList.add(new BusinessToAddPackage());
        fragList.add(new BusinessToCollab());
    }

    @Override
    public Fragment getItem(int position) {
        return fragList.get(position);
    }

    @Override
    public int getCount() {

        Log.d(TAG, "getCount: size frags : "+fragList.size());
        return fragList.size(); //three fragments
    }
}
