package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer;

import android.app.Application;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.base.ViewModelFactory;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityScope;

import dagger.Module;
import dagger.Provides;


@Module
public class HomeFarmerModule {

    @Provides
    @ActivityContext
    @ActivityScope
    Context provideContext(HomeFarmerActivity homeFarmerActivity){
        return homeFarmerActivity;
    }


    @Provides
    @ActivityScope
    HomeFarmerViewModel provideVM(@ActivityContext Context context, Application application, AppDataManager appDataManager){

        HomeFarmerViewModel vm = new HomeFarmerViewModel(appDataManager, application);
        ViewModelFactory<HomeFarmerViewModel> factory = new ViewModelFactory<>(vm,appDataManager,application);
        return ViewModelProviders.of((HomeFarmerActivity) context, factory).get(HomeFarmerViewModel.class);

    }


}
