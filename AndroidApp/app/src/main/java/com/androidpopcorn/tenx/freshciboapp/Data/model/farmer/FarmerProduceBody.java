package com.androidpopcorn.tenx.freshciboapp.Data.model.farmer;

import com.google.gson.annotations.SerializedName;

public class FarmerProduceBody {

    @SerializedName("farmer_produce_name")
    private String farmer_produce_name;
    @SerializedName("farmer_produce_quantity")
    private String farmer_produce_quantity;
    @SerializedName("farmer_produce_category")
    private String farmer_produce_category;
    @SerializedName("farmer_produce_details")
    private String farmer_produce_details;
    @SerializedName("farmer_produce_harvest_time")
    private String farmer_produce_harvest_time;
    @SerializedName("farmer_produce_land_area")
    private String farmer_produce_land_area;
    @SerializedName("farmer_produce_organically_farm")
    private String farmer_produce_organically_farm;
    @SerializedName("farmer_produce_expected_price")
    private String farmer_produce_expected_price;

    public FarmerProduceBody(String farmer_produce_name, String farmer_produce_quantity, String farmer_produce_category, String farmer_produce_details, String farmer_produce_harvest_time, String farmer_produce_land_area, String farmer_produce_organically_farm, String farmer_produce_expected_price) {
        this.farmer_produce_name = farmer_produce_name;
        this.farmer_produce_quantity = farmer_produce_quantity;
        this.farmer_produce_category = farmer_produce_category;
        this.farmer_produce_details = farmer_produce_details;
        this.farmer_produce_harvest_time = farmer_produce_harvest_time;
        this.farmer_produce_land_area = farmer_produce_land_area;
        this.farmer_produce_organically_farm = farmer_produce_organically_farm;
        this.farmer_produce_expected_price = farmer_produce_expected_price;
    }

    public FarmerProduceBody() {
    }

    public String getFarmer_produce_name() {
        return farmer_produce_name;
    }

    public void setFarmer_produce_name(String farmer_produce_name) {
        this.farmer_produce_name = farmer_produce_name;
    }

    public String getFarmer_produce_quantity() {
        return farmer_produce_quantity;
    }

    public void setFarmer_produce_quantity(String farmer_produce_quantity) {
        this.farmer_produce_quantity = farmer_produce_quantity;
    }

    public String getFarmer_produce_category() {
        return farmer_produce_category;
    }

    public void setFarmer_produce_category(String farmer_produce_category) {
        this.farmer_produce_category = farmer_produce_category;
    }

    public String getFarmer_produce_details() {
        return farmer_produce_details;
    }

    public void setFarmer_produce_details(String farmer_produce_details) {
        this.farmer_produce_details = farmer_produce_details;
    }

    public String getFarmer_produce_harvest_time() {
        return farmer_produce_harvest_time;
    }

    public void setFarmer_produce_harvest_time(String farmer_produce_harvest_time) {
        this.farmer_produce_harvest_time = farmer_produce_harvest_time;
    }

    public String getFarmer_produce_land_area() {
        return farmer_produce_land_area;
    }

    public void setFarmer_produce_land_area(String farmer_produce_land_area) {
        this.farmer_produce_land_area = farmer_produce_land_area;
    }

    public String getFarmer_produce_organically_farm() {
        return farmer_produce_organically_farm;
    }

    public void setFarmer_produce_organically_farm(String farmer_produce_organically_farm) {
        this.farmer_produce_organically_farm = farmer_produce_organically_farm;
    }

    public String getFarmer_produce_expected_price() {
        return farmer_produce_expected_price;
    }

    public void setFarmer_produce_expected_price(String farmer_produce_expected_price) {
        this.farmer_produce_expected_price = farmer_produce_expected_price;
    }
}
