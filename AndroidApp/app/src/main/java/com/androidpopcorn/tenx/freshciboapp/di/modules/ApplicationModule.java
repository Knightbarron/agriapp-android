package com.androidpopcorn.tenx.freshciboapp.di.modules;


import android.app.Application;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;
import com.androidpopcorn.tenx.freshciboapp.Data.prefs.PreferencesInfo;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityScope;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ApplicationContext;
import com.androidpopcorn.tenx.freshciboapp.utils.AppConstants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public abstract class ApplicationModule {


    @Binds
    @Singleton
    @ApplicationContext
    abstract Context bindContext(Application application);


    @Provides
    @Singleton
    static ApiService provideApiService(Retrofit retrofit){

        return retrofit.create(ApiService.class);

    }



//    provide file name for shared prefs
    @Provides
    @Singleton
    @PreferencesInfo
    static String providePrefFileName(){
        return AppConstants.PREF_FILE_NAME;
    }



    //Glide


    @Provides
    @Singleton
    static RequestOptions provideRequestOptions(){
        return RequestOptions.placeholderOf(R.drawable.ic_launcher_background).
                error(R.drawable.ic_launcher_background);
    }

    @Provides
    @Singleton
    static Drawable provideAppDrawable(Application application){
        return ContextCompat.getDrawable(application,
                R.drawable.ic_launcher_background);
    }

    @Provides
    @Singleton
    static RequestManager provideGlideInstance(Application application,
                                               RequestOptions requestOptions){
        return Glide.with(application).setDefaultRequestOptions(requestOptions);
    }


}
