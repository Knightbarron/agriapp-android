package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.profile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpertPackageAdapter extends RecyclerView.Adapter<ExpertPackageAdapter.ExpertPackageViewHolder> {


    private static final String TAG = "ExpertPackageAdapter";

    private Context mCtx;
    private List<ExpertPackageResponse.ExpertPackage> mList;


    @Inject
    public ExpertPackageAdapter(@ActivityContext Context mCtx) {
        this.mCtx = mCtx;
        mList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ExpertPackageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mCtx).inflate(R.layout.listitem_packages, viewGroup, false);

        return new ExpertPackageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpertPackageViewHolder vh, int i) {
        ExpertPackageResponse.ExpertPackage currentPackage = mList.get(i);

        vh.tvPackageTitle.setText(currentPackage.getPackage_title());
        vh.tvPackageCategory.setText(currentPackage.getPackage_category());
        vh.tvPackageDetails.setText(currentPackage.getPackage_details());
        vh.tvPackageDuration.setText(currentPackage.getPackage_duration());
        vh.tvPackagePrice.setText(currentPackage.getPackage_price());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ExpertPackageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_package_title)
        TextView tvPackageTitle;
        @BindView(R.id.tv_package_category)
        TextView tvPackageCategory;
        @BindView(R.id.tv_package_duration)
        TextView tvPackageDuration;
        @BindView(R.id.tv_package_price)
        TextView tvPackagePrice;
        @BindView(R.id.tv_package_details)
        TextView tvPackageDetails;

        public ExpertPackageViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


    public void updateData(List<ExpertPackageResponse.ExpertPackage> data){
        mList.clear();
        mList.addAll(data);
        notifyDataSetChanged();
    };


    public void updateSinglePackage(ExpertPackageResponse.ExpertPackage pack){
        Log.d(TAG, "updateSinglePackage: updaging single package");
        mList.add(pack);
        notifyItemInserted(mList.size()-1);
    }
}
