package com.androidpopcorn.tenx.freshciboapp.Data.model.business;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BusinessCategoryBody {

    @SerializedName("categories")
    ArrayList<String> categories;


    public BusinessCategoryBody(ArrayList<String> categories) {
        this.categories = categories;
    }


    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public BusinessCategoryBody() {

    }
}
