package com.androidpopcorn.tenx.freshciboapp.Data.model.business;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BusinessCategoryResponse {



    @Override
    public String toString() {
        String test = "";
        for(CategoryItem item : categories){
            test += item.getName() + " ,";
        }
        return test;
    }

    @SerializedName("categories")
    private ArrayList<CategoryItem> categories;


    public class CategoryItem {
        @SerializedName("business_category_name")
        private String name;

        @SerializedName("business_category_id")
        private int category_id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }
    }

    public ArrayList<CategoryItem> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoryItem> categories) {
        this.categories = categories;
    }
}
