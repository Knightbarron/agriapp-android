package com.androidpopcorn.tenx.freshciboapp.Data;

import android.content.Context;

import com.androidpopcorn.tenx.freshciboapp.Data.api.AppApiHelper;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.AllExpertsResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.NotificationResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;


import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceBody;

import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.login.LoginResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.OtpResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyBusiness;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyExpert;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyFarmer;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.ExpertPatchResponse;


import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.VerifyResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.TokenResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.PostFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetBusinessResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetFarmerResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetSpecialistResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetCommentResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetQueryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.verifyUser.UserObject;
import com.androidpopcorn.tenx.freshciboapp.Data.prefs.AppPreferencesHelper;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ApplicationContext;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class AppDataManager implements DataManager {

    private AppApiHelper apiHelper;
    private AppPreferencesHelper prefHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context, AppApiHelper apiHelper,
                          AppPreferencesHelper prefHelper) {
        this.apiHelper = apiHelper;
        this.prefHelper = prefHelper;
    }

    @Override
    public Observable<Response<VerifyResponse>> verifyAuth() {
        return apiHelper.verifyAuth();
    }

    @Override
    public Observable<Response<UserObject>> verifyUserType() {
        return apiHelper.verifyUserType();
    }


    @Override
    public Observable<Response<DefaultResponse>> saveFarmCategory(FarmerCategoryBody farmerCategoryBody) {
        return apiHelper.saveFarmCategory(farmerCategoryBody);
    }

    @Override
    public Observable<Response<DefaultResponse>> saveProduceDetails(FarmerProduceBody body) {
        return apiHelper.saveProduceDetails(body);
    }

    @Override
    public Observable<Response<DefaultResponse>> saveCompleteProfileDetails(FarmerCompleteProfileBody body) {
        return apiHelper.saveCompleteProfileDetails(body);
    }

    @Override
    public Observable<Response<FarmerProfileResponse>> getCurrentFarmerProfile() {
        return apiHelper.getCurrentFarmerProfile();
    }

    @Override
    public Observable<Response<FarmerProduceResponse>> getCurrentFarmerProduce(int id) {
        return apiHelper.getCurrentFarmerProduce(id);
    }

    @Override
    public Observable<Response<FarmerCategoryResponse>> getCurrentFarmerCategory(int id) {
        return apiHelper.getCurrentFarmerCategory(id);
    }

    @Override
    public Observable<Response<DefaultResponse>> patchFarmerProduce(int id,FarmerProduceBody body) {
        return apiHelper.patchFarmerProduce(id,body);
    }

    @Override
    public Observable<Response<ExpertPackageResponse.ExpertPackage>> saveExpertPackage(ExpertPackageBody body) {
        return apiHelper.saveExpertPackage(body);
    }

    @Override
    public Observable<Response<AllExpertsResponse>> getAllExperts() {
        return apiHelper.getAllExperts();
    }

    @Override
    public Observable<Response<BusinessCategoryBody>> saveBusinessCategory(BusinessCategoryBody body) {
        return apiHelper.saveBusinessCategory(body);
    }

    @Override
    public Observable<Response<BusinessProfileResponse>> getBusinessProfile() {
        return apiHelper.getBusinessProfile();
    }

    @Override
    public Observable<Response<DefaultResponse>> patchBusinessProfile(BusinessProfileBody body) {
        return apiHelper.patchBusinessProfile(body);
    }

    @Override
    public Observable<Response<RegistrationResponse>> farmerRegister(RegistrationBodyFarmer
                                                                                 body) {
        return apiHelper.farmerRegister(body);
    }

    @Override
    public Observable<Response<RegistrationResponse>> specialistRegister(
            RegistrationBodyExpert body) {
        return apiHelper.specialistRegister(body);
    }

    @Override
    public Observable<Response<RegistrationResponse>> businessRegister(RegistrationBodyBusiness body) {
        return apiHelper.businessRegister(body);
    }

    @Override
    public Observable<Response<DefaultResponse>> validate(String email, String token) {
        return apiHelper.validate(email, token);
    }

    @Override
    public Observable<Response<LoginResponse>> loginFarmer(String phone, String password) {
        return apiHelper.loginFarmer(phone,password);
    }

    @Override
    public Observable<Response<LoginResponse>> loginExpert(String phone, String password) {
        return  apiHelper.loginExpert(phone,password);
    }

    @Override
    public Observable<Response<LoginResponse>> loginBusiness(String phone, String password) {
        return apiHelper.loginBusiness(phone,password);
    }

    @Override
    public Observable<Response<OtpResponse>> sendOtpRequest(String phone) {
        return apiHelper.sendOtpRequest(phone);
    }

    @Override
    public Observable<Response<OtpResponse>> verifyOtp(String session_id, String phone) {
        return apiHelper.verifyOtp(session_id,phone);
    }


    @Override
    public String getAccessToken() {
        return prefHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String token) {
        prefHelper.setAccessToken(token);
    }

    @Override
    public void setEmail(String email) {
        prefHelper.setEmail(email);
    }

    @Override
    public String getEmail() {
        return prefHelper.getEmail();
    }

    @Override
    public String getTypeUser() {
        return prefHelper.getTypeUser();
    }

    @Override
    public void setTypeUser(String userType) {
            prefHelper.setTypeUser(userType);
    }

    @Override
    public Observable<Response<TokenResponse>> saveToken(String token) {
        return apiHelper.saveToken(token);
    }

    @Override
    public Observable<Response<ExpertPatchResponse>> updateExpertProfile(ExpertPatchBody body) {
        return apiHelper.updateExpertProfile(body);
    }

    @Override
    public Observable<Response<DefaultResponse>> uploadDocuments(MultipartBody.Part file, RequestBody requestBody, String doctype) {
        return apiHelper.uploadDocuments(file, requestBody, doctype);
    }

    @Override
    public Observable<Response<ExpertProfileResponse>> getCurrentExpertProfile() {
        return apiHelper.getCurrentExpertProfile();
    }

    @Override
    public Observable<Response<ExpertPackageResponse>> getCurrentExpertPackage() {
        return apiHelper.getCurrentExpertPackage();
    }

    @Override
    public Observable<Response<DefaultResponse>> uploadProfilePicture(MultipartBody.Part file, RequestBody requestBody) {
        return apiHelper.uploadProfilePicture(file, requestBody);
    }

    @Override
    public Observable<Response<NotificationResponse>> getNotificationsForUser() {
        return apiHelper.getNotificationsForUser();
    }

    public Observable<Response<DefaultResponse>> saveBusinessPackage(BusinessPackageBody body) {
        return apiHelper.saveBusinessPackage(body);
    }

    @Override
    public Observable<Response<BusinessCategoryResponse>> getBusinessCategory() {
        return apiHelper.getBusinessCategory();
    }

    @Override
    public Observable<Response<BusinessPackageResponse>> getBusinessPackageList() {
        return apiHelper.getBusinessPackageList();
    }

    public Observable<Response<PostFeedResponse>> postCreatePost(MultipartBody.Part file, RequestBody titleBody, RequestBody contentBody, RequestBody categoryBody) {
        return apiHelper.postCreatePost(file, titleBody, contentBody,categoryBody);
    }

    @Override
    public Observable<Response<DefaultResponse>> createNewPost(MultipartBody.Part file, RequestBody titleBody, RequestBody contentBody) {
        return apiHelper.createNewPost(file,titleBody,contentBody);
    }

    @Override
    public Observable<Response<DefaultResponse>> createNewQuery(MultipartBody.Part file, RequestBody questionBody) {
        return apiHelper.createNewQuery(file,questionBody);
    }

    @Override
    public Observable<Response<GetFeedResponse>> getGetAllFeeds() {
        return apiHelper.getGetAllFeeds();
    }

    @Override
    public Observable<Response<GetQueryResponse>> getQueriesForSpecialist() {
        return apiHelper.getQueriesForSpecialist();
    }

    @Override
    public Observable<Response<GetCommentResponse>> getCommentsForConv(String convid) {
        return apiHelper.getCommentsForConv(convid);
    }

    @Override
    public Observable<Response<GetFeedResponse>> getGetAllFeedsPaged(int limit, int page) {
        return apiHelper.getGetAllFeedsPaged(limit, page);
    }

    @Override
    public Observable<Response<GetFarmerResponse>> getFarmerById(int farmerid) {
        return apiHelper.getFarmerById(farmerid);
    }

    @Override
    public Observable<Response<GetSpecialistResponse>> getExpertById(int expertID) {
        return apiHelper.getExpertById(expertID);
    }

    @Override
    public Observable<Response<GetBusinessResponse>> getBusinessById(int businessID) {
        return apiHelper.getBusinessById(businessID);
    }
}
