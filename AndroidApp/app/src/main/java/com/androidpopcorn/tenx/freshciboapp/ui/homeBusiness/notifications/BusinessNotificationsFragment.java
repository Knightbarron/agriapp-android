package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.notifications;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.notifications.adapter.NotificationAdapter;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.notifications.adapter.NotificationsClass;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessNotificationsFragment extends Fragment {


    @Inject
    HomeBusinessViewModel viewModel;

    RecyclerView recyclerView;

    @Inject
    NotificationAdapter adapter;

    ArrayList<NotificationsClass> items;

    private static final String TAG = "BusinessNotificationsFr";


    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) v.getTag();
            int position = viewHolder.getAdapterPosition();

            String selectedItem = items.get(position).getTitle();

            Log.d(TAG, "onClick: " + items.get(position).getTitle());

            Toast.makeText(getActivity(), "You Selected : " + selectedItem, Toast.LENGTH_SHORT).show();



        }
    };



    @Inject
    public BusinessNotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_business_notifications, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        loadItems();
        setUpRecycler(recyclerView, adapter);


        if(viewModel==null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");

        return view;
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    private ArrayList<NotificationsClass> loadItems() {

        //TODO change the photos
        items = new ArrayList<>();
        items.add(new NotificationsClass(R.drawable.cereals_, "Seeds Supplier","Body"));
        items.add(new NotificationsClass(R.drawable.cotton_, "Fertilizer Supplier","2asasfasfsaf"));
        items.add(new NotificationsClass(R.drawable.chat_button, "Farm Machinery","asfasdf"));
        items.add(new NotificationsClass(R.drawable.circle, "Pesticides & Insecticles","asdasd"));

        return items;
    }

    private void setUpRecycler(RecyclerView recyclerView, NotificationAdapter adapter) {


        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(onItemClickListener);
        adapter.updateListItems(loadItems());
    }



}
