package com.androidpopcorn.tenx.freshciboapp.base;

import android.app.Application;
import android.arch.lifecycle.ViewModel;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.DataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel extends ViewModel {

    private CompositeDisposable compositeDisposable;
    private AppDataManager dataManager;

    public BaseViewModel(AppDataManager appDataManager,
                         Application application) {
        this.dataManager = appDataManager;
        this.compositeDisposable = new CompositeDisposable();
    }


    @Override
    protected void onCleared() {
        super.onCleared();
//        clear the   rxjava calls
        compositeDisposable.dispose();
        compositeDisposable.clear();

    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }
}
