package com.androidpopcorn.tenx.freshciboapp.ui.main.welcome;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.main.SelectionExtra.SelectionExtraFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.login.LoginFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.phone.PhonenoFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;
import retrofit2.internal.EverythingIsNonNull;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeFragment extends Fragment {


    private static final String TAG = "WelcomeFragment";

    @Inject
    MainViewModel viewModel;
    Unbinder unbinder;

    WelcomeFragment frag;

    @Inject
    FragmentManager fm;
    @BindView(R.id.btn_get_started)
    Button btnGetStarted;
    @BindView(R.id.btn_otp_login)
    Button btnOtpLogin;

    @Inject
    PhonenoFragment phonenoFragment;

    @Inject
    SelectionExtraFragment selectionExtraFragment;

    @Inject
    LoginFragment loginFragment;



    @Inject
    public WelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_welcome, container, false);


        if (viewModel == null)
            Log.d(TAG, "onCreateView: ViewMOdel is NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_get_started)
    public void onBtnGetStartedClicked() {
      initializeFragments(phonenoFragment);
    }

    @OnClick(R.id.btn_otp_login)
    public void onBtnOtpLoginClicked() {
        initializeFragments(selectionExtraFragment);
    }

    public void initializeFragments( Fragment frag){
        String backStateName = frag.getClass().toString();
        //Log.d(TAG, "onBtnOtpLoginClicked: " + backStateName);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_right,R.anim.enter_from_right,R.anim.exit_to_right);

        transaction.replace(R.id.frame_main,frag);
        transaction.addToBackStack(backStateName);

        transaction.commit();
    }



}
