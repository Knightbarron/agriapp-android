package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.CategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.base.FarmerBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.selection.SelectionFragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class FarmerCategoryFragment extends Fragment {


    private static final String TAG = "FarmerCategoryFragment";

    @Inject
    HomeFarmerViewModel viewModel;


    RecyclerView recyclerView;

    ArrayList<CategoryClass> items;

    //CategoriesSelected
    ArrayList<CategoryBody> selectedItems;


    @Inject
    FarmerCategoryAdapter adapter;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;
    Unbinder unbinder;

    @Inject
    FarmerBaseFragment farmerBaseFragment;

    @Inject
    public FarmerCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_farmer_category, container, false);


        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");

        recyclerView = view.findViewById(R.id.recycler_view);
        loadItems();
        selectedItems = new ArrayList<>();
        setUpRecycler(recyclerView, adapter);


        subscribeObserver();

        unbinder = ButterKnife.bind(this, view);

        editBtnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButtonClicked();
            }
        });


        return view;
    }


    public void goToBase() {
        String backStateName = farmerBaseFragment.getClass().toString();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(R.id.container_farmer, farmerBaseFragment);
        transaction.addToBackStack(backStateName);
        transaction.commit();
    }


    private void subscribeObserver() {

        viewModel.getStatusCategoryFarmer().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean){
                    Log.d(TAG, "onChanged: 200 Status received .");
                    //TODO test
                    goToBase();

                }
                else{
                    Toast.makeText(getActivity(), "Error while saving the data. Please check your network connectivity and try again . ", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private ArrayList<CategoryClass> loadItems() {
        //TODO change the photos
        items = new ArrayList<>();
        items.add(new CategoryClass(R.drawable.cereals_, "Cereals"));
        items.add(new CategoryClass(R.drawable.cotton_, "Cotton and  other Fibres"));
        items.add(new CategoryClass(R.drawable.dairy_, "Dairy Farming"));
        items.add(new CategoryClass(R.drawable.oilseeds_, "Edible oilseeds"));
        items.add(new CategoryClass(R.drawable.fish_, "Fish Farming"));
        items.add(new CategoryClass(R.drawable.fruits_, "Fruits"));
        items.add(new CategoryClass(R.drawable.pulses_, "Grams and  Pulses"));
        items.add(new CategoryClass(R.drawable.nuts_, "Nuts"));
        items.add(new CategoryClass(R.drawable.plantation_, "Plantation Crops"));
        items.add(new CategoryClass(R.drawable.poultry_, "Poultry Farming"));
        items.add(new CategoryClass(R.drawable.roots_, "Roots and Tubers"));
        items.add(new CategoryClass(R.drawable.sapling_, "Saplings and Nursery"));
        items.add(new CategoryClass(R.drawable.spices_, "Cash crops  and Spices "));
        items.add(new CategoryClass(R.drawable.vegetables_, "Vegetables"));
        return items;

    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    public void setUpRecycler(RecyclerView recycler, FarmerCategoryAdapter adapter) {
        recycler.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recycler.setAdapter(adapter);
        adapter.updateListItems(loadItems());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void ButtonClicked() {

        Log.d(TAG, "ButtonClicked: SIZE OF THE LIST : " +adapter.selectedItems.size());

        Iterator<String> iter = adapter.selectedItems.iterator();
        while(iter.hasNext()){
            CategoryBody c = new CategoryBody();
                c.setName(iter.next());
                selectedItems.add(c);
        }
        viewModel.saveCategoryFarmer(new FarmerCategoryBody(selectedItems));


    }
}
