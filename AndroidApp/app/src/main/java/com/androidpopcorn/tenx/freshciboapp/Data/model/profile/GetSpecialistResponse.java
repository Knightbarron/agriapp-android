package com.androidpopcorn.tenx.freshciboapp.Data.model.profile;

import com.google.gson.annotations.SerializedName;

public class GetSpecialistResponse {


    @SerializedName("specialist")
    private SpecialistData specialistData;


    public SpecialistData getSpecialistData() {
        return specialistData;
    }

    public void setSpecialistData(SpecialistData specialistData) {
        this.specialistData = specialistData;
    }

    public class SpecialistData {
        private float specialist_id;
        private String specialist_full_name;
        private String specialist_email = null;
        private String specialist_address = null;
        private String specialist_phone;
        private String specialist_image_path = null;
        private String specialist_organization = null;
        private String specialist_qualification = null;
        private String specialist_specialization = null;
        private String specialist_specialization2 = null;
        private String specialist_cert_pdf_path = null;
        private String specialist_companyid_pdf_path = null;
        private String specialist_about_me = null;
        private String specialist_date_created;
        private String specialist_date_modified;

        public float getSpecialist_id() {
            return specialist_id;
        }

        public void setSpecialist_id(float specialist_id) {
            this.specialist_id = specialist_id;
        }

        public String getSpecialist_full_name() {
            return specialist_full_name;
        }

        public void setSpecialist_full_name(String specialist_full_name) {
            this.specialist_full_name = specialist_full_name;
        }

        public String getSpecialist_email() {
            return specialist_email;
        }

        public void setSpecialist_email(String specialist_email) {
            this.specialist_email = specialist_email;
        }

        public String getSpecialist_address() {
            return specialist_address;
        }

        public void setSpecialist_address(String specialist_address) {
            this.specialist_address = specialist_address;
        }

        public String getSpecialist_phone() {
            return specialist_phone;
        }

        public void setSpecialist_phone(String specialist_phone) {
            this.specialist_phone = specialist_phone;
        }

        public String getSpecialist_image_path() {
            return specialist_image_path;
        }

        public void setSpecialist_image_path(String specialist_image_path) {
            this.specialist_image_path = specialist_image_path;
        }

        public String getSpecialist_organization() {
            return specialist_organization;
        }

        public void setSpecialist_organization(String specialist_organization) {
            this.specialist_organization = specialist_organization;
        }

        public String getSpecialist_qualification() {
            return specialist_qualification;
        }

        public void setSpecialist_qualification(String specialist_qualification) {
            this.specialist_qualification = specialist_qualification;
        }

        public String getSpecialist_specialization() {
            return specialist_specialization;
        }

        public void setSpecialist_specialization(String specialist_specialization) {
            this.specialist_specialization = specialist_specialization;
        }

        public String getSpecialist_specialization2() {
            return specialist_specialization2;
        }

        public void setSpecialist_specialization2(String specialist_specialization2) {
            this.specialist_specialization2 = specialist_specialization2;
        }

        public String getSpecialist_cert_pdf_path() {
            return specialist_cert_pdf_path;
        }

        public void setSpecialist_cert_pdf_path(String specialist_cert_pdf_path) {
            this.specialist_cert_pdf_path = specialist_cert_pdf_path;
        }

        public String getSpecialist_companyid_pdf_path() {
            return specialist_companyid_pdf_path;
        }

        public void setSpecialist_companyid_pdf_path(String specialist_companyid_pdf_path) {
            this.specialist_companyid_pdf_path = specialist_companyid_pdf_path;
        }

        public String getSpecialist_about_me() {
            return specialist_about_me;
        }

        public void setSpecialist_about_me(String specialist_about_me) {
            this.specialist_about_me = specialist_about_me;
        }

        public String getSpecialist_date_created() {
            return specialist_date_created;
        }

        public void setSpecialist_date_created(String specialist_date_created) {
            this.specialist_date_created = specialist_date_created;
        }

        public String getSpecialist_date_modified() {
            return specialist_date_modified;
        }

        public void setSpecialist_date_modified(String specialist_date_modified) {
            this.specialist_date_modified = specialist_date_modified;
        }
    }


}
