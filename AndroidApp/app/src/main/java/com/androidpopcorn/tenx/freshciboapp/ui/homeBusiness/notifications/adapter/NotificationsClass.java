package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.notifications.adapter;

public class NotificationsClass {

    private Integer profilePic;
    private String title;
    private String body;

    public NotificationsClass(Integer profilePic, String title, String body) {
        this.profilePic = profilePic;
        this.title = title;
        this.body = body;
    }


    public NotificationsClass() {
    }

    public Integer getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Integer profilePic) {
        this.profilePic = profilePic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
