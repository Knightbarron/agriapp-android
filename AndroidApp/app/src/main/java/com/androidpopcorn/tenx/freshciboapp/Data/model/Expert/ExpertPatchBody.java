package com.androidpopcorn.tenx.freshciboapp.Data.model.Expert;

import com.google.gson.annotations.SerializedName;

public class ExpertPatchBody {

    @SerializedName("specialist_full_name")
    private String name;
    @SerializedName("specialist_email")
    private String email;

    @SerializedName("specialist_address")
    private String address;

    @SerializedName("specialist_organization")
    private String organization;

    @SerializedName("specialist_specialization")
    private String specialization;

    @SerializedName("specialist_specialization2")
    private String specialization2;

    @SerializedName("specialist_qualification")
    private String qualification;

    @SerializedName("specialist_about_me")
    private String aboutMe;

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getSpecialization2() {
        return specialization2;
    }

    public void setSpecialization2(String specialization2) {
        this.specialization2 = specialization2;
    }

    public ExpertPatchBody() {
    }

    public ExpertPatchBody(String name, String email, String address, String organization, String specialization, String specialization2, String qualification) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.organization = organization;
        this.specialization = specialization;
        this.specialization2 = specialization2;
        this.qualification = qualification;
    }

    public ExpertPatchBody(String aboutMe) {
        this.aboutMe = aboutMe;
    }
}
