package com.androidpopcorn.tenx.freshciboapp.Data.model.business;

import com.google.gson.annotations.SerializedName;

public class BusinessProfileBody {

    @SerializedName("business_name")
    private String name;
    @SerializedName("business_organization")
    private String organization;
    @SerializedName("business_address")
    private String address;
    @SerializedName("business_email")
    private String email;
    @SerializedName("business_location")
    private String location ;
    @SerializedName("business_type_1")
    private String business_type_1;
    @SerializedName("business_type_2")
    private String business_type_2;
    @SerializedName("business_role")
    private String business_role;
    @SerializedName("business_about_me")
    private String business_bio;


    public BusinessProfileBody(String name, String email, String location, String business_type_1, String business_type_2, String business_role) {
        this.name = name;
        this.email = email;
        this.location = location;
        this.business_type_1 = business_type_1;
        this.business_type_2 = business_type_2;
        this.business_role = business_role;
    }

    public BusinessProfileBody(String name, String email, String location, String business_type_1,
                               String business_type_2, String business_role,String address,String organization) {
        this.name = name;
        this.email = email;
        this.location = location;
        this.business_type_1 = business_type_1;
        this.business_type_2 = business_type_2;
        this.business_role = business_role;
        this.address = address;
        this.organization = organization;


    }



    public BusinessProfileBody(String business_bio){
        this.business_bio = business_bio;
    }


    public BusinessProfileBody(String name, String address, String email) {
        this.name = name;
        this.address = address;
        this.email = email;
    }

    public BusinessProfileBody(String name, String organization, String address, String email) {
        this.name = name;
        this.organization = organization;
        this.address = address;
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBusiness_type_1() {
        return business_type_1;
    }

    public void setBusiness_type_1(String business_type_1) {
        this.business_type_1 = business_type_1;
    }

    public String getBusiness_type_2() {
        return business_type_2;
    }

    public void setBusiness_type_2(String business_type_2) {
        this.business_type_2 = business_type_2;
    }

    public String getBusiness_role() {
        return business_role;
    }

    public void setBusiness_role(String business_role) {
        this.business_role = business_role;
    }

    public String getBusiness_bio() {
        return business_bio;
    }

    public void setBusiness_bio(String business_bio) {
        this.business_bio = business_bio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
