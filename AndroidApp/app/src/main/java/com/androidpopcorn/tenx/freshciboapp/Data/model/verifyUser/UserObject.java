package com.androidpopcorn.tenx.freshciboapp.Data.model.verifyUser;

import com.google.gson.annotations.SerializedName;

public class UserObject {

    @SerializedName("user")
    User UserObject;


    // Getter Methods

    public User getUser() {
        return UserObject;
    }

    // Setter Methods

    public void setUser(User userObject) {
        this.UserObject = userObject;
    }

    public class User {
        private String phone;
        private String type;
        private float id;
        private float iat;
        private float exp;


        // Getter Methods

        public String getPhone() {
            return phone;
        }

        public String getType() {
            return type;
        }

        public float getId() {
            return id;
        }

        public float getIat() {
            return iat;
        }

        public float getExp() {
            return exp;
        }

        // Setter Methods

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setId(float id) {
            this.id = id;
        }

        public void setIat(float iat) {
            this.iat = iat;
        }

        public void setExp(float exp) {
            this.exp = exp;
        }

    }
}
