package com.androidpopcorn.tenx.freshciboapp.Data.model.query;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetCommentResponse {
    @SerializedName("conversation_id")
    private int convid;

    @SerializedName("comments")
    private ArrayList<CommentItem> comments;

    public int getConvid() {
        return convid;
    }

    public void setConvid(int convid) {
        this.convid = convid;
    }

    public ArrayList<CommentItem> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentItem> comments) {
        this.comments = comments;
    }

    public class CommentItem {

        @SerializedName("query_comment_id")
        private String id;
        @SerializedName("query_comment_content")
        private String content;
        @SerializedName("farmer_farmer_id")
        private int farmerID;
        @SerializedName("specialist_specialist_id")
        private int specialistID;
        @SerializedName("query_comment_date_created")
        private String dateCreated;
        @SerializedName("query_comment_date_modified")
        private String dateModified;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getFarmerID() {
            return farmerID;
        }

        public void setFarmerID(int farmerID) {
            this.farmerID = farmerID;
        }

        public int getSpecialistID() {
            return specialistID;
        }

        public void setSpecialistID(int specialistID) {
            this.specialistID = specialistID;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public String getDateModified() {
            return dateModified;
        }

        public void setDateModified(String dateModified) {
            this.dateModified = dateModified;
        }
    }
}
