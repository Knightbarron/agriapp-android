package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer;

import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.answers.FarmerAnswersFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.base.FarmerBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection.FarmerCategoryFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCompleteProfile.FarmerCompleteProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.homefeed.FarmerHomeFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.notifications.FarmerNotificationsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.profile.FarmerProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.query.FarmerQueryFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class FarmerFragmentBuilder {


    @ContributesAndroidInjector(modules = {})
    abstract FarmerHomeFragment bindFarmerHomeFragment();

    @ContributesAndroidInjector(modules = {})
    abstract FarmerAnswersFragment bindFarmerAnswersFragment();

    @ContributesAndroidInjector(modules = {})
    abstract FarmerBaseFragment bindFarmerBaseFragment();

    @ContributesAndroidInjector(modules = {})
    abstract FarmerNotificationsFragment bindFarmerNotificationsFragment();

    @ContributesAndroidInjector(modules = {})
    abstract FarmerProfileFragment bindFarmerProfileFragment();

    @ContributesAndroidInjector(modules = {})
    abstract FarmerCategoryFragment bindFarmerCategoryFragment();

    @ContributesAndroidInjector(modules = {})
    abstract FarmerQueryFragment bindFarmerQueryFragment();

    @ContributesAndroidInjector(modules = {})
    abstract FarmerCompleteProfileFragment bindFarmerCompleteFragment();

}
