package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.query;


import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerViewModel;
import com.androidpopcorn.tenx.freshciboapp.utils.FileUtil;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class FarmerQueryFragment extends Fragment {


    private static final String TAG = "FarmerQueryFragment";
    public static final int REQUEST_CODE_IMAGE = 121;

    @Inject
    HomeFarmerViewModel viewModel;
    @BindView(R.id.upload_photos)
    Button uploadPhotos;
    @BindView(R.id.post_now)
    Button postNow;
    @BindView(R.id.editText)
    EditText body;
    @BindView(R.id.rel_layout1)
    RelativeLayout layout;
    @BindView(R.id.category_tv2)
    TextView tvCategory;

    Unbinder unbinder;

    String category;
    private File imageFile = null;



    @Inject
    public FarmerQueryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_farmer_query, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: ViewMODEL IS NULL");
        else {
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");
        }

        unbinder = ButterKnife.bind(this, view);

        subscribeObserverForCategorySelected();
        subscribeObserver();



        return view;
    }

    //ToDo check the spinner text item
    private void subscribeObserverForCategorySelected() {

        viewModel.getCategorySelected().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Log.d(TAG, "onChanged: Category is : " +s);
                tvCategory.setText(s);


            }
        });
    }

    private void subscribeObserver() {
        viewModel.getStatusQuery().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean){
                    Toast.makeText(getActivity(), "Query Posted", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getActivity(), "Failed to post query", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.upload_photos, R.id.post_now})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.upload_photos:
                searchImage();
                break;
            case R.id.post_now:
                postData();
                break;
        }
    }

    private void postData() {

        String postBody = body.getText().toString();
        if (TextUtils.isEmpty(postBody)) {
            Snackbar.make(layout, "Post content cannot be empty", Snackbar.LENGTH_SHORT).show();
            return;
        }


        viewModel.postNewQuery(postBody,imageFile);
    }

    private void searchImage() {

        Dexter.withActivity(getActivity()).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select a Image"),REQUEST_CODE_IMAGE);


            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(getActivity(), "You need to give read access", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }).check();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_IMAGE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                imageFile = getFile(uri);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    //ivImagePreview.setImageBitmap(bitmap);

                } catch (FileNotFoundException e) {
                    throw new Error("File not found");
                } catch (IOException e) {
                    throw new Error("IO exception");
                }
            }
        }
    }


    public File getFile(Uri uri) {
        try {
            File file = FileUtil.from(getActivity(), uri);
            Log.d("file", "File...:::: uti - " + file.getPath() + " file -" + file + " : " + file.exists());

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
