package com.androidpopcorn.tenx.freshciboapp.di;

import android.app.Application;

import com.androidpopcorn.tenx.freshciboapp.base.BaseApplication;
import com.androidpopcorn.tenx.freshciboapp.di.modules.ActivityBuilderModule;
import com.androidpopcorn.tenx.freshciboapp.di.modules.ApplicationModule;
import com.androidpopcorn.tenx.freshciboapp.di.modules.RetrofitModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, ApplicationModule.class, ActivityBuilderModule.class, RetrofitModule.class})
public interface ApplicationComponent {
    void inject(BaseApplication baseApplication);


    @Component.Builder
    interface Builder {
        ApplicationComponent build();


        @BindsInstance
        Builder application(Application application);
    }

}
