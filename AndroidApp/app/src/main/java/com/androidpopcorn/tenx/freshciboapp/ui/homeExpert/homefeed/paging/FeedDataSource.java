package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetBusinessResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetFarmerResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetSpecialistResponse;
import com.androidpopcorn.tenx.freshciboapp.utils.NetworkState;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class FeedDataSource extends PageKeyedDataSource<Integer, GetFeedResponse.FeedData> {


    private static final String TAG = "FeedDataSource";

    private AppDataManager appDataManager;
    private MutableLiveData networkState;
    private MutableLiveData initialLoading;
    public static final int LIMIT_PAGE = 5;


//    temp
    private List<GetFeedResponse.FeedData> mList;


    public FeedDataSource(AppDataManager appDataManager) {
        this.appDataManager = appDataManager;
        networkState = new MutableLiveData();
        initialLoading = new MutableLiveData();
        mList = new ArrayList<>();
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, GetFeedResponse.FeedData> callback){
        initialLoading.postValue(NetworkState.LOADING);
        networkState.postValue(NetworkState.LOADING);

//        first clear the list
        mList.clear();

        appDataManager.getGetAllFeedsPaged(LIMIT_PAGE, 1).subscribeOn(Schedulers.io()).concatMap((Function<Response<GetFeedResponse>, ObservableSource<GetFeedResponse.FeedData>>) responseResponse -> {
            if(responseResponse.code() == 200){
                return Observable.fromIterable(responseResponse.body().getFeeds());
            }
            return null;
        }).concatMap((Function<GetFeedResponse.FeedData, ObservableSource<GetFeedResponse.FeedData>>) response -> {

            String type = response.getUser_type();
            float id = response.getUser_id();
            Observable<GetFeedResponse.FeedData> obs2 = Observable.create(emmiter -> {
                emmiter.onNext(response);
            });

            if(TextUtils.equals(type.toLowerCase(), "business")) {
                Observable<Response<GetBusinessResponse>> obs1 = appDataManager.getBusinessById((int) id);

                return Observable.zip(obs2, obs1, (feedData, profileData) -> {
                    GetFeedResponse.FeedData current = feedData;
                    if(profileData.code() == 200){
                        current.setUser_name(profileData.body().getBusinessData().getBusiness_name());
                        current.setUser_image(profileData.body().getBusinessData().getBusiness_image_path());
                    }
                    return current;
                });



            }else if (TextUtils.equals(type.toLowerCase(), "specialist")) {
                Observable<Response<GetSpecialistResponse>> obs1 = appDataManager.getExpertById((int) id);
                return Observable.zip(obs2, obs1, (feedData, profileData) -> {
                    GetFeedResponse.FeedData current = feedData;
                    if(profileData.code() == 200){
                        current.setUser_name(profileData.body().getSpecialistData().getSpecialist_full_name());
                        current.setUser_image(profileData.body().getSpecialistData().getSpecialist_image_path());
                    }
                    return current;
                });




            }else if(TextUtils.equals(type.toLowerCase(), "farmer")) {
                Observable<Response<GetFarmerResponse>> obs1 = appDataManager.getFarmerById((int) id);
                return Observable.zip(obs2, obs1, (feedData, profileData) -> {
                    GetFeedResponse.FeedData current = feedData;
                    if(profileData.code() == 200){
                        current.setUser_name(profileData.body().getFarmerData().getFarmerFullName());
                        current.setUser_image(profileData.body().getFarmerData().getFarmerImage());
                    }
                    return current;
                });

            }else {
                return obs2;
            }

        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<GetFeedResponse.FeedData>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(GetFeedResponse.FeedData response) {
                if(response != null){
                    mList.add(response);
                }
            }

            @Override
            public void onError(Throwable e) {
                initialLoading.postValue(new NetworkState(NetworkState.Status.FAILED, e.getMessage()));
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, e.getMessage()));
            }

            @Override
            public void onComplete() {

                    try {
                        callback.onResult(mList, null, 2);
                        initialLoading.postValue(NetworkState.LOADED);
                        networkState.postValue(NetworkState.LOADED);

                    }catch (NullPointerException e){
                        Log.d(TAG, "onNext: no feeds found");
                        initialLoading.postValue(new NetworkState(NetworkState.Status.FAILED, e.getMessage()));
                        networkState.postValue(new NetworkState(NetworkState.Status.FAILED, e.getMessage()));
                    }


            }
        });

    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, GetFeedResponse.FeedData> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, GetFeedResponse.FeedData> callback) {


        Log.d(TAG, "loadAfter: loading new page");
        networkState.postValue(NetworkState.LOADING);

//        clear this temp list before adding items
        mList.clear();

        int nextkey = params.key + 1;

        appDataManager.getGetAllFeedsPaged(LIMIT_PAGE, params.key).subscribeOn(Schedulers.io()).concatMap((Function<Response<GetFeedResponse>, ObservableSource<GetFeedResponse.FeedData>>) responseResponse -> {
            if(responseResponse.code() == 200){
                return Observable.fromIterable(responseResponse.body().getFeeds());
            }
            return null;
        }).concatMap((Function<GetFeedResponse.FeedData, ObservableSource<GetFeedResponse.FeedData>>) response -> {

            String type = response.getUser_type();
            float id = response.getUser_id();
            Observable<GetFeedResponse.FeedData> obs2 = Observable.create(emmiter -> {
                emmiter.onNext(response);
            });

            if(TextUtils.equals(type.toLowerCase(), "business")) {
                Observable<Response<GetBusinessResponse>> obs1 = appDataManager.getBusinessById((int) id);

                return Observable.zip(obs2, obs1, (feedData, profileData) -> {
                    GetFeedResponse.FeedData current = feedData;
                    if(profileData.code() == 200){
                        current.setUser_name(profileData.body().getBusinessData().getBusiness_name());
                        current.setUser_image(profileData.body().getBusinessData().getBusiness_image_path());
                    }
                    return current;
                });



            }else if (TextUtils.equals(type.toLowerCase(), "specialist")) {
                Observable<Response<GetSpecialistResponse>> obs1 = appDataManager.getExpertById((int) id);
                return Observable.zip(obs2, obs1, (feedData, profileData) -> {
                    GetFeedResponse.FeedData current = feedData;
                    if(profileData.code() == 200){
                        current.setUser_name(profileData.body().getSpecialistData().getSpecialist_full_name());
                        current.setUser_image(profileData.body().getSpecialistData().getSpecialist_image_path());
                    }
                    return current;
                });




            }else if(TextUtils.equals(type.toLowerCase(), "farmer")) {
                Observable<Response<GetFarmerResponse>> obs1 = appDataManager.getFarmerById((int) id);
                return Observable.zip(obs2, obs1, (feedData, profileData) -> {
                    GetFeedResponse.FeedData current = feedData;
                    if(profileData.code() == 200){
                        current.setUser_name(profileData.body().getFarmerData().getFarmerFullName());
                        current.setUser_image(profileData.body().getFarmerData().getFarmerImage());
                    }
                    return current;
                });

            }else {
                return obs2;
            }

        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<GetFeedResponse.FeedData>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(GetFeedResponse.FeedData response) {
                if(response != null){
                    mList.add(response);
                }
            }

            @Override
            public void onError(Throwable e) {
                initialLoading.postValue(new NetworkState(NetworkState.Status.FAILED, e.getMessage()));
                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, e.getMessage()));
            }

            @Override
            public void onComplete() {

                try {
                    callback.onResult(mList, nextkey);
                    initialLoading.postValue(NetworkState.LOADED);
                    networkState.postValue(NetworkState.LOADED);

                }catch (NullPointerException e){
                    Log.d(TAG, "onNext: no feeds found");
                    initialLoading.postValue(new NetworkState(NetworkState.Status.FAILED, e.getMessage()));
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, e.getMessage()));
                }


            }
        });

    }

    public MutableLiveData getNetworkState() {
        return networkState;
    }

    public MutableLiveData getInitialLoading() {
        return initialLoading;
    }
}
