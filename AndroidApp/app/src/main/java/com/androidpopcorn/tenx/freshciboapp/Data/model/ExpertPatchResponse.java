package com.androidpopcorn.tenx.freshciboapp.Data.model;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;
import com.google.gson.annotations.SerializedName;

public class ExpertPatchResponse {


    @SerializedName("content")
    String message;


    @SerializedName("updates")
    ExpertPatchBody updates;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ExpertPatchBody getUpdates() {
        return updates;
    }

    public void setUpdates(ExpertPatchBody updates) {
        this.updates = updates;
    }
}
