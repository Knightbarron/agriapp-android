package com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp;

import com.google.gson.annotations.SerializedName;

public class OtpResponse {
    @SerializedName("Status")
    private String Status;

    @SerializedName("Details")
    private String Details;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

}
