package com.androidpopcorn.tenx.freshciboapp.Data.model.farmer;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FarmerCategoryResponse {

    @SerializedName("produce_categories")
    private ArrayList<FarmCategory> body;

    public ArrayList<FarmCategory> getBody() {
        return body;
    }

    public void setBody(ArrayList<FarmCategory> body) {
        this.body = body;
    }

    public class FarmCategory{

        @SerializedName("farmer_farm_category_name")
        private String FarmCategory;

        public String getFarmCategory() {
            return FarmCategory;
        }

        public void setFarmCategory(String farmCategory) {
            FarmCategory = farmCategory;
        }
    }



}
