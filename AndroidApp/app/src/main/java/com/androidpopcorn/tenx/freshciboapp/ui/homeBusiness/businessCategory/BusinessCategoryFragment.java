package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.base.BusinessBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory.adapter.BusinessCategory;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory.adapter.BusinessCategoryAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessCategoryFragment extends Fragment {


    @Inject
    HomeBusinessViewModel viewModel;

    RecyclerView recyclerView;

    ArrayList<BusinessCategory> items;

    ArrayList<String> selectedItemsArray;

    @Inject
    BusinessCategoryAdapter adapter;


    private static final String TAG = "BusinessCategoryFragmen";

    @Inject
    BusinessBaseFragment businessBaseFragment;


    Unbinder unbinder;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;

    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) v.getTag();
            int position = viewHolder.getAdapterPosition();

            String selectedItem = items.get(position).getData();

            Log.d(TAG, "onClick: " + items.get(position).getData());

            Toast.makeText(getActivity(), "You Selected : " + selectedItem, Toast.LENGTH_SHORT).show();
            selectedItemsArray.add(selectedItem);


        }
    };

    @Inject
    public BusinessCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_category, container, false);


        recyclerView = view.findViewById(R.id.recycler_view);
        loadItems();
        setUpRecycler(recyclerView, adapter);


        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");

        unbinder = ButterKnife.bind(this, view);

        selectedItemsArray = new ArrayList<>();

        subscribeObservers();


        return view;
    }

    private void subscribeObservers() {

        viewModel.getBusinessCategoryStatus().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean){
                    //TODO test
                    Toast.makeText(getActivity(), "categories saved", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onChanged: Saved categories");
                    goToBase();
                }else{
                    Log.d(TAG, "onChanged: Not saved");
                }
            }
        });

    }

    private void setUpRecycler(RecyclerView recyclerView, BusinessCategoryAdapter adapter) {

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(onItemClickListener);
        adapter.updateListItems(loadItems());
    }

    private ArrayList<BusinessCategory> loadItems() {
        items = new ArrayList<>();
        items.add(new BusinessCategory(R.drawable.seeds_, "Seeds Supplier"));
         items.add(new BusinessCategory(R.drawable.fertilizer_, "Fertilizer \n Supplier"));
        items.add(new BusinessCategory(R.drawable.farm_tools_machinery, "Farm tools \n& Machinery"));
        items.add(new BusinessCategory(R.drawable.pesticide_insecticide, "Pesticides &\n Insecticides"));
        items.add(new BusinessCategory(R.drawable.organic_, "Organic \n Farming"));
        items.add(new BusinessCategory(R.drawable.soil_, "Soil testing \n & Monitoring"));
        items.add(new BusinessCategory(R.drawable.harvesting_storage, "Harvesting \n& Storage"));
        items.add(new BusinessCategory(R.drawable.contract_farming, "Contract \n Farming"));
        items.add(new BusinessCategory(R.drawable.smart_farming, "Smart Farm \n Services"));
        items.add(new BusinessCategory(R.drawable.crop_insurance_finance, "Finance & \n Crop Insurance"));
        items.add(new BusinessCategory(R.drawable.value_addition_marketing, "Value addition \n & Marketing"));
        items.add(new BusinessCategory(R.drawable.training_assistance, "Farmer Training \n & Assistance"));
        items.add(new BusinessCategory(R.drawable.farm_asset_renting, "Farm Asset \n Renting"));
        items.add(new BusinessCategory(R.drawable.logistics_, "Farm \nLogistics"));
        items.add(new BusinessCategory(R.drawable.dairy_, "Animal \n Husbandry"));
        items.add(new BusinessCategory(R.drawable.value_addition_marketing, "Others"));
        return items;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void goToBase() {
        String backStateName = businessBaseFragment.getClass().toString();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(R.id.container, businessBaseFragment);
        transaction.addToBackStack(backStateName);
        transaction.commit();
    }

    @OnClick(R.id.edit_btn_continue)
    public void onViewClicked() {

        viewModel.saveBusinessCategory(new BusinessCategoryBody(selectedItemsArray));
    }
}
