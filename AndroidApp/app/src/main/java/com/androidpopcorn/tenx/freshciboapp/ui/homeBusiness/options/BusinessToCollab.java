package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.options;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessViewModel;
import com.androidpopcorn.tenx.freshciboapp.utils.RoundedCornersTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessToCollab extends Fragment {


    @Inject
    HomeBusinessViewModel viewModel;

    private static final String TAG = "BusinessToCollaborate";
    @BindView(R.id.ib3)
    ImageButton ib3;
    @BindView(R.id.edit_btn_continue)
    ImageButton collabPage;
    Unbinder unbinder;

    public static int sCorner = 50;
    public static int sMargin = 20;

    @Inject
    FragmentManager fm;
    @BindView(R.id.image)
    ImageView image;


    @Inject
    public BusinessToCollab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_to_collab, container, false);


        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIWEMODEL is NUll");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        unbinder = ButterKnife.bind(this, view);

        onClickMethods();
        setUpImage();


        return view;
    }

    private void onClickMethods() {

        ib3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeFragments(fm, new BusinessToAddPackage());
            }
        });

        collabPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO I dont know the receiving end
                Toast.makeText(getActivity(), "Feature coming soon...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void initializeFragments(FragmentManager fm, Fragment frag) {
        String backStateName = frag.getClass().toString();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, frag);
        ft.addToBackStack(backStateName);
        ft.commit();
    }

    public void setUpImage() {
        Glide.with(this).load(R.drawable.get_started_login)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(image);
    }

}
