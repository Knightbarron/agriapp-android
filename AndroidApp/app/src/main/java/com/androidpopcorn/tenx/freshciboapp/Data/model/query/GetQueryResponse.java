package com.androidpopcorn.tenx.freshciboapp.Data.model.query;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class GetQueryResponse {
    @SerializedName("queries")
    private ArrayList<QueryItem> queries;

    @SerializedName("specialist_id")
    private int specialistID;

    public ArrayList<QueryItem> getQueries() {
        return queries;
    }

    public void setQueries(ArrayList<QueryItem> queries) {
        this.queries = queries;
    }

    public int getSpecialistID() {
        return specialistID;
    }

    public void setSpecialistID(int specialistID) {
        this.specialistID = specialistID;
    }

    public class QueryItem {


        @SerializedName("query_id")
        private float query_id;

        @SerializedName("query_question")
        private String query_question;

        @SerializedName("query_category")
        private String query_category = null;

        @SerializedName("query_image_path")
        private String query_image_path = null;

        @SerializedName("query_date_created")
        private String query_date_created;

        @SerializedName("query_date_modified")
        private String query_date_modified;

        @SerializedName("farmer_farmer_id")
        private float farmer_farmer_id;

        @SerializedName("conversation_id")
        private int conversationID;

        @SerializedName("comments")
        private ArrayList<GetCommentResponse.CommentItem> comments;

        public int getConversationID() {
            return conversationID;
        }

        public void setConversationID(int conversationID) {
            this.conversationID = conversationID;
        }

        public float getQuery_id() {
            return query_id;
        }

        public void setQuery_id(float query_id) {
            this.query_id = query_id;
        }

        public String getQuery_question() {
            return query_question;
        }

        public void setQuery_question(String query_question) {
            this.query_question = query_question;
        }

        public String getQuery_category() {
            return query_category;
        }

        public void setQuery_category(String query_category) {
            this.query_category = query_category;
        }

        public String getQuery_image_path() {
            return query_image_path;
        }

        public void setQuery_image_path(String query_image_path) {
            this.query_image_path = query_image_path;
        }

        public String getQuery_date_created() {
            return query_date_created;
        }

        public void setQuery_date_created(String query_date_created) {
            this.query_date_created = query_date_created;
        }

        public String getQuery_date_modified() {
            return query_date_modified;
        }

        public void setQuery_date_modified(String query_date_modified) {
            this.query_date_modified = query_date_modified;
        }

        public float getFarmer_farmer_id() {
            return farmer_farmer_id;
        }

        public void setFarmer_farmer_id(float farmer_farmer_id) {
            this.farmer_farmer_id = farmer_farmer_id;
        }

        public ArrayList<GetCommentResponse.CommentItem> getComments() {
            return comments;
        }

        public void setComments(ArrayList<GetCommentResponse.CommentItem> comments) {
            this.comments = comments;
        }
    }
}
