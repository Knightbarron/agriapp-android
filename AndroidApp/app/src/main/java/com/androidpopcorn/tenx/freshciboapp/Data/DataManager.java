package com.androidpopcorn.tenx.freshciboapp.Data;

import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiHelper;
import com.androidpopcorn.tenx.freshciboapp.Data.prefs.PreferencesHelper;

import java.util.List;

import io.reactivex.Observable;

public interface DataManager extends ApiHelper , PreferencesHelper {

}
