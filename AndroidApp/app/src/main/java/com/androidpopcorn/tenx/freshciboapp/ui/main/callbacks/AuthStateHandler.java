package com.androidpopcorn.tenx.freshciboapp.ui.main.callbacks;

public interface AuthStateHandler {

    public void onAuthStateChanged(Boolean isAuthenticated);

    void onRegistrationResult(Boolean isRegistered);

}
