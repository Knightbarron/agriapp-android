package com.androidpopcorn.tenx.freshciboapp.ui.main.regis;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyBusiness;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyExpert;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyFarmer;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisFragment extends Fragment {

    @Inject
    MainViewModel viewModel;


    private static final String TAG = "RegisFragment";
    @BindView(R.id.errormsg)
    TextView errormsg;
    @BindView(R.id.register_et_name)
    TextInputEditText registerEtName;
    @BindView(R.id.register_et_phone)
    TextInputEditText registerEtPhone;
    @BindView(R.id.register_et_password)
    TextInputEditText registerEtPassword;
    @BindView(R.id.register_et_cnf_password)
    TextInputEditText registerEtCnfPassword;
    @BindView(R.id.register_btn_signup)
    Button registerBtnSignup;
    Unbinder unbinder;


    @Inject
    public RegisFragment() {
        // Required empty public constructor
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_regis, container, false);
        unbinder =  ButterKnife.bind(this, v);

        errormsg.setVisibility(View.INVISIBLE);

        registerEtPhone.setText(viewModel.getUserPhone());



        return v;
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);

        super.onAttach(context);
    }

    @OnClick(R.id.register_btn_signup)
    public void onClickRegistration(){

        String user = viewModel.getUserType();
        Log.d(TAG, "onClickRegistration: User is a : " + user);


        if(user.equals("farmer"))
            doRegistrationFarmer();
        else if(user.equals("expert"))
            doRegistrationSpecialist();
        else if(user.equals("business"))
            doRegistrationBusiness();

    }

    public void doRegistrationBusiness() {

        String name  = registerEtName.getText().toString();
        String password  = registerEtPassword.getText().toString();
        String confirmPassword  = registerEtCnfPassword.getText().toString();
        String phoneNumber = registerEtPhone.getText().toString();

        if(TextUtils.isEmpty(name)
                || TextUtils.isEmpty(phoneNumber)
                || TextUtils.isEmpty(confirmPassword)
                || TextUtils.isEmpty(password)){

            setInfo("All Fields are necessary");
            return;
        }

        if(!password.equals(confirmPassword)){
            setInfo("Passwords did not match");
            return;
        }
        if(password.length()<6){
            setInfo("Password should have minimum 6 characters");
            return;
        }

        if(!TextUtils.equals(password,confirmPassword)){
            setInfo("Passwords do not match");
            return;
        }else{
            errormsg.setVisibility(View.INVISIBLE);
            viewModel.doRegistrationBusiness(new RegistrationBodyBusiness(name, password,phoneNumber));
        }

    }

    public void doRegistrationFarmer() {

        String name  = registerEtName.getText().toString();
        String password  = registerEtPassword.getText().toString();
        String confirmPassword  = registerEtCnfPassword.getText().toString();
        String phoneNumber = registerEtPhone.getText().toString();

        if(TextUtils.isEmpty(name)
                || TextUtils.isEmpty(phoneNumber)
                || TextUtils.isEmpty(confirmPassword)
                || TextUtils.isEmpty(password)){

            setInfo("All Fields are necessary");
            return;
        }

        if(!password.equals(confirmPassword)){
            setInfo("Passwords did not match");
            return;
        }
        if(password.length()<6){
            setInfo("Password should have minimum 6 characters");
            return;
        }

        if(!TextUtils.equals(password,confirmPassword)){
            setInfo("Passwords do not match");
            return;
        }else{
            errormsg.setVisibility(View.INVISIBLE);
            viewModel.doRegistrationFarmer(new RegistrationBodyFarmer(name, password,phoneNumber));
        }

    }

    public void doRegistrationSpecialist() {

        String name  = registerEtName.getText().toString();
        String password  = registerEtPassword.getText().toString();
        String confirmPassword  = registerEtCnfPassword.getText().toString();
        String phoneNumber = registerEtPhone.getText().toString();



        if(TextUtils.isEmpty(name)
                || TextUtils.isEmpty(phoneNumber)
                || TextUtils.isEmpty(confirmPassword)
                || TextUtils.isEmpty(password)){

            setInfo("All Fields are necessary");

            return;
        }

        if(!password.equals(confirmPassword)){
            setInfo("Passwords did not match");
            return;
        }
        if(password.length()<6){
            setInfo("Password should have minimum 6 characters");
            return;
        }

        if(!TextUtils.equals(password,confirmPassword)){
            setInfo("Passwords do not match");
            return;
        }else{
            errormsg.setVisibility(View.INVISIBLE);
            viewModel.doRegistrationSpecialist(new RegistrationBodyExpert(name, password,phoneNumber));
        }


    }

    public void setInfo(String text) {
        errormsg.setVisibility(View.VISIBLE);
        errormsg.setText(text);
    }
}
