package com.androidpopcorn.tenx.freshciboapp.ui.main.login;


import android.content.Context;

import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.androidpopcorn.tenx.freshciboapp.di.scope.FragmentScope;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {



    @Provides
    @FragmentScope
    @ActivityContext
    Context provideContext(LoginFragment loginFragment){
        return loginFragment.getActivity();
    }


}
