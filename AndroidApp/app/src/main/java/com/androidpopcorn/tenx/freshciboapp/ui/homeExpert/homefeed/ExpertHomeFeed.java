package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.adapter.FeedAdapter;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.paging.FeedPagingAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertHomeFeed extends Fragment {

    private static final String TAG = "ExpertHomeFeed";


    @Inject
    HomeExpertViewModel viewModel;

//    @Inject
//    FeedAdapter adapter;

    @Inject
    FeedPagingAdapter feedPagingAdapter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    Unbinder unbinder;

    @Inject
    public ExpertHomeFeed() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expert_home_feed, container, false);


        unbinder = ButterKnife.bind(this, view);
        viewModel.getAllFeedsPosts();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(feedPagingAdapter);

        viewModel.getFeedlivedata().observe(getActivity(), data -> {
            feedPagingAdapter.submitList(data);
        });


        viewModel.getNetworkState().observe(getActivity(), state -> {
            feedPagingAdapter.setNetworkState(state);
        });



//        viewModel.getOnGetAllFeeds().observe(getActivity(), res -> {
//            adapter.updateList(res.getFeeds());
//        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
