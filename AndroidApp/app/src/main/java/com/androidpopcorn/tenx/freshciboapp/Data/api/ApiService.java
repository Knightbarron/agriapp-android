package com.androidpopcorn.tenx.freshciboapp.Data.api;


import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.AllExpertsResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.NotificationResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceBody;

import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.login.LoginResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.OtpResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyBusiness;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyExpert;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyFarmer;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.ExpertPatchResponse;


import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.VerifyResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.TokenResponse;


import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.PostFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetBusinessResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetFarmerResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetSpecialistResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetCommentResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetQueryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.verifyUser.UserObject;


import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiService {

     String BASE_URL = "http://10.0.2.2:5000";


     //Registration

    @POST("/api/v1/registration/farmer")
    Observable<Response<RegistrationResponse>> registerFarmer(@Body RegistrationBodyFarmer body);
    @POST("/api/v1/registration/specialist")
    Observable<Response<RegistrationResponse>> registerExpert(@Body RegistrationBodyExpert body);
    @POST("/api/v1/registration/business")
    Observable<Response<RegistrationResponse>> registerBusiness(@Body RegistrationBodyBusiness body);


    //LOGIN

    @FormUrlEncoded
    @POST("/api/v1/login/farmer")
    Observable<Response<LoginResponse>> loginFarmer(@Field("farmer_phone") String phone,
                                                    @Field("farmer_password") String password);
    @FormUrlEncoded
    @POST("/api/v1/login/specialist")
    Observable<Response<LoginResponse>> loginExpert(@Field("specialist_phone") String phone,
                                                    @Field("specialist_password") String password);

    @FormUrlEncoded
    @POST("/api/v1/login/business")
    Observable<Response<LoginResponse>> loginBusiness(@Field("business_phone") String phone,
                                                      @Field("business_password") String password);





    //OTP

    @POST("/api/v1/otp/new/{phone_number}")
    Observable<Response<OtpResponse>> sendOtpRequest(@Path("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("/api/v1/otp/verify")
    Observable<Response<OtpResponse>> verifyOtp(@Field("session_id") String session_id, @Field("otp") String otp) ;



    //Token


    @POST("/api/v1/verify")
    Observable<Response<VerifyResponse>> verifyAuth();

    @FormUrlEncoded
    @POST("/api/v1/token")
    Observable<Response<TokenResponse>> saveFcmToken(@Field("token") String fcmToken);

    @FormUrlEncoded
    @POST("/authenticate/customer/checkauth")
    Observable<Response<DefaultResponse>> validateCustomerToken(@Field("email") String email,
                                                                @Field("token") String token);

    @POST("/api/v1/verify")
    Observable<Response<UserObject>> verifyUserType();

    //FARMER
    @POST("/api/v1/profile/farmer/farmcategory")
    Observable<Response<DefaultResponse>> saveFarmCategory(@Body FarmerCategoryBody body);

    //TODO check
    @POST("/api/v1/profile/farmer/produce")
    Observable<Response<DefaultResponse>> saveProduceDetails(@Body FarmerProduceBody body);


    @PATCH("/api/v1/profile/farmer")
    Observable<Response<DefaultResponse>> saveCompleteProfileDetails(@Body FarmerCompleteProfileBody body);


    @GET("/api/v1/profile/farmer/me")
    Observable<Response<FarmerProfileResponse>> getCurrentFarmerProfile();

    @GET("api/v1/profile/farmer/produce/farmerid={farmerid}")
    Observable<Response<FarmerProduceResponse>> getCurrentFarmerProduce(@Path("farmerid") int farmerId);

    @GET("/api/v1/profile/farmer/farmcategory/farmerid={farmerid}")
    Observable<Response<FarmerCategoryResponse>> getCurrentFarmerCategory(@Path("farmerid") int farmerId);


    @PATCH("/api/v1/profile/farmer/produce/produceid={produceid}")
    Observable<Response<DefaultResponse>> patchFarmerProduce(@Path("produceid") int produceid,@Body FarmerProduceBody body);

    //EXPERTS

    //    expert complete profile
    @PATCH("/api/v1/profile/specialist")
    Observable<Response<ExpertPatchResponse>> updateExpertProfile(@Body ExpertPatchBody data);

    @POST("/api/v1/package")
    Observable<Response<ExpertPackageResponse.ExpertPackage>> saveExpertPackage(@Body ExpertPackageBody body);

    @GET("/api/v1/profile/specialist")
    Observable<Response<AllExpertsResponse>> getAllExperts();

    @GET("/api/v1/profile/specialist/me")
    Observable<Response<ExpertProfileResponse>> getCurrentExpertProfile();

    @GET("/api/v1/package/me")
    Observable<Response<ExpertPackageResponse>> getCurrentExpertPackage();



    //BUSINESS
    @POST("/api/v1/profile/business/category")
    Observable<Response<BusinessCategoryBody>> saveBusinessCategoryBody(@Body BusinessCategoryBody body);

    @POST("/api/v1/profile/business/products")
    Observable<Response<DefaultResponse>> saveBusinessPackageBody(@Body BusinessPackageBody body);

    @GET("/api/v1/profile/business/category")
    Observable<Response<BusinessCategoryResponse>> getBusinessCategoryList();

    @GET("/api/v1/profile/business/products/me")
    Observable<Response<BusinessPackageResponse>> getBusinessPackageList();

    @GET("/api/v1/profile/business/me")
    Observable<Response<BusinessProfileResponse>> getBusinessProfileDetails();

    //TODO need to use this
    @PATCH("/api/v1/profile/business")
    Observable<Response<DefaultResponse>> patchBusinessProfile(@Body BusinessProfileBody body);






    //MULTIPART

    //    documents upload
    // for experts -- companyid -- certification id   : comp and cert
    // for business   license and bcert
    @Multipart
    @POST("/api/v1/profile/upload/docs&type={doctype}")
    Observable<Response<DefaultResponse>> uploadDocuments(@Part MultipartBody.Part file, @Part("doc") RequestBody requestBody , @Path("doctype") String doctype);

    @Multipart
    @POST("/api/v1/profile/upload")
    Observable<Response<DefaultResponse>> uploadProfilePicture(@Part MultipartBody.Part file, @Part("description") RequestBody body);

    @Multipart
    @POST("/api/v1/feed/new")
    Observable<Response<PostFeedResponse>>  postCreatePost(@Part MultipartBody.Part file, @Part("title")RequestBody titleBody, @Part("content") RequestBody contentBody, @Part("category") RequestBody categoryBody);

    @Multipart
    @POST("/api/v1/ad/new")
    Observable<Response<DefaultResponse>> createNewPost(@Part MultipartBody.Part file,@Part("title")RequestBody titleBody,@Part("content") RequestBody contentBody);

    //TODO add the category part
    @Multipart
    @POST("/api/v1/query/new")
    Observable<Response<DefaultResponse>> createNewQuery(@Part MultipartBody.Part file,@Part("question")RequestBody queryBody);



    //NOTIFICATION
    @GET("/api/v1/notification/me")
    Observable<Response<NotificationResponse>> getNotificationsForUser();







   //FEED
    //    upload feed
//   @GET("/api/v1/feed")
//   Observable<Response<GetFeedResponse>> getGetAllFeeds();

    //    get queries for specialist
    @GET("/api/v1/feed/all")
    Observable<Response<GetFeedResponse>> getGetAllFeeds();



    @GET("/api/v1/feed/all&limit={limit}&page={page}")
    Observable<Response<GetFeedResponse>> getGetAllFeedsPaged(@Path("limit") int limit , @Path("page") int page);

//    get queries for specialist
    @GET("/api/v1/query/specialist")
    Observable<Response<GetQueryResponse>> getQueriesForSpecialist();

    //    get comments for conversation
    @GET("/api/v1/query/comment/cid={convid}")
    Observable<Response<GetCommentResponse>> getCommentsForConv(@Path("convid") String convid);




    @GET("/api/v1/profile/farmer/id={id}")
    Observable<Response<GetFarmerResponse>> getFarmerById(@Path("id") int farmerid);

    @GET("/api/v1/profile/specialist/id={id}")
    Observable<Response<GetSpecialistResponse>> getExpertById(@Path("id") int expertID);

    @GET("/api/v1/profile/business/id={id}")
    Observable<Response<GetBusinessResponse>> getBusinessById(@Path("id") int businessID);








}
