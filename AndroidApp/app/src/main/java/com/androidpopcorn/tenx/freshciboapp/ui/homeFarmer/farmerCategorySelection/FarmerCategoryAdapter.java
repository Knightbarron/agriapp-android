package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

public class FarmerCategoryAdapter extends RecyclerView.Adapter<FarmerCategoryAdapter.FarmerCategoryViewHolder> {

    private static final String TAG = "FarmerCategoryAdapter";

    private List<CategoryClass> mList;

    public SparseBooleanArray mSelected = new SparseBooleanArray();

    public ArrayList<String> selectedItems = new ArrayList<>();

    @Inject
    public FarmerCategoryAdapter() {
        mList = new ArrayList<>();
    }

    @NonNull
    @Override
    public FarmerCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem_farmer_category,
                viewGroup,false);
        return new FarmerCategoryViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull FarmerCategoryViewHolder farmerCategoryViewHolder, int i) {

        farmerCategoryViewHolder.image.setImageResource(mList.get(i).getImage());
        farmerCategoryViewHolder.text.setText(mList.get(i).getData());
        farmerCategoryViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelected.get(i,false)){

                    mSelected.delete(i);
                    farmerCategoryViewHolder.icon.setVisibility(View.INVISIBLE);
                    v.setSelected(false);
                    selectedItems.remove(new String(mList.get(i).getData()));
                    Log.d(TAG, "onClick: Removed item : " + mList.get(i).getData());
                }
                else{
                    mSelected.put(i,true);
                    v.setSelected(true);
                    farmerCategoryViewHolder.icon.setVisibility(View.VISIBLE);
                    selectedItems.add(mList.get(i).getData());

                    Log.d(TAG, "onClick: Added item : " + mList.get(i).getData());

                }
                Log.d(TAG, "onClick: SIZE IS : " + selectedItems.size());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void updateListItems(List<CategoryClass> mList) {
        this.mList.addAll(mList);
        notifyDataSetChanged();


    }


    public class FarmerCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image,icon;
        TextView text;
        RelativeLayout relativeLayout;



        public FarmerCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setTag(this);
            image = itemView.findViewById(R.id.imageView);
            text  = itemView.findViewById(R.id.textView);
            icon = itemView.findViewById(R.id.selection);
            relativeLayout = itemView.findViewById(R.id.rel_layout1);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {


        }
    }
}
