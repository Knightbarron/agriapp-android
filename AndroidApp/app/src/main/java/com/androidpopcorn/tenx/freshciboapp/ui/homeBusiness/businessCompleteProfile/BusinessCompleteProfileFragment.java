package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCompleteProfile;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.uploadDocuments.BusinessUploadDocumentsFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessCompleteProfileFragment extends Fragment {


    @Inject
    HomeBusinessViewModel viewModel;

    private static final String TAG = "BusinessCompleteProfile";
    @BindView(R.id.et_business_name)
    EditText etBusinessName;
    @BindView(R.id.et_business_location)
    EditText etBusinessLocation;
    @BindView(R.id.specialization_tv)
    TextView businessTv;
    @BindView(R.id.spinner_specialization)
    Spinner spinnerBusinessType;
    @BindView(R.id.role_tv)
    TextView roleTv;
    @BindView(R.id.spinner_role)
    Spinner spinnerRole;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;
    @BindView(R.id.et_business_mail)
    EditText busMail;
    Unbinder unbinder;

    ArrayList<String> businessType;

    String typeBusiness;
    String typeRole;

    @BindView(R.id.type1)
    CheckBox type1;
    @BindView(R.id.type2)
    CheckBox type2;
    @BindView(R.id.type3)
    CheckBox type3;
    @BindView(R.id.type4)
    CheckBox type4;
    @BindView(R.id.type5)
    CheckBox type5;
    @BindView(R.id.type6)
    CheckBox type6;

    @Inject
    BusinessUploadDocumentsFragment businessUploadDocumentsFragment;


    @Inject
    public BusinessCompleteProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_complete_profile, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        unbinder = ButterKnife.bind(this, view);


        initSpinners();

        observerForPatchProfile();
        return view;
    }

    private void observerForPatchProfile() {

        viewModel.getStatusPatchBusProfile().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean){
                    Toast.makeText(getActivity(), "The profile has been updated", Toast.LENGTH_SHORT).show();
                    goToUploads();
                }else{
                    Toast.makeText(getActivity(), "Unsuccessful updating the profile", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //TODO test
    public void goToUploads() {

        String backStateName = businessUploadDocumentsFragment.getClass().toString();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(R.id.container, businessUploadDocumentsFragment);
        transaction.addToBackStack(backStateName);

        transaction.commit();
    }

    private int knowBusinessType(){
        businessType  = new ArrayList<>();
        int i =0;

        if(type1.isChecked()){
            i++;
            businessType.add("Sell to farmers and other services");
        }if(type2.isChecked()){
            i++;
            businessType.add("Buy from farmers and other services");
        }    if(type3.isChecked()){
            i++;
            businessType.add("Both Buy and Sell");
        }if(type4.isChecked()){
            i++;
            businessType.add("Not for profit");
        }    if(type5.isChecked()){
            i++;
            businessType.add("Advertising and Marketing");
        }if(type6.isChecked()){
            i++;
            businessType.add("Research and Lab testing");
        }
        return i;
    }

    private void initSpinners() {

        String[] businessTypedata = new String[]{"Partnership Firm", "Private Company", "Non-Profit Organization", "Other"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, businessTypedata);
        spinnerBusinessType.setAdapter(adapter);
        typeBusiness = spinnerBusinessType.getSelectedItem().toString();
        spinnerBusinessType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeBusiness = spinnerBusinessType.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Specialization : " + typeBusiness);
                businessTv.setText(typeBusiness);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        String[] roleCompany = new String[]{"Owner", "Partner", "Employee"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, roleCompany);
        spinnerRole.setAdapter(adapter1);
        typeRole = spinnerRole.getSelectedItem().toString();

        spinnerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeRole = spinnerRole.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Specialization : " + typeRole);
                roleTv.setText(typeRole);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.edit_btn_continue)
    public void onViewClicked() {

        int count =  knowBusinessType();
        if(count==0){
            Toast.makeText(getActivity(), "You must select one", Toast.LENGTH_SHORT).show();}
        else if (count>2){
            Toast.makeText(getActivity(), "You can only select two", Toast.LENGTH_SHORT).show();
        }
        else{
            String busName =  etBusinessName.getText().toString();
            String busLocation = etBusinessLocation.getText().toString();
            String busEMail = busMail.getText().toString();
            StringBuilder sb = new StringBuilder();
            for (String s : businessType)
            {
                sb.append(s);
                sb.append("\t, ");
            }
            Log.d(TAG, "onViewClicked: Name : " + busName + "Location : " + busLocation + "Type of Business: " + typeBusiness +
                    "Type of ROle : " + typeRole + "Business styles: " + businessType + " EMail : " + busEMail);
                viewModel.patchBusinessProfile(new BusinessProfileBody(busName,busEMail,busLocation,typeBusiness,sb.toString(),typeRole));
            }

    }
}
