package com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp;

import com.google.gson.annotations.SerializedName;

public class TokenResponse {

    @SerializedName("content")
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
