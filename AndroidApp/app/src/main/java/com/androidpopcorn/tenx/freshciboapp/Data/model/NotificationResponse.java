package com.androidpopcorn.tenx.freshciboapp.Data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotificationResponse {

    @SerializedName("content")
    private String message;

    @SerializedName("id")
    private int id;


    @SerializedName("notifications")
    private ArrayList<NotificationItem> notifications;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<NotificationItem> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<NotificationItem> notifications) {
        this.notifications = notifications;
    }

    public class NotificationItem {

        @SerializedName("notification_message")
        private String message;


        @SerializedName("notification_title")
        private String title;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

}
