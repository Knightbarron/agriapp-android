package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.util.Log;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.PostFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.base.BaseViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.paging.FeedDataFactory;
import com.androidpopcorn.tenx.freshciboapp.utils.NetworkState;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Response;

public class HomeBusinessViewModel extends BaseViewModel {
    private static final String TAG = "HomeBusinessViewModel";
    private AppDataManager appDataManager;

    //paging
    private Executor executor;
    private LiveData<NetworkState> networkStateLiveData;
    private LiveData<PagedList<GetFeedResponse.FeedData>> feedLiveData;
    //get all feed
    private MutableLiveData<GetFeedResponse> onGetAllFeeds;

    public LiveData<NetworkState> getNetworkStateLiveData() {
        return networkStateLiveData;
    }

    public LiveData<PagedList<GetFeedResponse.FeedData>> getFeedLiveData() {
        return feedLiveData;
    }

    public MutableLiveData<GetFeedResponse> getOnGetAllFeeds() {
        return onGetAllFeeds;
    }

    //LiveData to check the status of business category selection
    MutableLiveData<Boolean> statusBusinessCategory;

    public LiveData<Boolean> getBusinessCategoryStatus(){
        if(statusBusinessCategory==null)
            statusBusinessCategory = new MutableLiveData<>();
        return statusBusinessCategory;
    }

    private MutableLiveData<Boolean> statusPackage;

    public LiveData<Boolean> getStatusPackage(){
        if(statusPackage==null)
            statusPackage = new MutableLiveData<>();
        return statusPackage;
    }

    //getting the business profile pic upload status
    private MutableLiveData<Boolean> statusUploadPic;

    public LiveData<Boolean> getStatusUploadPic(){
        if (statusUploadPic == null)
            statusUploadPic = new MutableLiveData<>();
        return statusUploadPic;
    }

    //status for upload of pdf
    private MutableLiveData<Boolean> statusDocumentsUpload;

    public LiveData<Boolean> getStatusDocumentUpload(){
        if (statusDocumentsUpload == null)
            statusDocumentsUpload = new MutableLiveData<>();
        return statusDocumentsUpload;
    }

    //getting the business selected categpries
    private MutableLiveData<ArrayList<BusinessCategoryResponse.CategoryItem>> businessCategoryItems;

    public LiveData<ArrayList<BusinessCategoryResponse.CategoryItem>> getBusinessCategoryItems(){
        if(businessCategoryItems==null)
            businessCategoryItems = new MutableLiveData<>();
        return businessCategoryItems;
    }

    //getting the status for business packages
    private MutableLiveData<String> statusBusinessPackage;

    public LiveData<String> getStatusBusinessPackage(){
        if(statusBusinessPackage == null)
            statusBusinessPackage = new MutableLiveData<>();
        return statusBusinessPackage;
    }

    //getting the packages from the database
    public MutableLiveData<ArrayList<BusinessPackageResponse.BusinessPackageResponseBody>> businessPackageList;

    public LiveData<ArrayList<BusinessPackageResponse.BusinessPackageResponseBody>> getBusinessPackages(){

        if(businessPackageList == null)
            businessPackageList = new MutableLiveData<>();
        return businessPackageList;

    }


    //getting the status for creating ad for business
    private MutableLiveData<Boolean> statusAd;

    public LiveData<Boolean> getStatusAd(){
        if (statusAd == null)
            statusAd = new MutableLiveData<>();
        return  statusAd;
    }

    private MutableLiveData<PostFeedResponse> onNewPostObserver;

    public LiveData<PostFeedResponse> getNewPost(){
        if (onNewPostObserver == null)
            onNewPostObserver = new MutableLiveData<>();
        return onNewPostObserver;
    }

    private MutableLiveData<BusinessProfileResponse.Business> businessProfileData;

    public LiveData<BusinessProfileResponse.Business> getBusinessProfileData(){
        if (businessProfileData == null)
            businessProfileData = new MutableLiveData<>();
        return businessProfileData;
    }

    //Business PRofile Update
    public MutableLiveData<Boolean> statusPatchBusProfile;

    public LiveData<Boolean> getStatusPatchBusProfile(){
        if (statusPatchBusProfile == null)
            statusPatchBusProfile = new MutableLiveData<>();
        return statusPatchBusProfile;
    }

    public HomeBusinessViewModel(AppDataManager appDataManager, Application application) {

        super(appDataManager, application);
        this.appDataManager = appDataManager;
        init();
    }
//used for paging
    private void init() {

        executor =  Executors.newFixedThreadPool(5);
        FeedDataFactory feedDataFactory = new FeedDataFactory(appDataManager);
        networkStateLiveData = Transformations.switchMap(feedDataFactory.getFeedDataSourceMutableLiveData(),
                dataSource->dataSource.getNetworkState());
        PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setInitialLoadSizeHint(5)
                .setPageSize(5).build();
        feedLiveData = (new LivePagedListBuilder(feedDataFactory,pagedListConfig)).setFetchExecutor(executor).build();

    }

    public void saveBusinessCategory(BusinessCategoryBody body){
        appDataManager.saveBusinessCategory(body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<BusinessCategoryBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<BusinessCategoryBody> businessCategoryBodyResponse) {
                        if(businessCategoryBodyResponse.code()==201){

                            if(statusBusinessCategory==null)
                                statusBusinessCategory = new MutableLiveData<>();

                            try{

                                Log.d(TAG, "onNext: Success in saving the categories");
                                statusBusinessCategory.setValue(true);

                            }catch (NullPointerException e){

                                Log.d(TAG, "onNext: Null pointer Exception");
                                statusBusinessCategory.setValue(false);

                            }


                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                        statusBusinessCategory.setValue(false);
                        Log.e(TAG, "onError: ",e);

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Save Business Category call completed");
                    }
                });
    }

    //Saves Business PAckage details
    public void saveBusinessPackageDetails(BusinessPackageBody body) {
        appDataManager.saveBusinessPackage(body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<DefaultResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                        if (statusPackage == null) {
                            statusPackage = new MutableLiveData<>();
                        }

                        if (defaultResponseResponse.code() == 201) {
                            try {


                                statusPackage.setValue(true);
                                Log.d(TAG, "onNext: Success in storing the package");

                            } catch (NullPointerException e) {
                                statusPackage.setValue(false);
                                Log.d(TAG, "onNext: ");
                            }

                        } else {
                            statusPackage.setValue(false);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        statusPackage.setValue(false);
                        Log.d(TAG, "onError: ", e);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Save farmer package details");
                    }
                });
    }

    //get all category details
    public void getBusinessCategory(){
        appDataManager.getBusinessCategory().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<BusinessCategoryResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                    getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<BusinessCategoryResponse> defaultResponseResponse) {

                Log.d(TAG, "onNext: " + defaultResponseResponse.code());

                    if(businessCategoryItems == null)
                        businessCategoryItems = new MutableLiveData<>();

                    if(defaultResponseResponse.code()==200){
                        try{

                            Log.d(TAG, "onNext: "+ defaultResponseResponse.body().getCategories().size());
                            Log.d(TAG, "onNext: "+ defaultResponseResponse.body());
                            businessCategoryItems.setValue(defaultResponseResponse.body().getCategories());

                        }catch (NullPointerException e){
                            Log.d(TAG, "onNext: Null pointer",e);
                        }

                    }
                    else if (defaultResponseResponse.code()==204){
                        Log.d(TAG, "onNext: Nothing to return");
                    }

                    else{
                        Log.d(TAG, "onNext: Error occured while retriving");
                    }

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: ",e );


            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Category retrival methods");
            }
        });
    }

    //Upload Profile Picture
    public void uploadProfilePicture(File file){
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"),file);

        MultipartBody.Part part = MultipartBody.Part.createFormData("profile_image",file.getName(),fileReqBody);
        
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"),"This is the profile image");
        
        appDataManager.uploadProfilePicture(part,name).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<DefaultResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                        if (defaultResponseResponse.code()==200){
                            statusUploadPic.setValue(true);
                            Log.d(TAG, "onNext: PROFILE PICTURE SUCCESSFULLY UPLOADED");
                            
                        }else{
                            statusUploadPic.setValue(false);
                            Log.d(TAG, "onNext: ERROR");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: ERROR");
                        statusUploadPic.setValue(false);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Of Profile Pic upload");
                    }
                });
    }

    //get all packages for a particular business
    public void businessPackages(){
        appDataManager.getBusinessPackageList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<BusinessPackageResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<BusinessPackageResponse> businessPackageResponseResponse) {

                if(businessPackageList == null)
                    businessPackageList = new MutableLiveData<>();

                if(statusBusinessPackage == null)
                    statusBusinessPackage = new MutableLiveData<>();

                if(businessPackageResponseResponse.code()==200){
                    try{
                        statusBusinessPackage.setValue("200");
                        businessPackageList.setValue(businessPackageResponseResponse.body().getProductList());
                        Log.d(TAG, "onNext: " +  businessPackageResponseResponse.body().getProductList().size());
                    }catch (NullPointerException e){
                        Log.d(TAG, "onNext: Null pointer exception");
                    }
                }else if(businessPackageResponseResponse.code()==204){
                    statusBusinessPackage.setValue("204");
                    Log.d(TAG, "onNext: Nothing to return");
                }else if(businessPackageResponseResponse.code()==500)
                    statusBusinessPackage.setValue("500");
                    Log.d(TAG, "onNext: Server Error");
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: ",e);
                statusBusinessPackage.setValue("500");
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Method of getting all packages");
            }
        });
    }

    //UPLOAD business licenses
    public void businessDocumentsUpload(File file , String doctype){

        RequestBody fileRequestBody = RequestBody.create(MediaType.parse("application/pdf"),file);

        MultipartBody.Part part = MultipartBody.Part.createFormData("doc",file.getName(),fileRequestBody);

        appDataManager.uploadDocuments(part,fileRequestBody,doctype).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<DefaultResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                        if (statusDocumentsUpload == null)
                            statusDocumentsUpload = new MutableLiveData<>();

                        Log.d(TAG, "onNext: " + defaultResponseResponse.code());

                        if (defaultResponseResponse.code()==200){
                            statusDocumentsUpload.setValue(true);
                            Log.d(TAG, "onNext: " + doctype);

                        }
                        else{
                            statusDocumentsUpload.setValue(false);
                            Log.d(TAG, "onNext: Error occured");
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        statusDocumentsUpload.setValue(false);
                        Log.e(TAG, "onError: ",e );
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Business Documents method completed");
                    }
                });

    }

    //POST AN AD

    public void createNewAd(String content,File file){

        MultipartBody.Part part = null;

        if (file !=null){
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"),file);
            part = MultipartBody.Part.createFormData("ad_image",file.getName(),fileReqBody);
        }

        RequestBody contentBody = RequestBody.create(MediaType.parse("text/plain"),content);
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"),"");

        appDataManager.createNewPost(part,titleBody,contentBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<DefaultResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                        if (statusAd == null)
                            statusAd = new MutableLiveData<>();

                        if ( defaultResponseResponse.code()==201){
                            statusAd.setValue(true);

                            Log.d(TAG, "onNext: Ad created");
                        }else{
                            statusAd.setValue(false);
                            Log.d(TAG, "onNext: Error");
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        statusAd.setValue(false);
                        Log.e(TAG, "onError: ",e );
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: MEthod of posting AD");
                    }
                });
    }

    public void getAllFeedsPost(){
        appDataManager.getGetAllFeeds().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<GetFeedResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<GetFeedResponse> data) {
                        if(data.code() == 201 || data.code() == 200){
                            Log.d(TAG, "return all feeds");
                            setOnGetAllFeeds(data.body());


                        }
                        else {
                            Log.d(TAG, "onNext: error fetching feeds");

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ",e );
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Of feed retrival");
                    }
                });
    }
    public void setOnGetAllFeeds(GetFeedResponse data) {
        if(onGetAllFeeds == null){
            onGetAllFeeds = new MutableLiveData<>();
        }
        onGetAllFeeds.setValue(data);
    }

    //get profile details
    public void getProfileData(){
        appDataManager.getBusinessProfile().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<BusinessProfileResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                    getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<BusinessProfileResponse> businessProfileResponseResponse) {
                if (businessProfileData == null)
                    businessProfileData = new MutableLiveData<>();
                if (businessProfileResponseResponse.code()==200){
                    try{

                        businessProfileData.setValue(businessProfileResponseResponse.body().getBusiness());
                    }catch (NullPointerException e){
                        Log.d(TAG, "onNext: Null pointer Exception");
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: ",e );
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Profile Data retrival");
            }
        });
    }


    //For patching the business profile
    public void patchBusinessProfile(BusinessProfileBody body){
        appDataManager.patchBusinessProfile(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {
                if (statusPatchBusProfile==null)
                    statusPatchBusProfile = new MutableLiveData<>();

                if (defaultResponseResponse.code()==200){
                    try {
                        statusPatchBusProfile.setValue(true);
                        Log.d(TAG, "onNext: Patched business profile....");
                    }catch (NullPointerException e){
                        statusPatchBusProfile.setValue(false);
                        Log.d(TAG, "onNext: Null pointer exception");
                    }
                }else{
                    statusPatchBusProfile.setValue(false);
                    Log.d(TAG, "onNext : " + defaultResponseResponse.code());
                }


            }

            @Override
            public void onError(Throwable e) {
                statusPatchBusProfile.setValue(false);
                Log.e(TAG, "onError: ", e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: PAtching the business profile completed");
            }
        });

    }

}
