package com.androidpopcorn.tenx.freshciboapp.Data.api;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.AllExpertsResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.ExpertPatchResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceBody;

import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.login.LoginResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.OtpResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyBusiness;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyExpert;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyFarmer;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.NotificationResponse;



import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.VerifyResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.TokenResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.PostFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetBusinessResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetFarmerResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetSpecialistResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetCommentResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetQueryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.verifyUser.UserObject;


import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class AppApiHelper implements ApiHelper {

    private ApiService apiService;

    @Inject
    public AppApiHelper(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Observable<Response<RegistrationResponse>> farmerRegister(RegistrationBodyFarmer body) {
        return apiService.registerFarmer(body);
    }

    @Override
    public Observable<Response<RegistrationResponse>> specialistRegister(RegistrationBodyExpert body) {
        return apiService.registerExpert(body);
    }

    @Override
    public Observable<Response<RegistrationResponse>> businessRegister(RegistrationBodyBusiness body) {
        return apiService.registerBusiness(body);
    }

    @Override
    public Observable<Response<DefaultResponse>> validate(String email, String token) {
        return apiService.validateCustomerToken(email,token);
    }

    @Override
    public Observable<Response<LoginResponse>> loginFarmer(String phone, String password) {
        return apiService.loginFarmer(phone,password);
    }

    @Override
    public Observable<Response<LoginResponse>> loginExpert(String phone, String password) {
        return apiService.loginExpert(phone,password);
    }

    @Override
    public Observable<Response<LoginResponse>> loginBusiness(String phone, String password) {
        return apiService.loginBusiness(phone,password);
    }

    @Override
    public Observable<Response<OtpResponse>> sendOtpRequest(String phone) {
        return apiService.sendOtpRequest(phone);
    }

    @Override
    public Observable<Response<OtpResponse>> verifyOtp(String session_id, String phone) {
        return apiService.verifyOtp(session_id,phone);
    }

    @Override

    public Observable<Response<VerifyResponse>> verifyAuth() {
        return apiService.verifyAuth();
    }

    @Override
    public Observable<Response<UserObject>> verifyUserType() {
        return apiService.verifyUserType();
    }


    @Override
    public Observable<Response<DefaultResponse>> saveFarmCategory(FarmerCategoryBody farmerCategoryBody) {
        return apiService.saveFarmCategory(farmerCategoryBody);
    }

    @Override
    public Observable<Response<DefaultResponse>> saveProduceDetails(FarmerProduceBody body) {
        return apiService.saveProduceDetails(body);
    }

    @Override
    public Observable<Response<DefaultResponse>> saveCompleteProfileDetails(FarmerCompleteProfileBody body) {
        return apiService.saveCompleteProfileDetails(body);
    }

    @Override
    public Observable<Response<FarmerProfileResponse>> getCurrentFarmerProfile() {
        return apiService.getCurrentFarmerProfile();
    }

    @Override
    public Observable<Response<FarmerProduceResponse>> getCurrentFarmerProduce(int id) {
        return apiService.getCurrentFarmerProduce(id);
    }

    @Override
    public Observable<Response<FarmerCategoryResponse>> getCurrentFarmerCategory(int id) {
        return apiService.getCurrentFarmerCategory(id);
    }

    @Override
    public Observable<Response<DefaultResponse>> patchFarmerProduce(int id,FarmerProduceBody body) {
        return apiService.patchFarmerProduce(id,body);
    }

    @Override
    public Observable<Response<ExpertPackageResponse.ExpertPackage>> saveExpertPackage(ExpertPackageBody body) {
        return apiService.saveExpertPackage(body);
    }

    @Override
    public Observable<Response<AllExpertsResponse>> getAllExperts() {
        return apiService.getAllExperts();
    }

    @Override
    public Observable<Response<BusinessCategoryBody>> saveBusinessCategory(BusinessCategoryBody body) {
        return apiService.saveBusinessCategoryBody(body);
    }

    @Override
    public Observable<Response<BusinessProfileResponse>> getBusinessProfile() {
        return apiService.getBusinessProfileDetails();
    }

    @Override
    public Observable<Response<DefaultResponse>> patchBusinessProfile(BusinessProfileBody body) {
        return apiService.patchBusinessProfile(body);
    }

    @Override
    public Observable<Response<TokenResponse>> saveToken(String token) {
        return apiService.saveFcmToken(token);
    }

    @Override
    public Observable<Response<ExpertPatchResponse>> updateExpertProfile(ExpertPatchBody body) {
        return apiService.updateExpertProfile(body);
    }

    @Override
    public Observable<Response<DefaultResponse>> uploadDocuments(MultipartBody.Part file, RequestBody requestBody, String doctype) {
        return apiService.uploadDocuments(file, requestBody, doctype);
    }

    @Override
    public Observable<Response<ExpertProfileResponse>> getCurrentExpertProfile() {
        return apiService.getCurrentExpertProfile();
    }

    @Override
    public Observable<Response<ExpertPackageResponse>> getCurrentExpertPackage() {
        return apiService.getCurrentExpertPackage();
    }

    @Override
    public Observable<Response<DefaultResponse>> uploadProfilePicture(MultipartBody.Part file, RequestBody requestBody) {
        return apiService.uploadProfilePicture(file, requestBody);
    }


    @Override
    public Observable<Response<NotificationResponse>> getNotificationsForUser() {
        return apiService.getNotificationsForUser();

    }
    @Override
    public Observable<Response<DefaultResponse>> saveBusinessPackage(BusinessPackageBody body) {
        return apiService.saveBusinessPackageBody(body);
    }

    @Override
    public Observable<Response<BusinessCategoryResponse>> getBusinessCategory() {
        return apiService.getBusinessCategoryList();
    }

    @Override

    public Observable<Response<BusinessPackageResponse>> getBusinessPackageList() {
        return apiService.getBusinessPackageList();
    }

    public Observable<Response<PostFeedResponse>> postCreatePost(MultipartBody.Part file, RequestBody titleBody, RequestBody contentBody, RequestBody categoryBody) {
        return apiService.postCreatePost(file, titleBody, contentBody, categoryBody);
    }

    @Override
    public Observable<Response<DefaultResponse>> createNewPost(MultipartBody.Part file, RequestBody titleBody, RequestBody contentBody) {
        return apiService.createNewPost(file,titleBody,contentBody);
    }

    @Override
    public Observable<Response<DefaultResponse>> createNewQuery(MultipartBody.Part file, RequestBody questionBody) {
        return apiService.createNewQuery(file,questionBody);
    }

    @Override
    public Observable<Response<GetFeedResponse>> getGetAllFeeds() {
        return apiService.getGetAllFeeds();
    }

    @Override
    public Observable<Response<GetQueryResponse>> getQueriesForSpecialist() {
        return apiService.getQueriesForSpecialist();
    }

    @Override
    public Observable<Response<GetCommentResponse>> getCommentsForConv(String convid) {
        return apiService.getCommentsForConv(convid);
    }

    @Override
    public Observable<Response<GetFeedResponse>> getGetAllFeedsPaged(int limit, int page) {
        return apiService.getGetAllFeedsPaged(limit, page);
    }

    @Override
    public Observable<Response<GetFarmerResponse>> getFarmerById(int farmerid) {
        return apiService.getFarmerById(farmerid);
    }

    @Override
    public Observable<Response<GetSpecialistResponse>> getExpertById(int expertID) {
        return apiService.getExpertById(expertID);
    }

    @Override
    public Observable<Response<GetBusinessResponse>> getBusinessById(int businessID) {
        return apiService.getBusinessById(businessID);
    }
}
