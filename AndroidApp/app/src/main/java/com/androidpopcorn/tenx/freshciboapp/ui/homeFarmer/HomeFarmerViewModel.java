package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.util.Log;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.AllExpertsResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.base.BaseViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.paging.FeedDataFactory;
import com.androidpopcorn.tenx.freshciboapp.utils.NetworkState;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class HomeFarmerViewModel extends BaseViewModel {


    private static final String TAG = "HomeFarmerViewModel";
    AppDataManager appDataManager;

    //PAGING
    private Executor executor;
    private LiveData<NetworkState> networkStateLiveData;
    private LiveData<PagedList<GetFeedResponse.FeedData>> feedLiveData;

    //get all feed
    MutableLiveData<GetFeedResponse> onGetAllFeeds;


    //Livedata for farmer selecting the different categories of produces
    private MutableLiveData<Boolean> statusCategoryFarmer;

    //LiveData for farmer for making the Produce Details.
    private MutableLiveData<Boolean> statusProduceDetailsAlertDialog;

    //LiveData for farmer Completing his profile
    private MutableLiveData<String> statusCompleteProfile;

    //Livedata for retrieving all the experts status
    private MutableLiveData<Boolean> statusGetAllExperts;

    //LIveData for retriebing all the experts
    private MutableLiveData<AllExpertsResponse> listExperts;

    //getting the business profile pic upload status
    private MutableLiveData<Boolean> statusUploadPic;

    //getting the status for posting new query
    private MutableLiveData<Boolean> statusQuery;

    //Updating a single produce package for a farmer
    private MutableLiveData<Boolean> statusPatchProduce;

    public LiveData<Boolean> getStatusPatchProduce(){
        if (statusPatchProduce == null)
            statusPatchProduce = new MutableLiveData<>();
        return  statusPatchProduce;
    }

    public LiveData<Boolean> getStatusQuery(){
        if (statusQuery == null)
            statusQuery = new MutableLiveData<>();
        return statusQuery;
    }

    public LiveData<Boolean> getStatusUploadPic(){
        if (statusUploadPic == null)
            statusUploadPic = new MutableLiveData<>();
        return statusUploadPic;
    }

    public LiveData<AllExpertsResponse> getListExpertsData() {

        if (listExperts == null)
            listExperts = new MutableLiveData<>();
        return listExperts;
    }

    public LiveData<Boolean> getStatusGetAllExperts() {
        if (statusGetAllExperts == null)
            statusGetAllExperts = new MutableLiveData<>();
        return statusGetAllExperts;
    }

    public LiveData<Boolean> getStatusCategoryFarmer() {
        if (statusCategoryFarmer == null) {
            statusCategoryFarmer = new MutableLiveData<>();
        }
        return statusCategoryFarmer;
    }

    public LiveData<String> getStatusCompleteProfile() {
        if (statusCompleteProfile == null)
            statusCompleteProfile = new MutableLiveData<>();
        return statusCompleteProfile;
    }

    public LiveData<Boolean> getStatusProduceDetailsAlertDialog() {
        if (statusProduceDetailsAlertDialog == null) {
            statusProduceDetailsAlertDialog = new MutableLiveData<>();
        }
        return statusProduceDetailsAlertDialog;
    }

    public MutableLiveData<String> categorySelected;

    public LiveData<String> getCategorySelected(){
        if (categorySelected == null)
            categorySelected = new MutableLiveData<>();
        return categorySelected;
    }

    //Data for Farmer Profile
    public MutableLiveData<FarmerProfileResponse.Farmer> farmerProfileResponse;

    public LiveData<FarmerProfileResponse.Farmer> getFarmerProfileResponse(){
        if (farmerProfileResponse == null)
            farmerProfileResponse = new MutableLiveData<>();
        return farmerProfileResponse;
    }

    //For storing the current farmer id
    int farmer_id ;



    //Data for Farmer Produce Details
    public MutableLiveData<FarmerProduceResponse> currentFarmerProduce;

    public LiveData<FarmerProduceResponse> getCurrentFarmerProduce(){
        if (currentFarmerProduce == null)
            currentFarmerProduce = new MutableLiveData<>();
        return currentFarmerProduce;
    }

    //Farmer Farm Categories
    public MutableLiveData<FarmerCategoryResponse> currentFarmerCategories;

    public LiveData<FarmerCategoryResponse> getCurrentFarmerCategories(){
        if (currentFarmerCategories == null){
            currentFarmerCategories = new MutableLiveData<>();
        }
        return currentFarmerCategories;
    }

    //status for farm category for showing progress bar
    public MutableLiveData<Boolean> statusForFarmCategory;

    public LiveData<Boolean> getStatusForFarmCategory(){
        if (statusForFarmCategory == null){
            statusForFarmCategory = new MutableLiveData<>();
        }
        return statusForFarmCategory;
    }

    public HomeFarmerViewModel(AppDataManager appDataManager, Application application) {
        super(appDataManager, application);
        this.appDataManager = appDataManager;
        init();
    }


    //used for paging
    private void init() {

        executor = Executors.newFixedThreadPool(5);

        FeedDataFactory feedDataFactory  = new FeedDataFactory(appDataManager);
        networkStateLiveData = Transformations.switchMap(feedDataFactory.getFeedDataSourceMutableLiveData(),
                dataSource -> dataSource.getNetworkState());

        PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false).setInitialLoadSizeHint(5)
                .setPageSize(5).build();

        feedLiveData = (new LivePagedListBuilder(feedDataFactory,pagedListConfig)).setFetchExecutor(executor).build();


    }

    public LiveData<NetworkState> getNetworkStateLiveData() {
        return networkStateLiveData;
    }

    public LiveData<PagedList<GetFeedResponse.FeedData>> getFeedLiveData() {
        return feedLiveData;
    }

    //sets selected category from home page
    public void setCategorySelected(String category){
        if (categorySelected == null)
            categorySelected = new MutableLiveData<>();
        categorySelected.setValue(category);

    }

    //Saves farmer farming categories
    public void saveCategoryFarmer(FarmerCategoryBody farmerCategoryBody) {
        appDataManager.saveFarmCategory(farmerCategoryBody).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {


                if (defaultResponseResponse.code() == 201) {

                    if (statusCategoryFarmer == null) {
                        statusCategoryFarmer = new MutableLiveData<>();
                    }
                    try {

                        statusCategoryFarmer.setValue(true);
                        Log.d(TAG, "onNext: Success in storing Categories");
                    } catch (NullPointerException e) {
                        Log.d(TAG, "onNext:  null token", e);
                    }


                } else {

                    statusCategoryFarmer.setValue(false);
                    Log.d(TAG, "onNext: Error occured while storing categories");
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: ", e);
                statusCategoryFarmer.setValue(false);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Save Farm Category method");
            }
        });

    }

    //saves Farmer produce Data
    public void saveProduceDetails(FarmerProduceBody farmerProduceBody) {
        appDataManager.saveProduceDetails(farmerProduceBody).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                if (defaultResponseResponse.code() == 201) {

                    if (statusProduceDetailsAlertDialog == null) {
                        statusProduceDetailsAlertDialog = new MutableLiveData<>();
                    }
                    try {

                        statusProduceDetailsAlertDialog.setValue(true);
                        Log.d(TAG, "onNext: Succes in storing produce details");
                    } catch (NullPointerException e) {
                        Log.d(TAG, "onNext: Error in storing produce details");
                        statusProduceDetailsAlertDialog.setValue(false);
                    }

                } else {
                    Log.d(TAG, "onNext: Error in storing produce data");
                }

            }

            @Override
            public void onError(Throwable e) {
                statusProduceDetailsAlertDialog.setValue(false);
                Log.e(TAG, "onError: ", e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: SaveproduceDetails method completed");
            }
        });
    }



    //TODO need to use this for displaying
    //TODO need to retrive the names
    //Get  a list of all the Experts
    public void getAllExperts() {
        appDataManager.getAllExperts().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<AllExpertsResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<AllExpertsResponse> allExpertsResponseResponse) {

                        if (listExperts == null)
                            listExperts = new MutableLiveData<>();
                        if(statusGetAllExperts==null)
                            statusGetAllExperts = new MutableLiveData<>();
                        if (allExpertsResponseResponse.code() == 200) {
                            Log.d(TAG, "onNext: size : "+allExpertsResponseResponse.body().getmList().size());

                            try {
                                statusGetAllExperts.setValue(true);
                                listExperts.setValue(allExpertsResponseResponse.body());
                                Log.d(TAG, "onNext: Body : " + allExpertsResponseResponse.body().toString());

                                Log.d(TAG, "onNext: " + allExpertsResponseResponse.body().getmList());
                            } catch (NullPointerException e) {
                                Log.e(TAG, "onNext: ", e);
                                statusGetAllExperts.setValue(false);
                            }
                        } else {

                            statusGetAllExperts.setValue(false);
                            Log.d(TAG, "onNext: Error retrieving data");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ",e );
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: All Experts are retrieved");
                    }
                });

    }
    //Upload Profile Picture
    public void uploadProfilePicture(File file){
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"),file);

        MultipartBody.Part part = MultipartBody.Part.createFormData("profile_image",file.getName(),fileReqBody);

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"),"This is the profile image");

        appDataManager.uploadProfilePicture(part,name).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<DefaultResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                        if (defaultResponseResponse.code()==200){
                            statusUploadPic.setValue(true);
                            Log.d(TAG, "onNext: PROFILE PICTURE SUCCESSFULLY UPLOADED");

                        }else{
                            statusUploadPic.setValue(false);
                            Log.d(TAG, "onNext: ERROR");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: ERROR");
                        statusUploadPic.setValue(false);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Of Profile Pic upload");
                    }
                });
    }

    //Post new query
    public void postNewQuery(String content ,File file){

        MultipartBody.Part part = null;

        if (file !=null){
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"),file);
            part = MultipartBody.Part.createFormData("query_image",file.getName(),fileReqBody);

        }

        RequestBody contentBody = RequestBody.create(MediaType.parse("text/plain"),content);

        appDataManager.createNewQuery(part,contentBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<DefaultResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<DefaultResponse> defaultResponseResponse) {
                            if (statusQuery == null)
                                statusQuery = new MutableLiveData<>();
                            if (defaultResponseResponse.code()==200){
                                statusQuery.setValue(true);
                                Log.d(TAG, "onNext: Query Posted");
                            }else{
                                Log.d(TAG, "onNext: Query posting failed");
                                statusQuery.setValue(false);
                            }

                    }

                    @Override
                    public void onError(Throwable e) {
                        statusQuery.setValue(false);
                        Log.e(TAG, "onError: ",e );
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Of the post new Query");
                    }
                });
    }

    //get profile data for farmer
    public void getProfileData() {
        appDataManager.getCurrentFarmerProfile().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<FarmerProfileResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<FarmerProfileResponse> farmerProfileResponseResponse) {

                            if (farmerProfileResponse == null)
                                farmerProfileResponse = new MutableLiveData<>();
                            if (farmerProfileResponseResponse.code()==200){
                                try {
                                    Log.d(TAG, "onNext: " + farmerProfileResponseResponse.body().getFarmer().getLocation());
                                    Log.d(TAG, "onNext: " + farmerProfileResponseResponse.body().getFarmer().getName());
                                    farmerProfileResponse.setValue(farmerProfileResponseResponse.body().getFarmer());
                                    farmer_id = farmerProfileResponseResponse.body().getFarmer().getFarmer_id();
                                    Log.d(TAG, "onNext: Farmer id IS : " + farmer_id);
                                }catch (NullPointerException e){
                                    Log.d(TAG, "onNext:NUll pointer exception . ");
                                }
                            }else{
                                Log.d(TAG, "onNext: Not successful");
                            }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ",e );
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Current profile of farmer retrieved");
                    }
                });

    }

    public void getAllFeedsPost(){
        appDataManager.getGetAllFeeds().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<GetFeedResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<GetFeedResponse> data) {
                        if(data.code() == 201 || data.code() == 200){
                            Log.d(TAG, "return all feeds");
                            setOnGetAllFeeds(data.body());


                        }
                        else {
                            Log.d(TAG, "onNext: error fetching feeds");

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ",e );
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Of feed retrival");
                    }
                });
    }
    public void setOnGetAllFeeds(GetFeedResponse data) {
        if(onGetAllFeeds == null){
            onGetAllFeeds = new MutableLiveData<>();
        }
        onGetAllFeeds.setValue(data);
    }

    public void retriveCurrentFarmerPackages(int farmer){
        appDataManager.getCurrentFarmerProduce(farmer).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<FarmerProduceResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<FarmerProduceResponse> farmerProduceResponseResponse) {
                if (currentFarmerProduce == null)
                     currentFarmerProduce = new MutableLiveData<>();
                if (farmerProduceResponseResponse.code()==200){
                    try{



                        Log.d(TAG, "onNext: SUCCESS");
                        Log.d(TAG, "onNext: SIze of produce details" + farmerProduceResponseResponse.body().getBody().size());
                        currentFarmerProduce.setValue(farmerProduceResponseResponse.body());

                    }catch (NullPointerException e){
                        Log.d(TAG, "onNext: ",e);
                    }
                }else{
                    Log.d(TAG, "onNext: " + farmerProduceResponseResponse.code());
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: ",e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: MEthod of the current Farmer PRoduce");
            }
        });
    }
    //CompleteProfile Fragment for Farmer. Completes the details
    public void saveCompleteProfileDetails(FarmerCompleteProfileBody body) {
        appDataManager.saveCompleteProfileDetails(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                if (statusCompleteProfile == null)
                    statusCompleteProfile = new MutableLiveData<>();


                if (defaultResponseResponse.code() == 200) {

                    try {
                        statusCompleteProfile.setValue("200");
                        Log.d(TAG, "onNext: Success in filling the form");

                    } catch (NullPointerException e) {

                        statusCompleteProfile.setValue("400");
                        Log.d(TAG, "onNext: Do not succedd");

                    }
                } else if (defaultResponseResponse.code() == 400) {
                    Log.d(TAG, "onNext: Not authorized");
                    statusCompleteProfile.setValue("400");
                } else if (defaultResponseResponse.code() == 501) {
                    statusCompleteProfile.setValue("501");
                    Log.d(TAG, "onNext: Internal server Error");
                } else {
                    Log.d(TAG, "onNext: Unknown Error");
                    statusCompleteProfile.setValue("501");
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: ", e);
                statusCompleteProfile.setValue("501");
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Complete Profile");
            }
        });
    }

    //Farmer Farm Category
    public void getCurrentFarmerFarmCategory(int farmer_id){
        appDataManager.getCurrentFarmerCategory(farmer_id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<FarmerCategoryResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<FarmerCategoryResponse> farmerCategoryResponseResponse) {

                if (currentFarmerCategories == null)
                    currentFarmerCategories = new MutableLiveData<>();
                if (statusForFarmCategory == null){
                    statusForFarmCategory = new MutableLiveData<>();
                }

                if (farmerCategoryResponseResponse.code()==200){

                    try{
                        currentFarmerCategories.setValue(farmerCategoryResponseResponse.body());
                        statusForFarmCategory.setValue(true);

                        Log.d(TAG, "onNext: "+ farmerCategoryResponseResponse.body().getBody().size());
                    }catch (NullPointerException e){
                        Log.d(TAG, "onNext: ",e);
                        statusForFarmCategory.setValue(false);
                    }
                }else{
                    statusForFarmCategory.setValue(false);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: ", e);
                statusForFarmCategory.setValue(false);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    //Updating a particular produce details for farmer
    public void patchFarmerProduce(int id,FarmerProduceBody body){
        appDataManager.patchFarmerProduce(id,body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                if (statusPatchProduce == null)
                    statusPatchProduce = new MutableLiveData<>();
                if (defaultResponseResponse.code()==200){

                    try{
                        statusPatchProduce.setValue(true);


                    }catch (NullPointerException e){
                        Log.d(TAG, "onNext: Null pointer exception");
                        statusPatchProduce.setValue(false);
                    }
                }else{
                    statusPatchProduce.setValue(false);
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: ",e );
                statusPatchProduce.setValue(false);
            }

            @Override
            public void onComplete() {

            }
        });
    }


}
