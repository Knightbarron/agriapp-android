package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.expertCompleteProfile;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.uploadDocuments.ExpertUploadDocuments;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertCompleteProfile extends Fragment  {

    @Inject
    HomeExpertViewModel viewModel;

    private static final String TAG = "ExpertCompleteProfile";

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_organisation)
    EditText etOrganisation;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;
    Unbinder unbinder;
    @BindView(R.id.name_tv)
    TextView nameTv;
    @BindView(R.id.specialization_tv)
    TextView specializationTv;
    @BindView(R.id.qualification_tv)
    TextView qualificationTv;
    @BindView(R.id.spinner_specialization)
    Spinner spinnerSpecialization;
    @BindView(R.id.spinner_qualification)
    Spinner spinnerQualification;
    @BindView(R.id.spinner_specialization_additional)
    Spinner spinner_specialization_additional;
    @BindView(R.id.additional_specialization_tv)
    TextView additionalspecializationTv;



    @BindView(R.id.tv_error_info)
    TextView tvErrorInfo;


    String specialization;
    String qualification;
    String additionalSpecialization;

    @Inject
    ExpertUploadDocuments expertUploadDocuments;

    @Inject
    public ExpertCompleteProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expert_complete_profile, container, false);

        unbinder = ButterKnife.bind(this, view);

        initSpinners();

        editBtnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
                goToUploads();


            }
        });


        return view;
    }

    public void goToUploads() {

        String backStateName = expertUploadDocuments.getClass().toString();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(R.id.container, expertUploadDocuments);
        transaction.addToBackStack(backStateName);

        transaction.commit();
    }

    private void sendData() {

        String email = etEmail.getText().toString();
        String organisation = etOrganisation.getText().toString();
        String specialization = specializationTv.getText().toString();
        String specialization2 = additionalspecializationTv.getText().toString();
        String qualification = qualificationTv.getText().toString();

        if(!isEmailValid(email)) {
            tvErrorInfo.setText(R.string.error_enter_valid_email);
            return;
        }

        if(TextUtils.isEmpty(organisation)){
            tvErrorInfo.setText(R.string.error_enter_valid_org);
            return;
        }

        if(TextUtils.isEmpty(specialization)){
            tvErrorInfo.setText(R.string.error_enter_specialization);
            return;
        }

        if(TextUtils.isEmpty(qualification)){
            tvErrorInfo.setText(R.string.error_enter_qualification);
            return;
        }

        viewModel.updateExpertProfile(new ExpertPatchBody("", email, "", organisation, specialization, specialization2, qualification));
    }

    private void initSpinners() {

        String[] dataSpecialization = new String[]{"","AgriEngineering", "AgroEconomics", "Agronomy",
                "Bio Chemistry", "Biotechnology", "Ecology", "Entomology", "Extension Education", "Food Science & technology",
                "Forage Sciences", "Meterology", "Ornamental horticulture", "Other", "Pathology", "Agri business management",
                "Agroforestry", "Animal husbandry", "Bioinformatics", "Enviormental Studies", "Floriculture",
                "Landscaping", "Microbiology", "Nursery & green house technology", "Olericulture", "Physiology",
                "Plant breeding", "Plant Genetics", "Plantation crops,spices and aromatic", "Pomology",
                "Post harvest management", "Rural banking & finance management", "Silviculture", "Soil Science",
                "Statistics", "Weed Science"};


        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataSpecialization);
        spinnerSpecialization.setAdapter(adapter);

        specialization = spinnerSpecialization.getSelectedItem().toString();

        spinnerSpecialization.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                specialization = spinnerSpecialization.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Specialization : " + specialization);
                specializationTv.setText(specialization);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_specialization_additional.setAdapter(adapter);
        spinner_specialization_additional.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                additionalSpecialization = spinner_specialization_additional.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Specialization : " + additionalSpecialization);
                additionalspecializationTv.setText(additionalSpecialization);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        String[] dataQualification = new String[]{"","Diploma", "Undergraduate/graduate", "Post graduate",
                "Doctorate", "Post Doctorate"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataQualification);
        spinnerQualification.setAdapter(adapter1);

        spinnerQualification.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                qualification = spinnerQualification.getSelectedItem().toString();

                Log.d(TAG, "initSpinners: Qualification : " + qualification);

                qualificationTv.setText(qualification);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }



//    helper functions
boolean isEmailValid(CharSequence email) {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
}

public void setInfoMessage(String message){
        tvErrorInfo.setText(message);
}
}
