package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert;

import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertProfileResponse;

public interface HomeExpertCallback {


    void onUploadSuccess();
    void onUploadFailure();


    void onFetchCurrentExpertSuccess(ExpertProfileResponse data);
    void onFetchCurrentExpertFailure();

    void onFetchCurrentExpertPackagesSuccess(ExpertPackageResponse data);
    void onFetchCurrentExpertPackagesFailure();


    void onProfilePictureUpdateSuccess(DefaultResponse data);
    void onProfilePictureUpdateFailure();




}
