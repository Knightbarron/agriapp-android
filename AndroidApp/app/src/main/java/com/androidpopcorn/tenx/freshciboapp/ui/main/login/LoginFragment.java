package com.androidpopcorn.tenx.freshciboapp.ui.main.login;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.main.regis.RegisFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {


    private static final String TAG = "LoginFragment";


//
    private String user;


    @Inject
    MainViewModel viewModel;

    @Inject
    RegisFragment regisFragment;
    @BindView(R.id.errormsg)
    TextView errormsg;
    @BindView(R.id.login_et_phone)
    TextInputEditText loginEtPhone;
    @BindView(R.id.login_et_password)
    TextInputEditText loginEtPassword;
    @BindView(R.id.login_btn_signin)
    Button loginBtnSignin;
    Unbinder unbinder;

    @Inject
    public LoginFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        user = viewModel.getUserType();
        Log.d(TAG, "onClickRegistration: Attempting login as :   " + user);

        unbinder = ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @OnClick(R.id.login_btn_signin)
    public void onClickLogin(){


        if(user.equals("farmer"))
            doLogin("farmer");
        else if(user.equals("expert"))
            doLogin("expert");
        else if(user.equals("business"))
            doLogin("business");

    }

    public void doLogin(String type) {
        Log.d(TAG, "doLogin: Attempting login ");
        String phone = loginEtPhone.getText().toString();
        String password = loginEtPassword.getText().toString();

        if (TextUtils.isEmpty(phone) || TextUtils.isEmpty(password)) {
            setInfo("Please fill up the fields");
            return;
        } else {
            errormsg.setVisibility(View.INVISIBLE);

            if(TextUtils.equals(type, "farmer")) {
                viewModel.attemptLoginFarmer(phone, password);
            }else if(TextUtils.equals(type, "expert")) {
                viewModel.attemptLoginExpert(phone, password);
            }else if(TextUtils.equals(type,"business")){
                viewModel.attemptLoginBusiness(phone,password);
            }


        }
    }



    public void setInfo(String text) {
        errormsg.setVisibility(View.VISIBLE);
        errormsg.setText(text);
    }

}
