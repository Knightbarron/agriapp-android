package com.androidpopcorn.tenx.freshciboapp.Data.api;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.AllExpertsResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.ExpertPatchResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.NotificationResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceBody;

import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.login.LoginResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.OtpResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyBusiness;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyExpert;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationBodyFarmer;
import com.androidpopcorn.tenx.freshciboapp.Data.model.registration.RegistrationResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.VerifyResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp.TokenResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.PostFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetBusinessResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetFarmerResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.profile.GetSpecialistResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetCommentResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetQueryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.verifyUser.UserObject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Path;


public interface ApiHelper {

    //REGISTRATION
    Observable<Response<RegistrationResponse>> farmerRegister(RegistrationBodyFarmer body);
    Observable<Response<RegistrationResponse>> specialistRegister(RegistrationBodyExpert body);
    Observable<Response<RegistrationResponse>> businessRegister(RegistrationBodyBusiness body);



    //LOGIN
    Observable<Response<LoginResponse>> loginFarmer(String phone,String password);
    Observable<Response<LoginResponse>> loginExpert(String phone,String password);
    Observable<Response<LoginResponse>> loginBusiness(String phone,String password);


    //OTP
    Observable<Response<OtpResponse>> sendOtpRequest(String phone);
    Observable<Response<OtpResponse>> verifyOtp(String session_id,String phone);


    //AUTH
    Observable<Response<DefaultResponse>> validate(String email,String token);
    Observable<Response<TokenResponse>> saveToken(String token);
    Observable<Response<VerifyResponse>> verifyAuth();
    Observable<Response<UserObject>> verifyUserType();


    //NOTIFICATION
    Observable<Response<NotificationResponse>> getNotificationsForUser();

    //FARMER
    Observable<Response<DefaultResponse>> saveFarmCategory(FarmerCategoryBody body);
    Observable<Response<DefaultResponse>> saveProduceDetails(FarmerProduceBody body);
    Observable<Response<DefaultResponse>> saveCompleteProfileDetails(FarmerCompleteProfileBody body);
    Observable<Response<FarmerProfileResponse>> getCurrentFarmerProfile();
    Observable<Response<FarmerProduceResponse>> getCurrentFarmerProduce(int id);
    Observable<Response<FarmerCategoryResponse>> getCurrentFarmerCategory(int id);
    Observable<Response<DefaultResponse>> patchFarmerProduce(int id,FarmerProduceBody body);

    //BUSINESS
    Observable<Response<BusinessCategoryResponse>> getBusinessCategory();
    Observable<Response<BusinessPackageResponse>> getBusinessPackageList();
    Observable<Response<DefaultResponse>> saveBusinessPackage(BusinessPackageBody body);
    Observable<Response<BusinessCategoryBody>> saveBusinessCategory(BusinessCategoryBody body);
    Observable<Response<BusinessProfileResponse>> getBusinessProfile();
    Observable<Response<DefaultResponse>> patchBusinessProfile(@Body BusinessProfileBody body);


    //EXPERT
    Observable<Response<GetQueryResponse>> getQueriesForSpecialist();
    Observable<Response<ExpertPatchResponse>> updateExpertProfile(ExpertPatchBody body);
    Observable<Response<ExpertPackageResponse.ExpertPackage>> saveExpertPackage(ExpertPackageBody body);
    Observable<Response<AllExpertsResponse>> getAllExperts();
    Observable<Response<ExpertProfileResponse>> getCurrentExpertProfile();
    Observable<Response<ExpertPackageResponse>> getCurrentExpertPackage();

    //MULTIPART
    Observable<Response<DefaultResponse>> uploadDocuments(MultipartBody.Part file, RequestBody requestBody ,String doctype);
    Observable<Response<DefaultResponse>> uploadProfilePicture(MultipartBody.Part file, RequestBody requestBody);
    Observable<Response<PostFeedResponse>>  postCreatePost(MultipartBody.Part file, RequestBody titleBody, RequestBody contentBody, RequestBody categoryBody);
    Observable<Response<DefaultResponse>> createNewPost(MultipartBody.Part file,RequestBody titleBody,RequestBody contentBody);
    Observable<Response<DefaultResponse>> createNewQuery(MultipartBody.Part file,RequestBody questionBody);


    //FEED
    Observable<Response<GetFeedResponse>> getGetAllFeeds();
    Observable<Response<GetFeedResponse>> getGetAllFeedsPaged(int limit ,  int page);

    //Observable<Response<GetQueryResponse>> getQueriesForSpecialist();
    Observable<Response<GetCommentResponse>> getCommentsForConv(String convid);



//    profile get routes


    Observable<Response<GetFarmerResponse>> getFarmerById(int farmerid);


    Observable<Response<GetSpecialistResponse>> getExpertById(int expertID);


    Observable<Response<GetBusinessResponse>> getBusinessById(int businessID);





}
