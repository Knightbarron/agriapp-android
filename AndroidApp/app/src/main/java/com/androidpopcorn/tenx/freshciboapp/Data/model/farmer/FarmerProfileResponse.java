package com.androidpopcorn.tenx.freshciboapp.Data.model.farmer;

import com.google.gson.annotations.SerializedName;

public class FarmerProfileResponse {

    @SerializedName("farmer")
    private Farmer farmer;

    public FarmerProfileResponse(Farmer farmer) {
        this.farmer = farmer;
    }


    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    public class Farmer{
        @SerializedName("farmer_id")
        private int farmer_id;
    @SerializedName("farmer_full_name")
    private String name;
    @SerializedName("farmer_location")
    private String location;
    @SerializedName("farmer_image_path")
    private String image;
    @SerializedName("farmer_about_me")
    private String bio ;
        @SerializedName("farmer_address")
        private String address;

        public String getAddress() {
            return address;
        }

        @SerializedName("farmer_email")
        private String email;

        @SerializedName("farmer_nearest_market")
        private String nearestMarketLocation;
        @SerializedName("farmer_land_area")
        private String landArea;
        @SerializedName("farmer_land_owner")
        private String farmLandOwner;
        @SerializedName("farmer_organic_farming")
        private int organicFarm;


        public String getEmail() {
            return email;
        }

        public String getNearestMarketLocation() {
            return nearestMarketLocation;
        }

        public String getLandArea() {
            return landArea;
        }

        public String getFarmLandOwner() {
            return farmLandOwner;
        }

        public int getOrganicFarm() {
            return organicFarm;
        }

        public Farmer(int farmer_id, String name, String location, String image, String bio) {
            this.farmer_id = farmer_id;
            this.name = name;
            this.location = location;
            this.image = image;
            this.bio = bio;
        }



        public Farmer(String bio){
        this.bio  = bio ;
    }

        public void setFarmer_id(int farmer_id) {
            this.farmer_id = farmer_id;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        //    @SerializedName("farmer_organic_farming")
//    private Boolean organicFarming;

        public Farmer(String name, String location, String image) {
            this.name = name;
            this.location = location;
            this.image = image;
        }

        public int getFarmer_id() {
            return farmer_id;
        }

        public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

//    public Boolean getOrganicFarming() {
//        return organicFarming;
//    }
//
//    public void setOrganicFarming(Boolean organicFarming) {
//        this.organicFarming = organicFarming;
//    }

    }
}
