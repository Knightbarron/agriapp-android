package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory.adapter;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection.CategoryClass;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class BusinessCategoryAdapter extends RecyclerView.Adapter<BusinessCategoryAdapter.BusinessCategoryViewHolder> {


    private static final String TAG = "BusinessCategoryAdapter";

    private List<BusinessCategory> mList;

    private View.OnClickListener onItemClickListener;

    public void setOnItemClickListener(View.OnClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Inject
    public BusinessCategoryAdapter() {

        mList = new ArrayList<>();
    }

    @NonNull
    @Override
    public BusinessCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.listitem_farmer_category,viewGroup,false);

        return new BusinessCategoryViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull BusinessCategoryViewHolder businessCategoryViewHolder, int i) {
        businessCategoryViewHolder.imageView.setImageResource(mList.get(i).getImage());

        businessCategoryViewHolder.textView.setText(mList.get(i).getData());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void updateListItems(List<BusinessCategory> mList) {
        this.mList.addAll(mList);
        notifyDataSetChanged();


    }

    public class BusinessCategoryViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        public BusinessCategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setTag(this);

            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            itemView.setOnClickListener(onItemClickListener);


        }
    }
}
