package com.androidpopcorn.tenx.freshciboapp.Data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.androidpopcorn.tenx.freshciboapp.di.scope.ApplicationContext;

import javax.inject.Inject;

public class AppPreferencesHelper implements PreferencesHelper {


    private SharedPreferences mPrefs;
    private static final String PREF_KEY_ACCESS_TOKEN = "pref_key_access_token";
    private static final String EMAIL_ACCESS_KEY = "Myemail";
    private static final String USER_TYPE_KEY = "UserType";





    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context, @PreferencesInfo String filename){
        mPrefs = context.getSharedPreferences(filename, Context.MODE_PRIVATE);
    }


    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "");
    }

    @Override
    public void setAccessToken(String token) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, token).apply();
    }

    @Override
    public void setEmail(String email) {
        mPrefs.edit().putString(EMAIL_ACCESS_KEY, email).apply();
    }

    @Override
    public String getEmail() {
        return mPrefs.getString(EMAIL_ACCESS_KEY, "");
    }

    @Override
    public String getTypeUser() {
        return mPrefs.getString(USER_TYPE_KEY,"");
    }

    @Override
    public void setTypeUser(String userType) {
        mPrefs.edit().putString(USER_TYPE_KEY,userType).apply();
    }
}
