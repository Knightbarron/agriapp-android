package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.notifications;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.notifications.adapters.ExpertNotificationAdapter;
import com.bumptech.glide.RequestManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertNotificationsFragment extends Fragment {

    private static final String TAG = "ExpertNotificationsFrag";

    @Inject
    HomeExpertViewModel viewModel;



    @Inject
    RequestManager requestManager;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Inject
    ExpertNotificationAdapter adapter;


    Unbinder unbinder;


    @Inject
    public ExpertNotificationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        viewModel.loadNotifications();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expert_notifications, container, false);


        unbinder = ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        
        viewModel.getOnNotificationGetObs().observe(getActivity(), res -> {
            Log.d(TAG, "onCreateView: On change called + "+res.getNotifications().size());
            adapter.updateList(res.getNotifications());
        });
        
            
        
        
        return view;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
