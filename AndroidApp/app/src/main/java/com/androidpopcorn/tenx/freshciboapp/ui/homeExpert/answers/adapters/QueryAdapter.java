package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.answers.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetQueryResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QueryAdapter extends RecyclerView.Adapter<QueryAdapter.QueryViewHolder> {


    private static final String TAG = "QueryAdapter";
    private Context mCtx;
    private List<GetQueryResponse.QueryItem> mList;


    @Inject
    public QueryAdapter(@ActivityContext Context mCtx) {
        this.mCtx = mCtx;
        mList = new ArrayList<>();
    }


    @NonNull
    @Override
    public QueryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mCtx).inflate(R.layout.listitem_queries, viewGroup, false);

        return new QueryViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull QueryViewHolder vh, int i) {
        GetQueryResponse.QueryItem current = mList.get(i);

        vh.body.setText(current.getQuery_question());
//        TODO ADD OTHERS

        try {
            vh.tvComment.setVisibility(View.VISIBLE);
            vh.tvComment.setText(current.getComments().get(0).getContent());
        }catch (NullPointerException e){
            Log.d(TAG, "onBindViewHolder: no comment");
            vh.tvComment.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class QueryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivQuerySender)
        ImageView ivQuerySender;
        @BindView(R.id.tvQuerySenderName)
        TextView tvQuerySenderName;
        @BindView(R.id.tv_body)
        TextView body;

        @BindView(R.id.tv_comment)
        TextView tvComment;

        public QueryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void updateList(List<GetQueryResponse.QueryItem> newlist) {
        mList.clear();
        mList.addAll(newlist);
        notifyDataSetChanged();

        final QueryDiffUtilility diffCallback = new QueryDiffUtilility(mList, newlist);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        mList.clear();
        mList.addAll(newlist);
        diffResult.dispatchUpdatesTo(this);
    }
}
