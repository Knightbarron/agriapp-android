package com.androidpopcorn.tenx.freshciboapp.ui.settings.contact;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsFragment extends Fragment {

    @Inject
    SettingsViewModel viewModel;
    @BindView(R.id.care)
    TextView care;
    @BindView(R.id.agspert)
    TextView agspert;
    Unbinder unbinder;

    @Inject
    public ContactUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);


        unbinder = ButterKnife.bind(this, view);


        return view;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.care, R.id.agspert})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.care:

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("message/rfc822");  //set the email recipient
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { care.getText().toString() });
                //let the user choose what email client to use
                startActivity(Intent.createChooser(emailIntent, "Send mail using..."));

                break;
            case R.id.agspert:
                Intent emailIntent2 = new Intent(Intent.ACTION_SEND);
                emailIntent2.setType("message/rfc822");  //set the email recipient
                emailIntent2.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] { agspert.getText().toString() });
                //let the user choose what email client to use
                startActivity(Intent.createChooser(emailIntent2, "Send mail using..."));
                break;
        }
    }
}
