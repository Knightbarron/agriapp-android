package com.androidpopcorn.tenx.freshciboapp.ui.main.phone;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.main.SelectionExtra.SelectionExtraFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.otp.OtpFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhonenoFragment extends Fragment {


    private static final String TAG = "PhonenoFragment";

    @Inject
    MainViewModel viewModel;


    @BindView(R.id.phoneno_et)
    EditText phonenoEt;
    @BindView(R.id.btn_otp)
    Button btnOtp;
    Unbinder unbinder;
    @BindView(R.id.tv_phone_message)
    TextView tvPhoneMessage;

    @Inject
    SelectionExtraFragment selectionExtraFragment;

    @Inject
    OtpFragment otpFragment;


    @Inject
    public PhonenoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_phoneno, container, false);

        unbinder = ButterKnife.bind(this, view);
        susbcribeObservers();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_otp)
    public void onViewClicked() {

        String phone = phonenoEt.getText().toString();


//        save phone number
        viewModel.saveUserPhone(phone);


        Log.d(TAG, "onViewClicked: Phone no is: " + phone);

        if (phone.length() != 10) {
            tvPhoneMessage.setText("The phone no should be of 10 digits");
            return;
        }


        viewModel.sendOtpRequest(phone);


    }


    private void susbcribeObservers() {
        viewModel.getIsSend().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    initializeFragments(otpFragment);
                } else {
                   tvPhoneMessage.setText("Error sending otp");
                }
            }
        });
    }


    @OnClick(R.id.btn_otp_login)
    public void loginFrag() {
        initializeFragments(selectionExtraFragment);
    }


    public void initializeFragments( Fragment frag){
        String backStateName = frag.getClass().toString();
        //Log.d(TAG, "onBtnOtpLoginClicked: " + backStateName);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_right,R.anim.enter_from_right,R.anim.exit_to_right);

        transaction.replace(R.id.frame_main,frag);
        transaction.addToBackStack(backStateName);

        transaction.commit();
    }



}

