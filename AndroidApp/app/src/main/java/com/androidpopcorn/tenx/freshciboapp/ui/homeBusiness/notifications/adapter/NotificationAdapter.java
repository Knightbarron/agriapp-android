package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.notifications.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private static final String TAG = "NotificationAdapter";
    private ArrayList<NotificationsClass> mList;

    private View.OnClickListener onItemClickListener;

    public void setOnItemClickListener(View.OnClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Inject
    public NotificationAdapter() {
        this.mList = new ArrayList<>();
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem_notifications,viewGroup,false);
        return  new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder notificationViewHolder, int i) {
        notificationViewHolder.profilePic.setImageResource(mList.get(i).getProfilePic());
        notificationViewHolder.title.setText(mList.get(i).getTitle());
        notificationViewHolder.body.setText(mList.get(i).getBody());
    }

    public void updateListItems(List<NotificationsClass> mList){
            this.mList.addAll(mList);
            notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        ImageView profilePic;
        TextView title;
        TextView body;


        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setTag(this);

            profilePic = itemView.findViewById(R.id.imageView);
            title   = itemView.findViewById(R.id.name);
            body = itemView.findViewById(R.id.tv_body);

        }

    }
}
