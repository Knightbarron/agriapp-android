package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.answers.adapters;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.text.TextUtils;

import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetQueryResponse;

import java.util.List;

public class QueryDiffUtilility extends DiffUtil.Callback {
    private List<GetQueryResponse.QueryItem> oldList;
    private List<GetQueryResponse.QueryItem> newList;

    public QueryDiffUtilility(List<GetQueryResponse.QueryItem> oldList, List<GetQueryResponse.QueryItem> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int i, int i1) {
        return oldList.get(i).getConversationID() == newList.get(i1).getConversationID();
    }

    @Override
    public boolean areContentsTheSame(int i, int i1) {
        return oldList.get(i).getConversationID() == newList.get(i1).getConversationID();
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
