package com.androidpopcorn.tenx.freshciboapp.Data.model.farmer;

import com.androidpopcorn.tenx.freshciboapp.Data.model.CategoryBody;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FarmerCategoryBody {

        @SerializedName("categories")
        private ArrayList<CategoryBody> categories;


    public FarmerCategoryBody() {
    }

    public FarmerCategoryBody(ArrayList<CategoryBody> categories) {
        this.categories = categories;
    }

    public ArrayList<CategoryBody> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoryBody> categories) {
        this.categories = categories;
    }


}
