package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.answers.FarmerAnswersFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.base.FarmerBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection.FarmerCategoryFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCompleteProfile.FarmerCompleteProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.homefeed.FarmerHomeFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.notifications.FarmerNotificationsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.profile.FarmerProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsActivity;
import com.jaeger.library.StatusBarUtil;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class HomeFarmerActivity extends AppCompatActivity  implements HasSupportFragmentInjector {

    private static final String TAG = "HomeFarmerActivity";


    @Inject
    FarmerAnswersFragment farmerAnswersFragment;

    @Inject
    FarmerBaseFragment farmerBaseFragment;

    @Inject
    FarmerHomeFragment farmerHomeFragment;

    @Inject
    FarmerNotificationsFragment farmerNotificationsFragment;

    @Inject
    FarmerProfileFragment farmerProfileFragment;

    @Inject
    HomeFarmerViewModel viewModel;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    Toolbar toolbar;

    @Inject
    FarmerCategoryFragment farmerCategoryFragment;

    @Inject
    FragmentManager fm;

    @Inject
    FarmerCompleteProfileFragment farmerCompleteProfileFragment;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    initializeFragments(fm,farmerBaseFragment);
                    setToolBar(R.drawable.agspert_logo_regular);
                    return true;
                case R.id.navigation_notifications:
                    initializeFragments(fm,farmerHomeFragment);
                    setToolBar(R.drawable.feedpage_logo);
                    return true;
                case R.id.navigation_base:
                    initializeFragments(fm,farmerNotificationsFragment);
                    setToolBar(R.drawable.notifications_logo);
                    return true;
                case R.id.navigation_my_answers:
                    initializeFragments(fm,farmerAnswersFragment);


                    return true;
                case R.id.navigation_my_profile:
                    initializeFragments(fm,farmerProfileFragment);
                    setToolBar(R.drawable.my_profile_logo);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        //injection
        AndroidInjection.inject(this);

//        Window w = getWindow();
//        w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        StatusBarUtil.setTransparent(this);



        Log.d(TAG, "onCreate: Created ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_farmer);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        changeIconSize(navView);


        if(viewModel == null){
            Log.d(TAG, "onCreate: VIEW MODEL IS NULL");
        }else {
            Log.d(TAG, "onCreate: VIEW MODEL IS NOT NULL");
        }

        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setToolBar(R.drawable.agspert_logo_regular);

        initializeFragments(fm,farmerCompleteProfileFragment);

        ImageView ic = findViewById(R.id.ig_icon);
        ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeFarmerActivity.this, SettingsActivity.class);
                intent.putExtra("CustomerType","Farmer");
                startActivity(intent);
            }
        });

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void setToolBar(int drawable){
        getSupportActionBar().setIcon(drawable);
        getSupportActionBar().setTitle("");
    }

    private void changeIconSize(BottomNavigationView navView) {

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
        final View iconView = menuView.getChildAt(2).findViewById(android.support.design.R.id.icon);
        final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
        final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, displayMetrics);
        layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, displayMetrics);
        iconView.setLayoutParams(layoutParams);

    }

    public void initializeFragments(FragmentManager fm, Fragment frag){
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_farmer, frag);
        ft.commit();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

}
