package com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsBusinessEditProfileFragment extends Fragment {


    @Inject
    SettingsViewModel viewModel;

    private static final String TAG = "SettingsBusinessEditPro";
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_location)
    EditText etOrganization;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;
    Unbinder unbinder;
    @BindView(R.id.et_business_location)
    EditText etBusinessLocation;
    @BindView(R.id.specialization_tv)
    TextView businessTv;
    @BindView(R.id.spinner_specialization)
    Spinner spinnerBusinessType;
    @BindView(R.id.type1)
    CheckBox type1;
    @BindView(R.id.type2)
    CheckBox type2;
    @BindView(R.id.type3)
    CheckBox type3;
    @BindView(R.id.type4)
    CheckBox type4;
    @BindView(R.id.type5)
    CheckBox type5;
    @BindView(R.id.type6)
    CheckBox type6;
    @BindView(R.id.role_tv)
    TextView roleTv;
    @BindView(R.id.spinner_role)
    Spinner spinnerRole;


    String typeBusiness;
    String typeRole;
    ArrayList<String> businessType;

    @Inject
    public SettingsBusinessEditProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings_business_edit_profile, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        unbinder = ButterKnife.bind(this, view);

        observerForPatchProfile();
        initSpinners();
        viewModel.getProfileDataBusiness();
        observerForProfile();


        return view;
    }

    private void observerForProfile() {
        viewModel.getBusinessProfileData().observe(this, new Observer<BusinessProfileResponse.Business>() {
            @Override
            public void onChanged(@Nullable BusinessProfileResponse.Business business) {
                updateProfile(business);
            }
        });
    }
    public void updateProfile(BusinessProfileResponse.Business data) {

        if (!TextUtils.isEmpty(data.getName())) {
            etName.setText(data.getName());
        }
        if (!TextUtils.isEmpty(data.getOrganization())){
            etOrganization.setText(data.getOrganization());
        }
        if (!TextUtils.isEmpty(data.getAddress())){
            etAddress.setText(data.getAddress());
        }
        if (!TextUtils.isEmpty(data.getEmail())){
            etEmail.setText(data.getEmail());
        }
        if (!TextUtils.isEmpty(data.getLocation())){
            etBusinessLocation.setText(data.getLocation());
        }
        if (!TextUtils.isEmpty(data.getBusiness_role())){
            roleTv.setText(data.getBusiness_role());
        }
        if (!TextUtils.isEmpty(data.getBusiness_type_1())){
            businessTv.setText(data.getBusiness_type_1());
        }


    }


    private void initSpinners() {

        String[] businessTypedata = new String[]{"Partnership Firm", "Private Company", "Non-Profit Organization", "Other"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, businessTypedata);
        spinnerBusinessType.setAdapter(adapter);
        typeBusiness = spinnerBusinessType.getSelectedItem().toString();
        spinnerBusinessType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeBusiness = spinnerBusinessType.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Specialization : " + typeBusiness);
                businessTv.setText(typeBusiness);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        String[] roleCompany = new String[]{"Owner", "Partner", "Employee"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, roleCompany);
        spinnerRole.setAdapter(adapter1);
        typeRole = spinnerRole.getSelectedItem().toString();

        spinnerRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeRole = spinnerRole.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Specialization : " + typeRole);
                roleTv.setText(typeRole);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
    private int knowBusinessType(){
        businessType  = new ArrayList<>();
        int i =0;

        if(type1.isChecked()){
            i++;
            businessType.add("Sell to farmers and other services");
        }if(type2.isChecked()){
            i++;
            businessType.add("Buy from farmers and other services");
        }    if(type3.isChecked()){
            i++;
            businessType.add("Both Buy and Sell");
        }if(type4.isChecked()){
            i++;
            businessType.add("Not for profit");
        }    if(type5.isChecked()){
            i++;
            businessType.add("Advertising and Marketing");
        }if(type6.isChecked()){
            i++;
            businessType.add("Research and Lab testing");
        }
        return i;
    }

    private void observerForPatchProfile() {

        viewModel.getStatusPatchBusProfile().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    Toast.makeText(getActivity(), "The profile has been updated", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Unsuccessful updating the profile", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.edit_btn_continue)
    public void onViewClicked() {

        //int count =  knowBusinessType();

            String busName = etName.getText().toString();
            String busLocation = etAddress.getText().toString();
            String busEmail = etEmail.getText().toString();
            String busOrg = etOrganization.getText().toString();
            String address = etBusinessLocation.getText().toString();

            StringBuilder sb = new StringBuilder();
            for (String s : businessType)
            {
                sb.append(s);
                sb.append("\t, ");
            }


            viewModel.patchBusinessProfile(new BusinessProfileBody(busName,busEmail,busLocation,
                    typeBusiness,sb.toString(),typeRole,address,busOrg));

        }

        //viewModel.patchBusinessProfile(new BusinessProfileBody(busName,busOrg,busLocation,busEmail));

}
