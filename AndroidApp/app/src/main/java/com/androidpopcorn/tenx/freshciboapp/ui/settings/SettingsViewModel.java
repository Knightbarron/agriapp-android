package com.androidpopcorn.tenx.freshciboapp.ui.settings;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.ExpertPatchResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.business.BusinessProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.base.BaseViewModel;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class SettingsViewModel extends BaseViewModel {

    private static final String TAG = "SettingsViewModel";

    private AppDataManager appDataManager;

    //Farmer Profile Update
    public MutableLiveData<Boolean> statusFarmerProfile;
    public LiveData<Boolean> getStatusFarmerProfile(){

        if (statusFarmerProfile == null)
            statusFarmerProfile = new MutableLiveData<>();
        return statusFarmerProfile;
    }

    //Business PRofile Update
    public MutableLiveData<Boolean> statusPatchBusProfile;

    public LiveData<Boolean> getStatusPatchBusProfile(){
        if (statusPatchBusProfile == null)
            statusPatchBusProfile = new MutableLiveData<>();
        return statusPatchBusProfile;
    }
    
    //Expert Profile Update
    public MutableLiveData<Boolean> statusPatchExpertProfile;
    
    public LiveData<Boolean> getStatusPatchExpertProfile(){
        if (statusPatchExpertProfile == null)
            statusPatchExpertProfile = new MutableLiveData<>();
        return statusPatchExpertProfile;
    }

    private MutableLiveData<String> userType;

    public LiveData<String> getUserType(){
        if (userType == null)
            userType = new MutableLiveData<>();
        return userType;
    }
    int farmer_id ;

    private MutableLiveData<BusinessProfileResponse.Business> businessProfileData;

    public LiveData<BusinessProfileResponse.Business> getBusinessProfileData(){
        if (businessProfileData == null)
            businessProfileData = new MutableLiveData<>();
        return businessProfileData;
    }


    //Data for Farmer Profile
    public MutableLiveData<FarmerProfileResponse.Farmer> farmerProfileResponse;

    public LiveData<FarmerProfileResponse.Farmer> getFarmerProfileResponse(){
        if (farmerProfileResponse == null)
            farmerProfileResponse = new MutableLiveData<>();
        return farmerProfileResponse;
    }


    public SettingsViewModel(AppDataManager appDataManager, Application application) {
        super(appDataManager, application);

        this.appDataManager = appDataManager;

    }

    //For patching the farmer details
    public void saveFarmerProfile(FarmerCompleteProfileBody body){
        appDataManager.saveCompleteProfileDetails(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {

                if (statusFarmerProfile == null)
                    statusFarmerProfile = new MutableLiveData<>();
                    if (defaultResponseResponse.code()==200){
                        try{
                            Log.d(TAG, "onNext: Success...");
                            statusFarmerProfile.setValue(true);
                        }catch (NullPointerException e){
                            statusFarmerProfile.setValue(false);
                            Log.d(TAG, "onNext: ",e);
                        }
                    }
            }

            @Override
            public void onError(Throwable e) {
                statusFarmerProfile.setValue(false);
                Log.e(TAG, "onError: ",e );
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Method of the Farmer PRofile");
            }
        });
    }

    //For patching the business profile
    public void patchBusinessProfile(BusinessProfileBody body){
        appDataManager.patchBusinessProfile(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {
                if (statusPatchBusProfile==null)
                    statusPatchBusProfile = new MutableLiveData<>();

                if (defaultResponseResponse.code()==200){
                    try {
                        statusPatchBusProfile.setValue(true);
                        Log.d(TAG, "onNext: Patched business profile....");
                    }catch (NullPointerException e){
                        statusPatchBusProfile.setValue(false);
                        Log.d(TAG, "onNext: Null pointer exception");
                    }
                }else{
                    statusPatchBusProfile.setValue(false);
                    Log.d(TAG, "onNext : " + defaultResponseResponse.code());
                }


            }

            @Override
            public void onError(Throwable e) {
                statusPatchBusProfile.setValue(false);
                Log.e(TAG, "onError: ", e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: PAtching the business profile completed");
            }
        });

    }
    
    //for patching the expert details
    public void patchExpertProfile(ExpertPatchBody body){
        appDataManager.updateExpertProfile(body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<ExpertPatchResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                    getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<ExpertPatchResponse> expertPatchResponseResponse) {

                if (statusPatchExpertProfile == null)
                    statusPatchExpertProfile = new MutableLiveData<>();
                if (expertPatchResponseResponse.code()==200){
                    try {
                        statusPatchExpertProfile.setValue(true);
                        Log.d(TAG, "onNext: Success ");
                    }catch (NullPointerException e){
                        statusPatchExpertProfile.setValue(false);
                        Log.d(TAG, "onNext: Null pointer exception");
                    }
                }else{
                    statusPatchExpertProfile.setValue(false);
                    Log.d(TAG, "onNext: Not successful");
                }
                
            }

            @Override
            public void onError(Throwable e) {
                statusPatchExpertProfile.setValue(false);
                Log.e(TAG, "onError: ",e );
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: MEthod of the PAtching expert details");
            }
        });
    }

    public void setUserType(String user){
        if (userType == null)
            userType = new MutableLiveData<>();
        userType.setValue(user);

    }

    //get profile data for farmer
    public void getFarmerProfileData() {
        appDataManager.getCurrentFarmerProfile().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<FarmerProfileResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<FarmerProfileResponse> farmerProfileResponseResponse) {

                        if (farmerProfileResponse == null)
                            farmerProfileResponse = new MutableLiveData<>();
                        if (farmerProfileResponseResponse.code()==200){
                            try {
                                Log.d(TAG, "onNext: " + farmerProfileResponseResponse.body().getFarmer().getLocation());
                                Log.d(TAG, "onNext: " + farmerProfileResponseResponse.body().getFarmer().getName());
                                farmerProfileResponse.setValue(farmerProfileResponseResponse.body().getFarmer());
                                farmer_id = farmerProfileResponseResponse.body().getFarmer().getFarmer_id();
                                Log.d(TAG, "onNext: Farmer id IS : " + farmer_id);
                            }catch (NullPointerException e){
                                Log.d(TAG, "onNext:NUll pointer exception . ");
                            }
                        }else{
                            Log.d(TAG, "onNext: Not successful");
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ",e );
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Current profile of farmer retrieved");
                    }
                });

    }

    //get profile details for business
    public void getProfileDataBusiness(){
        appDataManager.getBusinessProfile().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<BusinessProfileResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<BusinessProfileResponse> businessProfileResponseResponse) {
                if (businessProfileData == null)
                    businessProfileData = new MutableLiveData<>();
                if (businessProfileResponseResponse.code()==200){
                    try{

                        businessProfileData.setValue(businessProfileResponseResponse.body().getBusiness());
                    }catch (NullPointerException e){
                        Log.d(TAG, "onNext: Null pointer Exception");
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: ",e );
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: Profile Data retrival");
            }
        });
    }
}
