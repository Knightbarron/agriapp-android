package com.androidpopcorn.tenx.freshciboapp.ui.settings;

import android.app.Application;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.base.ViewModelFactory;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityScope;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsModule  {


    @Provides
    @ActivityContext
    @ActivityScope
    Context provideContext(SettingsActivity settingsActivity){
        return settingsActivity;
    }


    @Provides
    @ActivityScope
    SettingsViewModel provideVM(@ActivityContext Context context, Application application, AppDataManager appDataManager){

        SettingsViewModel vm = new SettingsViewModel(appDataManager, application);
        ViewModelFactory<SettingsViewModel> factory = new ViewModelFactory<>(vm,appDataManager,application);
        return ViewModelProviders.of((SettingsActivity) context, factory).get(SettingsViewModel.class);

    }
}
