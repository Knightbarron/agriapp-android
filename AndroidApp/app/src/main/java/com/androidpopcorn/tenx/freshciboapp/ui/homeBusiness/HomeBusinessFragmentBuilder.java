package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness;


import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.answers.BusinessAnswersFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.base.BusinessBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory.BusinessCategoryFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCompleteProfile.BusinessCompleteProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCreatePost.BusinessCreatePostFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.homefeed.BusinessHomeFeedFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.notifications.BusinessNotificationsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.options.BusinessToAddPackage;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.options.BusinessToCollab;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.options.BusinessToCreateAd;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.profile.BusinessProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.uploadDocuments.BusinessUploadDocumentsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.expertCompleteProfile.ExpertCompleteProfile;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HomeBusinessFragmentBuilder {

    @ContributesAndroidInjector(modules = {})
    abstract BusinessAnswersFragment bindBusinessAnswers();

    @ContributesAndroidInjector(modules = {})
    abstract BusinessBaseFragment bindBusinessBase();

    @ContributesAndroidInjector(modules = {})
    abstract BusinessHomeFeedFragment bindBusinessHomeFeed();

    @ContributesAndroidInjector(modules = {})
    abstract BusinessNotificationsFragment bindBusinessNotifications();

    @ContributesAndroidInjector(modules = {})
    abstract BusinessProfileFragment bindBusinessProfile();

    @ContributesAndroidInjector(modules = {})
    abstract BusinessCreatePostFragment bindBusinessCreatePost();

    @ContributesAndroidInjector(modules = {})
    abstract BusinessCompleteProfileFragment bindBusinessCompleteProfile();

    @ContributesAndroidInjector(modules = {})
    abstract BusinessUploadDocumentsFragment bindBusinessUploadDocuments();

    @ContributesAndroidInjector(modules = {})
    abstract BusinessCategoryFragment bindBusinessCategoryFragment();

    @ContributesAndroidInjector
    abstract BusinessToAddPackage bindBusinessToAddPackFragment();

    @ContributesAndroidInjector
    abstract BusinessToCollab bindBusinessToCollabFragment();

    @ContributesAndroidInjector
    abstract BusinessToCreateAd bindBusinessToCreateAdFragment();


}
