package com.androidpopcorn.tenx.freshciboapp.Data.model.profile;

import com.google.gson.annotations.SerializedName;

public class GetBusinessResponse {
    @SerializedName("business")
    private BusinessData businessData;

    public BusinessData getBusinessData() {
        return businessData;
    }

    public void setBusinessData(BusinessData businessData) {
        this.businessData = businessData;
    }

    public class BusinessData {


        @SerializedName("business_id")
        private float business_id;

        @SerializedName("business_name")
        private String business_name;
        @SerializedName("business_email")
        private String business_email = null;
        @SerializedName("business_address")
        private String business_address = null;
        @SerializedName("business_phone")
        private String business_phone;
        @SerializedName("business_image_path")
        private String business_image_path;
        @SerializedName("business_organization")
        private String business_organization = null;
        @SerializedName("business_tradelicense_path")
        private String business_tradelicense_path = null;
        @SerializedName("business_cert_path")
        private String business_cert_path = null;
        @SerializedName("business_date_created")
        private String business_date_created;
        @SerializedName("business_date_modified")
        private String business_date_modified;


        public float getBusiness_id() {
            return business_id;
        }

        public void setBusiness_id(float business_id) {
            this.business_id = business_id;
        }

        public String getBusiness_name() {
            return business_name;
        }

        public void setBusiness_name(String business_name) {
            this.business_name = business_name;
        }

        public String getBusiness_email() {
            return business_email;
        }

        public void setBusiness_email(String business_email) {
            this.business_email = business_email;
        }

        public String getBusiness_address() {
            return business_address;
        }

        public void setBusiness_address(String business_address) {
            this.business_address = business_address;
        }

        public String getBusiness_phone() {
            return business_phone;
        }

        public void setBusiness_phone(String business_phone) {
            this.business_phone = business_phone;
        }

        public String getBusiness_image_path() {
            return business_image_path;
        }

        public void setBusiness_image_path(String business_image_path) {
            this.business_image_path = business_image_path;
        }

        public String getBusiness_organization() {
            return business_organization;
        }

        public void setBusiness_organization(String business_organization) {
            this.business_organization = business_organization;
        }

        public String getBusiness_tradelicense_path() {
            return business_tradelicense_path;
        }

        public void setBusiness_tradelicense_path(String business_tradelicense_path) {
            this.business_tradelicense_path = business_tradelicense_path;
        }

        public String getBusiness_cert_path() {
            return business_cert_path;
        }

        public void setBusiness_cert_path(String business_cert_path) {
            this.business_cert_path = business_cert_path;
        }

        public String getBusiness_date_created() {
            return business_date_created;
        }

        public void setBusiness_date_created(String business_date_created) {
            this.business_date_created = business_date_created;
        }

        public String getBusiness_date_modified() {
            return business_date_modified;
        }

        public void setBusiness_date_modified(String business_date_modified) {
            this.business_date_modified = business_date_modified;
        }
    }
}
