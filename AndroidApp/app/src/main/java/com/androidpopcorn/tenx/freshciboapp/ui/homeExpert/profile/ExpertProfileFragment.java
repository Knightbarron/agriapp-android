package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.profile;



import android.arch.lifecycle.Observer;

import android.Manifest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.Nullable;

import android.provider.MediaStore;

import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageBody;

import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertProfileResponse;


import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.profile.adapters.ExpertPackageAdapter;
import com.androidpopcorn.tenx.freshciboapp.utils.FileUtil;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertProfileFragment extends Fragment {

    private static final String TAG = "ExpertProfileFragment";

//    activity result key

    public static final int RESULT_CODE_IMAGE = 1022;

    //    image file
    File imageFile = null;
    @Inject
    HomeExpertViewModel viewModel;

    //    recycler adapter
    @Inject
    ExpertPackageAdapter adapter;


    @BindView(R.id.add_new_package)
    ImageView addNewPackage;
    Unbinder unbinder;
    String expert_bio;
    String timeType;
    String durationPrice;

    @BindView(R.id.profile_pic)
    CircleImageView profilePic;

    @BindView(R.id.profile_name)
    TextView profileName;

    @BindView(R.id.profile_specialization)
    TextView profileSpecialization;

    @BindView(R.id.profile_workplace)
    TextView profileWorkplace;


    @BindView(R.id.tv_bio)
    TextView tvBio;

    CircleImageView civDemo;


//    TODO REMOVE THIS


    @BindView(R.id.recycler_packages)
    RecyclerView recyclerPackages;

    @Inject
    public ExpertProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        viewModel.getCurrentExpertProfile();
        viewModel.getCurrentExpertPackages();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expert_profile, container, false);

        unbinder = ButterKnife.bind(this, view);


        recyclerPackages.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerPackages.setAdapter(adapter);


        viewModel.getOnExpertProfilePatchObs().observe(getActivity(), res -> {
//            update about me for now
           try {
               assert res != null;
               String about = res.getUpdates().getAboutMe();
               if(!TextUtils.isEmpty(about)){
                   tvBio.setText(about);
               }
           }catch (NullPointerException e){
               Log.d(TAG, "onCreateView: No updates recieved");
           }
        });


//        this will be called when package is added
        viewModel.getSinglePackageObserver().observe(getActivity(), res -> {
//            new package was added or changed || update in adapter
            adapter.updateSinglePackage(res);

        });



        return view;
    }



    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }
    @OnClick(R.id.tv_bio)
    public void onClickBio() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_bio, null);
        dialogBuilder.setView(dialogView);
        String prev = tvBio.getText().toString();
        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        edt.setText(prev);
        edt.setSingleLine(false);

        dialogBuilder.setTitle("Write something about yourself");

        dialogBuilder.setPositiveButton("Done", (dialog, whichButton) -> {
            //do something with edt.getText().toString();
            expert_bio = edt.getText().toString();
            Log.d(TAG, "onClick: " + expert_bio);
            viewModel.updateExpertProfile(new ExpertPatchBody(expert_bio));


        });
        dialogBuilder.setNegativeButton("Cancel", (dialog, whichButton) -> {
            //pass
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }
    @OnClick(R.id.add_new_package)
    public void onClickPackage() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_package, null);
        dialogBuilder.setView(dialogView);


        final EditText serviceName = dialogView.findViewById(R.id.add_a_service);
        final EditText serviceDuration = dialogView.findViewById(R.id.add_duration);
        final EditText serviceCategory = dialogView.findViewById(R.id.service_category);
        final EditText serviceDescription = dialogView.findViewById(R.id.service_description);
        final EditText serviceprice = dialogView.findViewById(R.id.add_price);
        Spinner timeSpinner = (Spinner) dialogView.findViewById(R.id.spinner_duration);
        Spinner perHourSpinner = (Spinner) dialogView.findViewById(R.id.spinner_time);
        TextView timeTv = (TextView) dialogView.findViewById(R.id.duration_et);
        TextView priceTv = (TextView) dialogView.findViewById(R.id.et_price);

        serviceDescription.setSingleLine(false);

        Button saveBtn = (Button) dialogView.findViewById(R.id.btn_save);
        ImageView cancelBtn = (ImageView) dialogView.findViewById(R.id.imageView);

        String[] datatime = new String[]{"hours", "days", "Weeks"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, datatime);
        timeSpinner.setAdapter(adapter1);

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                timeType = timeSpinner.getSelectedItem().toString();

                Log.d(TAG, "initSpinners: Qualification : " + timeType);

                timeTv.setText(timeType);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String[] perhour = new String[]{"per hour", "per day", "per week"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, perhour);
        perHourSpinner.setAdapter(adapter2);

        perHourSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                durationPrice = perHourSpinner.getSelectedItem().toString();

                Log.d(TAG, "initSpinners: Qualification : " + durationPrice);

                priceTv.setText(durationPrice);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        AlertDialog b = dialogBuilder.create();


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Hoi goise");

                String name = serviceName.getText().toString();
                String duration = serviceDuration.getText().toString();
                String category = serviceCategory.getText().toString();
                String description = serviceDescription.getText().toString();
                String price = serviceprice.getText().toString();

                duration = duration + " " + timeType;
                price = price + " " + durationPrice;


                Log.d(TAG, "onClick: name: " + name + "Duration : " + duration +
                        "Category : " + category + "Description : " + description + "Price : " + price);
                b.dismiss();



                viewModel.saveExpertPackageDetails(new ExpertPackageBody(name,description,price,duration,category));


            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Cancelling");
                b.dismiss();
            }
        });


        b.show();

    }
    @OnClick(R.id.profile_pic)
    public void showUpdatePhotoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_update_profileimage, null);
        builder.setView(dialogView);
        Button btnupload = (Button) dialogView.findViewById(R.id.btn_upload);
        civDemo = dialogView.findViewById(R.id.civ_profile_demo);
        Button btnselectphoto = (Button) dialogView.findViewById(R.id.btn_select_photo);
        btnselectphoto.setOnClickListener(v -> {
            searchImage();
        });

        btnupload.setOnClickListener(v -> {
            if (imageFile == null) {
                Toast.makeText(getActivity(), "Select a image first", Toast.LENGTH_SHORT).show();
                return;
            }
            viewModel.uploadProfilePicture(imageFile);
        });


        builder.create().show();

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void updateProfileData(ExpertProfileResponse data) {

        Log.d(TAG, "updateProfileData: " + data);
        if (!TextUtils.isEmpty(data.getAboutMe())) {
            tvBio.setText(data.getAboutMe());
        }
        if (!TextUtils.isEmpty(data.getSpecialization())) {
            String spec = "Specialization : " + data.getSpecialization();
            if (!TextUtils.isEmpty(data.getSpecialization2())) {
                spec += ", " + data.getSpecialization2();
            }
            profileSpecialization.setText(spec);
        }
        if (!TextUtils.isEmpty(data.getOrganization())) {
            profileWorkplace.setText(data.getOrganization());
        }

        if (!TextUtils.isEmpty(data.getFullName())) {
            profileName.setText(data.getFullName());
        }

        if(!TextUtils.isEmpty(data.getImage_path())){
//            loadImageInCircleView()
            loadImageInCircleView(data.getImage_path());
        }
    }
    public void loadImageInCircleView(String image_path) {
        Glide.with(getActivity()).load(ApiService.BASE_URL +"/"+ image_path).into(profilePic);
    }
    public void notifyError(String message) {
//        TODO REVIEW THIS
        if(getActivity() != null){
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }
    public void updatePackageData(ExpertPackageResponse data) {
        adapter.updateData(data.getPackages());
    }
    public void searchImage() {
        Dexter.withActivity(getActivity()).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select a PDF "), RESULT_CODE_IMAGE);
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(getActivity(), "Enable storage read permission", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }).check();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_CODE_IMAGE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                imageFile = getFile(uri);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    if(civDemo != null){
                        civDemo.setImageBitmap(bitmap);
                    }

                }catch (FileNotFoundException e){
                    throw new Error("File not found");
                }catch (IOException e){
                    throw new Error("IO exception");
                }
            }
        }
    }


    public File getFile(Uri uri) {
        try {
            File file = FileUtil.from(getActivity(), uri);
            Log.d("file", "File...:::: uti - " + file.getPath() + " file -" + file + " : " + file.exists());

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
