package com.androidpopcorn.tenx.freshciboapp.Data.model.Expert;

import com.google.gson.annotations.SerializedName;

public class ExpertProfileResponse {


    @SerializedName("specialist_id")
    private float specialistID;

    @SerializedName("specialist_full_name")
    private String fullName;

    @SerializedName("specialist_email")
    private String email;

    @SerializedName("specialist_address")
    private String address;

    @SerializedName("specialist_phone")
    private String phone;

    @SerializedName("specialist_image_path")
    private String image_path;

    @SerializedName("specialist_organization")
    private String organization;

    @SerializedName("specialist_qualification")
    private String qualification;

    @SerializedName("specialist_specialization")
    private String specialization;

    @SerializedName("specialist_specialization2")
    private String specialization2;

    @SerializedName("specialist_cert_pdf_path")
    private String specialistPdf;

    @SerializedName("specialist_companyid_pdf_path")
    private String companyPdf;

    @SerializedName("specialist_about_me")
    private String aboutMe;

    @SerializedName("specialist_date_created")
    private String dateCreated;

    @SerializedName("specialist_date_modified")
    private String dateModified;

    // Getter Methods

    public float getSpecialistID() {
        return specialistID;
    }

    public void setSpecialistID(float specialistID) {
        this.specialistID = specialistID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getSpecialization2() {
        return specialization2;
    }

    public void setSpecialization2(String specialization2) {
        this.specialization2 = specialization2;
    }

    public String getSpecialistPdf() {
        return specialistPdf;
    }

    public void setSpecialistPdf(String specialistPdf) {
        this.specialistPdf = specialistPdf;
    }

    public String getCompanyPdf() {
        return companyPdf;
    }

    public void setCompanyPdf(String companyPdf) {
        this.companyPdf = companyPdf;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }
    @Override
    public String toString() {
        return fullName +" || "+ organization +" || "+ specialization +" || "+ specialization2;
    }
}
