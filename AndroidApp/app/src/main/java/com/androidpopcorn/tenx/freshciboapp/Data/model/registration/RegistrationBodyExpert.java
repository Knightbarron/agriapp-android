package com.androidpopcorn.tenx.freshciboapp.Data.model.registration;

import com.google.gson.annotations.SerializedName;

public class RegistrationBodyExpert {

    @SerializedName("specialist_full_name")
    private String name;


    @SerializedName("specialist_password")
    private String password;


    @SerializedName("specialist_phone")
    private String phone;

    public RegistrationBodyExpert(String name, String password, String phone) {

        this.password = password;
        this.name = name;
        this.phone = phone;
    }

    public RegistrationBodyExpert() {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
