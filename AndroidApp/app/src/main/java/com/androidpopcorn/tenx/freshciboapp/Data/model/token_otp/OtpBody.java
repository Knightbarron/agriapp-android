package com.androidpopcorn.tenx.freshciboapp.Data.model.token_otp;

import com.google.gson.annotations.SerializedName;

public class OtpBody {
    @SerializedName("session_id")
    private String session_id;


    @SerializedName("otp")
    private String otp;


    public OtpBody(String session_id, String otp) {
        this.session_id = session_id;
        this.otp = otp;
    }

    public OtpBody() {
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
