package com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFarmerEditProfileFragment extends Fragment {

    @Inject
    SettingsViewModel viewModel;

    private static final String TAG = "SettingsEditProfileFrag";
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_location)
    EditText etLocation;
    @BindView(R.id.et_market)
    EditText etMarket;
    @BindView(R.id.et_land)
    EditText etLand;
    @BindView(R.id.landTypeTv)
    TextView landTypeTv;
    @BindView(R.id.spinner_area_type)
    Spinner spinnerAreaType;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;
    Unbinder unbinder;

    String selectedLandOwner;
    Boolean selectedOrganic;

    String[] dataLandType;
    int organic;
    FarmerProfileResponse.Farmer farmerData=null;

    @Inject
    public SettingsFarmerEditProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings_edit_profile, container, false);
        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");

        unbinder = ButterKnife.bind(this, view);

        initSpinner();
        viewModel.getFarmerProfileData();



        editBtnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();

            }
        });

        observerFarmerProfile();
        observerForProfileData();





        return view;
    }

    public void observerForProfileData(){
        viewModel.getFarmerProfileResponse().observe(this, new Observer<FarmerProfileResponse.Farmer>() {
            @Override
            public void onChanged(@Nullable FarmerProfileResponse.Farmer farmer) {

                farmerData = farmer;
                updateProfile(farmer);
                Log.d(TAG, "onChanged: " + farmer.getName());
                Log.d(TAG, "onChanged: " + farmer.getLocation());
                Log.d(TAG, "onChanged: " + farmer.getEmail());
                Log.d(TAG, "onChanged: " + farmer.getNearestMarketLocation());
                Log.d(TAG, "onChanged: " + farmer.getLandArea());
                Log.d(TAG, "onChanged: " + farmer.getFarmLandOwner());
                Log.d(TAG, "onChanged: " + farmer.getOrganicFarm());

            }
        });
    }

    public void updateProfile(FarmerProfileResponse.Farmer data) {

        if (!TextUtils.isEmpty(data.getName())) {
            etName.setText(data.getName());
        }if (!TextUtils.isEmpty(data.getLocation())){
            etAddress.setText(data.getLocation());
        }if (!TextUtils.isEmpty(data.getEmail())){
            etEmail.setText(data.getEmail());
        }if (!TextUtils.isEmpty(data.getAddress())){
            etLocation.setText(data.getAddress());
        }if (!TextUtils.isEmpty(data.getNearestMarketLocation())){
            etMarket.setText(data.getNearestMarketLocation());
        }if (!TextUtils.isEmpty(data.getLandArea())){
            etLand.setText(data.getLandArea());
        }


    }

    private void observerFarmerProfile() {
        viewModel.getStatusFarmerProfile().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean){
                    Toast.makeText(getActivity(), "Profile Updated", Toast.LENGTH_SHORT).show();

                }else{

                }
            }
        });
    }


    private void initSpinner() {

        dataLandType = new String[]{"Hector", "Katha", "Bigha"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataLandType);
        spinnerAreaType.setAdapter(adapter1);

        spinnerAreaType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedLandOwner = spinnerAreaType.getSelectedItem().toString();
                Log.d(TAG, "onItemSelected: Area Type is: " + selectedLandOwner);
                landTypeTv.setText(selectedLandOwner);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void sendData() {

        String etFarmLocation = etLocation.getText().toString();
        String etFarmMarket = etMarket.getText().toString();
        String etFarmLand = etLand.getText().toString();
        String landType = landTypeTv.getText().toString();
        String etFarmerName =etName.getText().toString();
        String etFarmerAddress = etAddress.getText().toString();
        String etFarmerEmail = etEmail.getText().toString();

        Log.d(TAG, "onViewClicked: Organic method :" + organic + " LandOwner : "+ selectedLandOwner+ " Farm Location : "+ etFarmLocation
                +" Farm Market : " + etFarmMarket + " Land quantity:" + etFarmLand + " Land type : " + landType + "Farmer NAme  : " +  etFarmerName + "Address : "
        +etFarmerAddress + " Farmer EMail : " + etFarmerEmail);

        String etLand = etFarmLand + " " + landType;
        viewModel.saveFarmerProfile(new FarmerCompleteProfileBody(etFarmerName,
                etFarmerEmail,etFarmerAddress,etFarmLocation
                ,etFarmMarket,etLand,selectedLandOwner,organic));
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
