package com.androidpopcorn.tenx.freshciboapp.ui.settings;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.contact.ContactUsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile.SettingsFarmerEditProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.home.SettingsHomeFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.settings.SettingsFragment;

import java.util.Observable;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class SettingsActivity extends AppCompatActivity implements HasSupportFragmentInjector {

    @Inject
    SettingsViewModel viewModel;

    @Inject
    SettingsFarmerEditProfileFragment settingsFarmerEditProfileFragment;


    @Inject
    SettingsHomeFragment settingsHomeFragment;

    @Inject
    SettingsFragment settingsFragment;

    @Inject
    ContactUsFragment contactUsFragment;

    @Inject
    FragmentManager fm;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    private static final String TAG = "SettingsActivity";

    String userType = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        AndroidInjection.inject(this);


        if (viewModel == null)
            Log.d(TAG, "onCreate: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreate: VIEWMODEL IS NOT NULL");


        initializeFragments(fm,settingsHomeFragment);



        userType = getIntent().getStringExtra("CustomerType");
        viewModel.setUserType(userType);
        Log.d(TAG, "onCreate: " + userType);

    }



    public void initializeFragments(FragmentManager fm, Fragment frag) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, frag);
        ft.commit();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {

        return dispatchingAndroidInjector;

    }


}
