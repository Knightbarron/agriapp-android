package com.androidpopcorn.tenx.freshciboapp.ui.main.selection;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.main.GetStarted.GetStartedFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;
import com.androidpopcorn.tenx.freshciboapp.utils.RoundedCornersTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectionFragment extends Fragment {


    @Inject
    MainViewModel viewModel;

    private static final String TAG = "SelectionFragment";
    @BindView(R.id.as_a_farmer)
    TextView asAFarmer;
    @BindView(R.id.as_an_expert)
    TextView asAnExpert;
    @BindView(R.id.as_an_business)
    TextView asAnBusiness;
    Unbinder unbinder;

    @Inject
    GetStartedFragment getStartedFragment;

    public static int sCorner = 50;
    public static int sMargin = 20;


    @BindView(R.id.farmer_image)
    ImageView farmerImage;
    @BindView(R.id.expert_image)
    ImageView expertImage;
    @BindView(R.id.business_image)
    ImageView businessImage;


    @Inject
    public SelectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_selection, container, false);


        if (viewModel == null)
            Log.d(TAG, "onCreateView: ViewMOdel is NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        unbinder = ButterKnife.bind(this, view);



        setUpImage();

        return view;
    }

    public void setUpImage(){
        Glide.with(this).load(R.drawable.continue_as_farmer)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(farmerImage);
        Glide.with(this).load(R.drawable.continue_as_expert)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(expertImage);
        Glide.with(this).load(R.drawable.continue_as_agri_business)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(businessImage);
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void goToGetStarted() {

        String backStateName = getStartedFragment.getClass().toString();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(R.id.frame_main, getStartedFragment);
        transaction.addToBackStack(backStateName);

        transaction.commit();
    }

    @OnClick({R.id.as_a_farmer, R.id.as_an_expert, R.id.as_an_business})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.as_a_farmer:
                Log.d(TAG, "onViewClicked: Farmer selected");
                viewModel.saveUserType("farmer");
                goToGetStarted();
                break;
            case R.id.as_an_expert:
                Log.d(TAG, "onViewClicked: Expert Selected");
                viewModel.saveUserType("expert");
                goToGetStarted();
                break;
            case R.id.as_an_business:
                Log.d(TAG, "onViewClicked: Business Selected");
                viewModel.saveUserType("business");
                goToGetStarted();
                break;
        }
    }
}
