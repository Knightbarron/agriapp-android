package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCreatePost;


import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.base.BusinessBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.utils.FileUtil;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessCreatePostFragment extends Fragment {


    @Inject
    HomeBusinessViewModel viewModel;

    public static final int REQUEST_CODE_IMAGE = 121;

    private static final String TAG = "BusinessCreatePostFragm";
    @BindView(R.id.et_email)
    EditText etPostBody;
    @BindView(R.id.spinner_specialization)
    Spinner spinnerCategory;
    @BindView(R.id.tv_photo_btn)
    Button tvPhotoBtn;
    @BindView(R.id.iv_image_preview)
    ImageView ivImagePreview;
    @BindView(R.id.post_now)
    Button postNow;

    @BindView(R.id.category_tv)
    TextView categoryTV;
    Unbinder unbinder;
    @BindView(R.id.pv_createpost)
    ScrollView pvCreatepost;

    private String category;
    private File imageFile = null;


    @Inject
    BusinessBaseFragment businessBaseFragment;

    @Inject
    public BusinessCreatePostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_create_post, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");

        unbinder = ButterKnife.bind(this, view);

        initSpinner();

        observerSubscriberForPost();



        return view;
    }

    private void observerSubscriberForPost() {
        viewModel.getStatusAd().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean){
                    Toast.makeText(getActivity(), "Posted", Toast.LENGTH_SHORT).show();
                    //TODO test
                    goToBase();
                }else{
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void goToBase() {
        String backStateName = businessBaseFragment.getClass().toString();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(R.id.container, businessBaseFragment);
        transaction.addToBackStack(backStateName);

        transaction.commit();
    }

    private void initSpinner() {

        String[] categoryData = new String[]{"1","2","3","4","5"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),R.layout.support_simple_spinner_dropdown_item,categoryData);
        spinnerCategory.setAdapter(adapter);

        category = spinnerCategory.getSelectedItem().toString();

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = spinnerCategory.getSelectedItem().toString();
                Log.d(TAG, "onItemSelected: " + category);
                categoryTV.setText(category);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tv_photo_btn, R.id.post_now})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_photo_btn:

                searchImage();
                break;
            case R.id.post_now:
                postData();
                break;
        }
    }

    private void postData() {

        String postBody = etPostBody.getText().toString();
        if (TextUtils.isEmpty(postBody)) {
            Snackbar.make(pvCreatepost, "Post content cannot be empty", Snackbar.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(category) || TextUtils.equals(category, "None")) {
            Snackbar.make(pvCreatepost, "Category cannot be empty", Snackbar.LENGTH_SHORT).show();
            return;
        }

        viewModel.createNewAd(postBody, imageFile);
    }

    private void searchImage() {

        Dexter.withActivity(getActivity()).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select a Image"),REQUEST_CODE_IMAGE);


            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(getActivity(), "You need to give read access", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }).check();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_IMAGE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                imageFile = getFile(uri);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    ivImagePreview.setImageBitmap(bitmap);

                } catch (FileNotFoundException e) {
                    throw new Error("File not found");
                } catch (IOException e) {
                    throw new Error("IO exception");
                }
            }
        }
    }


    public File getFile(Uri uri) {
        try {
            File file = FileUtil.from(getActivity(), uri);
            Log.d("file", "File...:::: uti - " + file.getPath() + " file -" + file + " : " + file.exists());

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
