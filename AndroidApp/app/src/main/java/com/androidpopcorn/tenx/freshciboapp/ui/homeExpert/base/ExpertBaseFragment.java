package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.base;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options.ViewPagerAdapter;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertBaseFragment extends Fragment {

    private static final String TAG = "ExpertBaseFragment";

    @Inject
    HomeExpertViewModel viewModel;

    ViewPager viewPager;

    ViewPagerAdapter adapter;


    @Inject
    public ExpertBaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_expert_base, container, false);
        if (viewModel == null)
            Log.d(TAG, "onCreateView: ViewModel is NULL");
        else
            Log.d(TAG, "onCreateView: ViewModel is NOT NULL");
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPager = view.findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);


        return view;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

}
