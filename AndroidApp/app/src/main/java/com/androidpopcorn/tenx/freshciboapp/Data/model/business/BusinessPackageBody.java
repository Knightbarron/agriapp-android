package com.androidpopcorn.tenx.freshciboapp.Data.model.business;

import com.google.gson.annotations.SerializedName;

public class BusinessPackageBody {


    @SerializedName("type")
    private String type;
    @SerializedName("name")
    private String name;
    @SerializedName("details")
    private String details;
    @SerializedName("collaborate")
    private Boolean collab;
    @SerializedName("price")
    private String price;
    @SerializedName("category_id")
    private int category;


    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public BusinessPackageBody(String type, String name, String details, Boolean collab, String price,int category) {
        this.type = type;
        this.name = name;
        this.details = details;
        this.collab = collab;
        this.price = price;
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Boolean getCollab() {
        return collab;
    }

    public void setCollab(Boolean collab) {
        this.collab = collab;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public BusinessPackageBody() {
    }
}
