package com.androidpopcorn.tenx.freshciboapp.Data.model.Expert;

import com.google.gson.annotations.SerializedName;

public class ExpertPackageBody {

    @SerializedName("package_title")
    private String title;
    @SerializedName("package_details")
    private String details;
    @SerializedName("package_price")
    private String price;
    @SerializedName("package_duration")
    private String duration ;
    @SerializedName("package_category")
    private String category;

    public ExpertPackageBody() {
    }

    public ExpertPackageBody(String title, String details, String price, String duration, String category) {
        this.title = title;
        this.details = details;
        this.price = price;
        this.duration = duration;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
