package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCompleteProfile;


import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection.FarmerCategoryFragment;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class FarmerCompleteProfileFragment extends Fragment {

    @Inject
    HomeFarmerViewModel viewModel;

    private static final String TAG = "FarmerCompleteProfile";
    @BindView(R.id.et_location)
    EditText etLocation;
    @BindView(R.id.et_market)
    EditText etMarket;
    @BindView(R.id.et_land)
    EditText etLand;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;
    Unbinder unbinder;
    @BindView(R.id.others)
    RadioButton others;
    @BindView(R.id.spinner_area_type)
    Spinner spinnerAreaType;
    @BindView(R.id.self)
    RadioButton self;


    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.radioGroup2)
    RadioGroup radioGroup2;


    String[] dataLandType;
    @BindView(R.id.landTypeTv)
    TextView landTypeTv;
    String selectedLandOwner;
    Boolean selectedOrganic;

    private FusedLocationProviderClient fusedLocationProviderClient;

    @Inject
    FarmerCategoryFragment farmerCategoryFragment;


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Inject
    public FarmerCompleteProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_farmer_complete_profile, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");
        unbinder = ButterKnife.bind(this, view);

        initSpinner();

        subscribeObservers();


        editBtnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedOwner = radioGroup.getCheckedRadioButtonId();
                RadioButton ownerButton = view.findViewById(selectedOwner);
                selectedLandOwner = ownerButton.getText().toString();


                int selectedFarming = radioGroup2.getCheckedRadioButtonId();
                RadioButton farmingButton = view.findViewById(selectedFarming);
                if(farmingButton.getText().toString().toLowerCase().equals("yes")){
                    selectedOrganic = true;
                }else {
                    selectedOrganic = false;
                }

                sendData();

            }
        });



        return view;
    }

    private void subscribeObservers() {

        viewModel.getStatusCompleteProfile().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if(s.equals("200")){
                    Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onChanged: Successful");
                    //TODO test


                }else if(s.equals("400")){
                    Toast.makeText(getActivity(), "You are not authorized", Toast.LENGTH_SHORT).show();
                }else if(s.equals("501")){
                    Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendData() {

        String etFarmLocation = etLocation.getText().toString();
        String etFarmMarket = etMarket.getText().toString();
        String etFarmLand = etLand.getText().toString();
        String landType = landTypeTv.getText().toString();

        Log.d(TAG, "onViewClicked: Organic method :" + selectedOrganic + " LandOwner : "+ selectedLandOwner+ " Farm Location : "+ etFarmLocation
                +" Farm Market : " + etFarmMarket + " Land quantity:" + etFarmLand + " Land type : " + landType);


        Log.d(TAG, "sendData: SELECTED ORGANIC TYPE IS WHAT:::::::" + selectedOrganic);

        int statusOrganic;
        if (selectedOrganic){
            statusOrganic = 1;

        }else{
            statusOrganic = 0;
        }
        Log.d(TAG, "sendData: STATUS IS::::" + statusOrganic);

        viewModel.saveCompleteProfileDetails(new FarmerCompleteProfileBody(etFarmLocation,etFarmMarket,etFarmLand,landType, statusOrganic));

    }


    private void initSpinner() {

        dataLandType = new String[]{"Hector", "Katha", "Bigha"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataLandType);
        spinnerAreaType.setAdapter(adapter1);

        spinnerAreaType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedLandOwner = spinnerAreaType.getSelectedItem().toString();
                Log.d(TAG, "onItemSelected: Area Type is: " + selectedLandOwner);
                landTypeTv.setText(selectedLandOwner);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.location_btn)
    public void onViewClicked() {

        attemptPermission();
    }

    private void attemptPermission() {

        Dexter.withActivity(getActivity()).withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                attemptLocation();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();
    }

    private void attemptLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());

        try{

            Task location = fusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()){
                        Log.d(TAG, "onComplete: found location..");
                        Location currentLocation = (Location) task.getResult();
                        Log.d(TAG, "onComplete: LAtitude is : " + currentLocation.getLatitude() +
                                " Longitude  : " + currentLocation.getLongitude());

                        getAddressFromLocation(currentLocation.getLatitude(),currentLocation.getLongitude());
                    }else{
                        Log.d(TAG, "onComplete: curretn location is null");
                        Toast.makeText(getActivity(),"Unable to get location",Toast.LENGTH_SHORT).show();
                    }
                }
            }) ;


        }catch (SecurityException e){
            Log.d(TAG, "attemptLocation: Security Exception : " + e.getMessage());
        }
    }

    private void getAddressFromLocation(double latitude, double longitude) {

        Geocoder geocoder = new Geocoder(getContext(), Locale.ENGLISH);


        try{
            List<Address> addresses = geocoder.getFromLocation(latitude,longitude,1);

            if (addresses.size()>0){
                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();

//                for(int i =0 ;i<fetchedAddress.getMaxAddressLineIndex();i++){
//
//                    strAddress.append(fetchedAddress.getLocality()).append(" ");
//                }

                Log.d(TAG, "getAddressFromLocation: LOCALITY IS : " + fetchedAddress.getLocality());

                etLocation.setText(fetchedAddress.getLocality());


            }else{
                Log.d(TAG, "getAddressFromLocation: Searching for location" );
            }


        }catch (IOException e){
            e.printStackTrace();
            Log.d(TAG, "getAddressFromLocation: COuld not get Address");
        }
    }



}
