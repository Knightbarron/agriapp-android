package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.paging;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.androidpopcorn.tenx.freshciboapp.utils.NetworkState;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeedPagingAdapter extends PagedListAdapter<GetFeedResponse.FeedData, RecyclerView.ViewHolder> {

    private static final String TAG = "FeedPagingAdapter";
    private static final int TYPE_PROGRESS = 0;
    private static final int TYPE_ITEM = 1;

    private Context context;
    private NetworkState networkState;

    @Inject
    public FeedPagingAdapter(@ActivityContext Context context) {
        super(DIFF_CALLBACK);
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewtype) {
        LayoutInflater inflater = LayoutInflater.from(context);

        if(viewtype == TYPE_ITEM){
            View v = inflater.inflate(R.layout.listitem_home_items_image, viewGroup, false);
            return new FeedItemViewHolder(v);

        }else{
            View v = inflater.inflate(R.layout.listitem_network_state, viewGroup, false);

            return new NetworkStateViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                if(viewHolder instanceof FeedItemViewHolder) {
//                    itemview



                try {
                    GetFeedResponse.FeedData currentFeed = getItem(i);
                    ((FeedItemViewHolder) viewHolder).bodyText.setText(currentFeed.getFeed_content());
                    ((FeedItemViewHolder) viewHolder).name.setText(currentFeed.getUser_name());
                }catch (NullPointerException e){
                    Log.e(TAG, "onBindViewHolder: null exception");
                }catch (IndexOutOfBoundsException e){
                    Log.e(TAG, "onBindViewHolder: out of bounds" );
                }


                }else{
//                    progress
                }
    }




    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1) {
            return TYPE_PROGRESS;
        } else {
            return TYPE_ITEM;
        }
    }

    public static DiffUtil.ItemCallback<GetFeedResponse.FeedData> DIFF_CALLBACK = new DiffUtil.ItemCallback<GetFeedResponse.FeedData>() {
        @Override
        public boolean areItemsTheSame(@NonNull GetFeedResponse.FeedData oldata, @NonNull GetFeedResponse.FeedData newdata) {
            return oldata.getFeed_id() == newdata.getFeed_id();
        }

        @Override
        public boolean areContentsTheSame(@NonNull GetFeedResponse.FeedData oldata, @NonNull GetFeedResponse.FeedData newdata) {
            return oldata.equals(newdata);
        }
    };


    private boolean hasExtraRow() {
        if (networkState != null && networkState != NetworkState.LOADED) {
            return true;
        } else {
            return false;
        }
    }

    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }

    public class FeedItemViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.circular_image)
        CircleImageView circularImage;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.body_text)
        TextView bodyText;
        @BindView(R.id.body_image)
        ImageView bodyImage;
        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.comment)
        ImageView comment;
        @BindView(R.id.react)
        ImageView react;

        public FeedItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public class NetworkStateViewHolder extends RecyclerView.ViewHolder {
        public NetworkStateViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);



        }
    }
}
