package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.base;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory.adapter.BusinessCategoryAdapter;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.base.adapter.FarmerCategoryData;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.base.adapter.StaggeredRecyclerViewAdapter;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection.CategoryClass;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.query.FarmerQueryFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class FarmerBaseFragment extends Fragment {

    @Inject
    HomeFarmerViewModel viewModel;
    private static final String TAG = "FarmerBaseFragment";

    private ArrayList<FarmerCategoryData> mList;
    RecyclerView recyclerView;

    @Inject
    StaggeredRecyclerViewAdapter adapter;

    @Inject
    FragmentManager fm;

    @Inject
    FarmerQueryFragment farmerQueryFragment;

    String categorySelected;

    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) v.getTag();
            int position = viewHolder.getAdapterPosition();

             categorySelected = mList.get(position).getName();

            Log.d(TAG, "onClick: " + mList.get(position).getName());

            viewModel.setCategorySelected(categorySelected);

            initializeFragments(fm,farmerQueryFragment);
        }
    };

    @Inject
    public FarmerBaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        setUpRecycler(recyclerView, adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_farmer_base, container, false);

        if(viewModel==null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        mList = new ArrayList<>();
        mList.clear();
        recyclerView = view.findViewById(R.id.recycler_view);
        //setUpRecycler(recyclerView, adapter);



        return view;
    }


    @Override
    public void onAttach(Context context) {

        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }
    private ArrayList<FarmerCategoryData> loadItems(){
        mList = new ArrayList<>();
        mList.add(new FarmerCategoryData(R.drawable.fquery_plantbreeding_and_genetics,"Plant Breeding and Genetics"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_agrimarketing_produceselling,"Agrimarketing and Produce Selling"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_animalhusbandry, "Animal Husbandary"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_harvesting_storage, "Harvesting and Storage"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_farmtools__machinery, "Farm tools and Machinery"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_seeds_and_saplings, "Seeds,Seed variety and Saplings"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_nursery_and_greenhousement_management, "Nursery and Green House Management"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_cropfinance_insurance, "Crop Finance & Insurance"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_fertilizer_application, "Fertilizer and their applications"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_disease_insects_pests, "Pesticides"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_sioilnutrition_intercropping, "Soil Nutrition and intercropping"));
        mList.add(new FarmerCategoryData(R.drawable.fquery_others, "Others"));
        return mList;
    }

    private void setUpRecycler(RecyclerView recyclerView, StaggeredRecyclerViewAdapter adapter) {

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(onItemClickListener);
        adapter.updateListItems(loadItems());
    }


    public void initializeFragments(FragmentManager fm, Fragment frag){
        String backStateName = frag.getClass().toString();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_farmer, frag);
        ft.addToBackStack(backStateName);
        ft.commit();

    }

}
