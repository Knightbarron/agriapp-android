package com.androidpopcorn.tenx.freshciboapp.Data.model.Expert;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllExpertsResponse {

    @SerializedName("specialists")
    private ArrayList<ExpertPatchBody> mList;
    public AllExpertsResponse(ArrayList<ExpertPatchBody> mList) {
        this.mList = mList;
    }


    public AllExpertsResponse() {
    }

    public ArrayList<ExpertPatchBody> getmList() {
        return mList;
    }

    public void setmList(ArrayList<ExpertPatchBody> mList) {
        this.mList = mList;
    }
}
