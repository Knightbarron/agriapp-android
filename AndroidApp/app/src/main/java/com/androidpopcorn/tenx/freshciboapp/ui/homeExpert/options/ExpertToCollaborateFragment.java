package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.utils.RoundedCornersTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertToCollaborateFragment extends Fragment {


    @Inject
    HomeExpertViewModel viewModel;

    private static final String TAG = "ExpertToCollaborateFrag";
    @BindView(R.id.ib3)
    ImageButton ib3;
    @BindView(R.id.edit_btn_continue)
    ImageButton collabPage;
    Unbinder unbinder;

    @Inject
    FragmentManager fm;
    @BindView(R.id.image)
    ImageView image;

    public static int sCorner = 50;
    public static int sMargin = 20;

//    @Inject
//    ExpertToAddNewService newService;

    @Inject
    public ExpertToCollaborateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expert_to_collaborate, container, false);


        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIWEMODEL is NUll");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        unbinder = ButterKnife.bind(this, view);
        setUpImage();

        onClickMethods();
        return view;
    }

    public void setUpImage() {
        Glide.with(this).load(R.drawable.get_started_login)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(image);
    }

    private void onClickMethods() {

        ib3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeFragments(fm, new ExpertToAddNewService());
            }
        });

        collabPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO I dont the receiving end
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void initializeFragments(FragmentManager fm, Fragment frag) {
        String backStateName = frag.getClass().toString();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, frag);
        ft.addToBackStack(backStateName);
        ft.commit();
    }
}