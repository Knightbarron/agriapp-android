package com.androidpopcorn.tenx.freshciboapp.di.modules;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;
import com.androidpopcorn.tenx.freshciboapp.Data.prefs.AppPreferencesHelper;
import com.androidpopcorn.tenx.freshciboapp.R;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {

    private static final String TAG = "RetrofitModule";

    private static Retrofit.Builder builder
            = new Retrofit.Builder()
            .baseUrl(ApiService.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

    private static OkHttpClient.Builder httpClient
            = new OkHttpClient.Builder();



    private static HttpLoggingInterceptor logging
            = new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BASIC);





    @Singleton
    @Provides
    public Retrofit provideRetrofit(AppPreferencesHelper appPreferencesHelper){

        //TODO add this
        //String authToken = "Bearer "+appPreferencesHelper.getAccessToken();

        //Token for business
       // String authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoiYnVzaW5lc3MiLCJpZCI6NSwiaWF0IjoxNTYxMjM1MzE3LCJleHAiOjE1OTI3NzEzMTd9.5mtNDwqKHnzHrfuoUyE-9aUEqW0pMx9nJWOA3nY39MY";

        //TOKEN FOR BUSIINESSSS TO USE"
       // String authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoiYnVzaW5lc3MiLCJpZCI6NSwiaWF0IjoxNTYyMDI1MDMxLCJleHAiOjE1OTM1NjEwMzF9.7eKC-ltswJFWQ4GwViayO_0C-vpNow83P03Y_Dj9dQ0";

        //TOKEN FOR FARMER
        //String authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoiYnVzaW5lc3MiLCJpZCI6NSwiaWF0IjoxNTYxNTY4MjEyLCJleHAiOjE1OTMxMDQyMTJ9.WPHFnUmzVtuYBFqd5wkE69MIadDXLqEfeYmPmlcASf4";

        //TOKEN FOR FARMER TO USE
        //String authToken =  "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoiZmFybWVyIiwiaWQiOjQ3LCJpYXQiOjE1NjE3NTEyNzQsImV4cCI6MTU5MzI4NzI3NH0.iPonT5GrzOVY6iu34naafhnY4TTV5h6bsYWA4skfRcQ";


        //TOKEN FOR EXPERT
        //String authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoic3BlY2lhbGlzdCIsImlkIjo1NywiaWF0IjoxNTYyMDA4Njk0LCJleHAiOjE1OTM1NDQ2OTR9.TI3EXXOhy20n4ssizc4tlrXbodDu5bPSLoU3lMUAN8c";


        //String authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoiZmFybWVyIiwiaWQiOjQ3LCJpYXQiOjE1NjI2MDg4NzAsImV4cCI6MTU5NDE0NDg3MH0.Kks25qryBajz4rOpanTSjcFeFIZ8u20cKfgxBVjcTrc";


        //BUSINESS
        //String authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoiYnVzaW5lc3MiLCJpZCI6NSwiaWF0IjoxNTYyOTI4OTc1LCJleHAiOjE1OTQ0NjQ5NzV9.pbv6S3YZibif0BDP10rMm2E2LKm0THWO_x7lkNVTDu0";

        //String authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoic3BlY2lhbGlzdCIsImlkIjo1NywiaWF0IjoxNTYxODM5MTYxLCJleHAiOjE1OTMzNzUxNjF9.cGPWXUFQ_m1hLH-BR7gsKU52VM4XlyJa_QRNHgtI-iA";


        String authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZSI6IjgxMzMwODkyOTEiLCJ0eXBlIjoiZmFybWVyIiwiaWQiOjQ3LCJpYXQiOjE1NjI2MDg4NzAsImV4cCI6MTU5NDE0NDg3MH0.Kks25qryBajz4rOpanTSjcFeFIZ8u20cKfgxBVjcTrc";
        Log.d(TAG, "provideRetrofit: " + authToken);


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Authorization" , authToken)
                        .method(original.method(), original.body()).build();
                return chain.proceed(request);
            }
        });

        if (!httpClient.interceptors().contains(logging)) {
            httpClient.addInterceptor(logging);
           httpClient.connectTimeout(60, TimeUnit.SECONDS);
           httpClient.callTimeout(60, TimeUnit.SECONDS);

            builder.client(httpClient.build());
            retrofit = builder.build();
        }

        return retrofit;
    }


}