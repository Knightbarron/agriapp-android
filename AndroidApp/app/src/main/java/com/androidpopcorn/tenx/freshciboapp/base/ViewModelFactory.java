package com.androidpopcorn.tenx.freshciboapp.base;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;

public class ViewModelFactory<V> implements ViewModelProvider.Factory {

    private V viewModel;
    private AppDataManager appDataManager;
    private Application application;

    public ViewModelFactory(V viewModel, AppDataManager appDataManager, Application application) {
        this.viewModel = viewModel;
        this.appDataManager = appDataManager;
        this.application = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(viewModel.getClass())) {
            return (T) viewModel;
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }

}
