package com.androidpopcorn.tenx.freshciboapp.Data.model.feed;

import com.google.gson.annotations.SerializedName;

public class PostFeedResponse {

    @SerializedName("title")
    public String title;


    @SerializedName("content")
    public String content;

    @SerializedName("category")
    public String category;


    @SerializedName("feed_image_path")
    public String image_path;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
