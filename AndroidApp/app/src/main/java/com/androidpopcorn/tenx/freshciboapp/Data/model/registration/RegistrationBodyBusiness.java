package com.androidpopcorn.tenx.freshciboapp.Data.model.registration;

import com.google.gson.annotations.SerializedName;

public class RegistrationBodyBusiness {

    @SerializedName("business_name")
    private String name;


    @SerializedName("business_password")
    private String password;


    @SerializedName("business_phone")
    private String phone;

    public RegistrationBodyBusiness(String name, String password, String phone) {

        this.password = password;
        this.name = name;
        this.phone = phone;
    }

    public RegistrationBodyBusiness() {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
