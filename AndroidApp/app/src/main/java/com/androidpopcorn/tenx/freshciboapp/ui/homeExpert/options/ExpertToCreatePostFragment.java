package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.expertCreatePost.ExpertCreatePostFragment;
import com.androidpopcorn.tenx.freshciboapp.utils.RoundedCornersTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertToCreatePostFragment extends Fragment {

    @Inject
    HomeExpertViewModel viewModel;
    private static final String TAG = "ExpertToCreatePostFragm";
    @BindView(R.id.ib)
    ImageButton ib;
    @BindView(R.id.edit_btn_continue)
    ImageButton nextPage;
    Unbinder unbinder;

    @Inject
    FragmentManager fm;

    public static int sCorner = 50;
    public static int sMargin = 20;
    @BindView(R.id.image)
    ImageView image;


    @Inject
    public ExpertToCreatePostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View v = inflater.inflate(R.layout.fragment_expert_to_create_post, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIWEMODEL is NUll");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");

        unbinder = ButterKnife.bind(this, v);

        setUpImage();

        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeFragments(fm, new ExpertCreatePostFragment());
            }
        });


        return v;
    }

    public void setUpImage() {
        Glide.with(this).load(R.drawable.continue_as_farmer)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(image);
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.ib)
    public void onViewClicked() {
        initializeFragments(fm, new ExpertToAddNewService());

    }

    public void initializeFragments(FragmentManager fm, Fragment frag) {
        String backStateName = frag.getClass().toString();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, frag);
        ft.addToBackStack(backStateName);
        ft.commit();
    }


}