package com.androidpopcorn.tenx.freshciboapp.Data.model;

import com.google.gson.annotations.SerializedName;

 public class CategoryBody {

    @SerializedName("name")
    private String name;

    public CategoryBody(String name) {
        this.name = name;
    }

    public CategoryBody() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
