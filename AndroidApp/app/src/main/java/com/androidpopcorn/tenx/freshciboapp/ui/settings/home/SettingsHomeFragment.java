package com.androidpopcorn.tenx.freshciboapp.ui.settings.home;


import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile.SettingsBusinessEditProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile.SettingsExpertEditProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile.SettingsFarmerEditProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.settings.SettingsFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsHomeFragment extends Fragment {


    @Inject
    SettingsViewModel viewModel;

    @Inject
    SettingsFarmerEditProfileFragment settingsFarmerEditProfileFragment;

    @Inject
    SettingsFragment settingsFragment;

    @Inject
    SettingsBusinessEditProfileFragment businessEditProfileFragment;

    @Inject
    SettingsExpertEditProfileFragment expertEditProfileFragment;

    @Inject
    FragmentManager fm;
    private static final String TAG = "SettingsHomeFragment";
    @BindView(R.id.settings)
    TextView settings;
    @BindView(R.id.share)
    TextView share;
    @BindView(R.id.socialize)
    TextView socialize;
    @BindView(R.id.help)
    TextView help;
    @BindView(R.id.privacy_policy)
    TextView privacyPolicy;
    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.log_out)
    TextView logOut;
    @BindView(R.id.ic_back_arrow)
    ImageView backArrow;

    Unbinder unbinder;
    String userTypeFromVM = null;


    @Inject
    public SettingsHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings_home, container, false);
        unbinder =  ButterKnife.bind(this, view);


        observeForUserType();
        navBack();

        return view;
    }

    public void navBack(){
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if (userTypeFromVM.equals("Farmer")){
                  Intent intent = new Intent(getActivity(),HomeFarmerActivity.class);
                  startActivity(intent);

              }else if(userTypeFromVM.equals("Expert")){
                  Intent intent = new Intent(getActivity(), HomeExpertActivity.class);
                  startActivity(intent);

              }else if(userTypeFromVM.equals("Business")){
                  Intent intent = new Intent(getActivity(), HomeBusinessActivity.class);
                  startActivity(intent);

              }else{
                  Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
              }

            }
        });
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    public void initializeFragments(FragmentManager fm, Fragment frag) {
        Log.d(TAG, "initializeFragments: Transaction started");
        String backStateName = frag.getClass().toString();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, frag);
        ft.addToBackStack(backStateName);
        ft.commit();
    }

    private void observeForUserType() {
        viewModel.getUserType().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                userTypeFromVM = s;
            }
        });
    }


    @OnClick({R.id.settings, R.id.share, R.id.socialize, R.id.help, R.id.privacy_policy, R.id.terms, R.id.log_out, R.id.container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.settings:
                if (userTypeFromVM.equals("Farmer")){
                    initializeFragments(fm, settingsFarmerEditProfileFragment);
                }else if(userTypeFromVM.equals("Expert")){
                    initializeFragments(fm, expertEditProfileFragment);
                }else if(userTypeFromVM.equals("Business")){
                    initializeFragments(fm, businessEditProfileFragment);
                }else{
                    Toast.makeText(getActivity(), "Server Error....", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.share:

                break;
            case R.id.socialize:

                break;
            case R.id.help:
                break;
            case R.id.privacy_policy:
                break;
            case R.id.terms:
                break;
            case R.id.log_out:
                break;
        }
    }

}
