package com.androidpopcorn.tenx.freshciboapp.ui.settings;


import com.androidpopcorn.tenx.freshciboapp.ui.settings.contact.ContactUsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile.SettingsBusinessEditProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile.SettingsExpertEditProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile.SettingsFarmerEditProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.home.SettingsHomeFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.settings.SettingsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SettingsFragmentBuilder {


    @ContributesAndroidInjector
    public abstract SettingsFragment bindSettingsFragment();

    @ContributesAndroidInjector
    public abstract SettingsFarmerEditProfileFragment bindSettingsEditProfileFragment();

    @ContributesAndroidInjector
    public abstract SettingsHomeFragment bindSettingsHomeFragment();

    @ContributesAndroidInjector
    public abstract SettingsExpertEditProfileFragment bindSettingsExpertEditProfileFragment();

    @ContributesAndroidInjector
    public abstract SettingsBusinessEditProfileFragment bindSettingsBusinessEditPRofileFragment();

    @ContributesAndroidInjector
    public abstract ContactUsFragment bindContactUsFragment();

}
