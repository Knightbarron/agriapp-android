package com.androidpopcorn.tenx.freshciboapp.ui.main;


import com.androidpopcorn.tenx.freshciboapp.di.scope.FragmentScope;
import com.androidpopcorn.tenx.freshciboapp.ui.main.GetStarted.GetStartedFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.SelectionExtra.SelectionExtraFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.login.LoginFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.login.LoginModule;
import com.androidpopcorn.tenx.freshciboapp.ui.main.otp.OtpFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.phone.PhonenoFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.regis.RegisFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.selection.SelectionFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.welcome.WelcomeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBuilder {


    @ContributesAndroidInjector(modules = {LoginModule.class})
    @FragmentScope
    abstract LoginFragment bindLoginFragment();


    @ContributesAndroidInjector(modules = {})
    @FragmentScope
    abstract RegisFragment bindRegistrationFragment();

    @ContributesAndroidInjector(modules = {})
    @FragmentScope
    abstract GetStartedFragment bindGetStartedFragment();

    @ContributesAndroidInjector(modules = {})
    @FragmentScope
    abstract OtpFragment bindOtpFragment();

    @ContributesAndroidInjector(modules = {})
    @FragmentScope
    abstract PhonenoFragment bindPhonenoFragment();

    @ContributesAndroidInjector(modules = {})
    @FragmentScope
    abstract SelectionFragment bindSelectionFragment();

    @ContributesAndroidInjector(modules = {})
    @FragmentScope
    abstract WelcomeFragment bindWelcomeFragment();

    @ContributesAndroidInjector(modules = {})
    @FragmentScope
    abstract SelectionExtraFragment SelecExtraFragment();



}
