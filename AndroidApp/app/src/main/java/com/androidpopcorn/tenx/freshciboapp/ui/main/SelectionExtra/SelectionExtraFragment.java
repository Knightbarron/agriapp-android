package com.androidpopcorn.tenx.freshciboapp.ui.main.SelectionExtra;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.main.login.LoginFragment;
import com.androidpopcorn.tenx.freshciboapp.utils.RoundedCornersTransformation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectionExtraFragment extends Fragment {

    private static final String TAG = "SelectionExtraFragment";


    @Inject
    MainViewModel viewModel;

    @BindView(R.id.as_a_farmer)
    TextView asAFarmer;
    @BindView(R.id.as_an_expert)
    TextView asAnExpert;
    @BindView(R.id.as_an_business)
    TextView asAnBusiness;
    Unbinder unbinder;

    @Inject
    LoginFragment loginFragment;

    public static int sCorner = 50;
    public static int sMargin = 20;
    @BindView(R.id.farmer_image)
    ImageView farmerImage;
    @BindView(R.id.expert_image)
    ImageView expertImage;
    @BindView(R.id.business_image)
    ImageView businessImage;

    @Inject
    public SelectionExtraFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_selection_extra, container, false);


//        test
        if (viewModel == null) {
            Log.d(TAG, "onCreateView: VIEW MODEL IS NULL");
        } else {
            Log.d(TAG, "onCreateView: VIEW MODEL IS NOT NULL");
        }

        unbinder = ButterKnife.bind(this, v);


        setUpImage();


        return v;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.as_a_farmer, R.id.as_an_expert, R.id.as_an_business})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.as_a_farmer:
                initializeFragments("farmer");
                break;
            case R.id.as_an_expert:
                initializeFragments("expert");
                break;
            case R.id.as_an_business:
                initializeFragments("business");
                break;
        }
    }


    public void setUpImage() {
        Glide.with(this).load(R.drawable.continue_as_farmer)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(farmerImage);
        Glide.with(this).load(R.drawable.continue_as_expert)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(expertImage);
        Glide.with(this).load(R.drawable.continue_as_agri_business)
                .apply(RequestOptions.bitmapTransform(
                        new RoundedCornersTransformation(getActivity(), sCorner, sMargin))).into(businessImage);
    }

    public void initializeFragments(String userType) {
        viewModel.saveUserType(userType);
        String backStateName = loginFragment.getClass().toString();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);

        transaction.replace(R.id.frame_main, loginFragment);
        transaction.addToBackStack(backStateName);

        transaction.commit();
    }
}
