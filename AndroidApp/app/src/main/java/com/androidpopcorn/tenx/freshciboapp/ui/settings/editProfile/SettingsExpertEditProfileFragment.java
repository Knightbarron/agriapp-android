package com.androidpopcorn.tenx.freshciboapp.ui.settings.editProfile;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsExpertEditProfileFragment extends Fragment {

    @Inject
    SettingsViewModel viewModel;

    private static final String TAG = "SettingsExpertEditProfi";
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_location)
    EditText etOrganisation;
    @BindView(R.id.specialization_tv)
    TextView specializationTv;
    @BindView(R.id.spinner_specialization)
    Spinner spinnerSpecialization;
    @BindView(R.id.additional_specialization_tv)
    TextView additionalSpecializationTv;
    @BindView(R.id.spinner_specialization_additional)
    Spinner spinnerSpecializationAdditional;
    @BindView(R.id.qualification_tv)
    TextView qualificationTv;
    @BindView(R.id.spinner_qualification)
    Spinner spinnerQualification;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;
    Unbinder unbinder;

    String specialization;
    String qualification;
    String additionalSpecialization;

    @Inject
    public SettingsExpertEditProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings_expert_edit_profile, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        unbinder = ButterKnife.bind(this, view);

        initSpinners();

        observerForExpertPatchProfile();



        return view;
    }

    private void observerForExpertPatchProfile() {
        viewModel.getStatusPatchExpertProfile().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean){
                    Toast.makeText(getActivity(), "The profile has been updated", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getActivity(), "Unsuccessful updating the profile", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.edit_btn_continue)
    public void onViewClicked() {
        String name = etName.getText().toString();
        String address = etAddress.getText().toString();
        String email = etEmail.getText().toString();
        String organisation = etOrganisation.getText().toString();
        String specialization = specializationTv.getText().toString();
        String specialization2 = additionalSpecializationTv.getText().toString();
        String qualification = qualificationTv.getText().toString();


        viewModel.patchExpertProfile(new ExpertPatchBody(name,email,address,organisation,specialization,specialization2,qualification));
    }


    private void initSpinners() {

        String[] dataSpecialization = new String[]{"","AgriEngineering", "AgroEconomics", "Agronomy",
                "Bio Chemistry", "Biotechnology", "Ecology", "Entomology", "Extension Education", "Food Science & technology",
                "Forage Sciences", "Meterology", "Ornamental horticulture", "Other", "Pathology", "Agri business management",
                "Agroforestry", "Animal husbandry", "Bioinformatics", "Enviormental Studies", "Floriculture",
                "Landscaping", "Microbiology", "Nursery & green house technology", "Olericulture", "Physiology",
                "Plant breeding", "Plant Genetics", "Plantation crops,spices and aromatic", "Pomology",
                "Post harvest management", "Rural banking & finance management", "Silviculture", "Soil Science",
                "Statistics", "Weed Science"};


        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataSpecialization);
        spinnerSpecialization.setAdapter(adapter);

        specialization = spinnerSpecialization.getSelectedItem().toString();

        spinnerSpecialization.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                specialization = spinnerSpecialization.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Specialization : " + specialization);
                specializationTv.setText(specialization);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerSpecializationAdditional.setAdapter(adapter);
        spinnerSpecializationAdditional.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                additionalSpecialization = spinnerSpecializationAdditional.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Specialization : " + additionalSpecialization);
                additionalSpecializationTv.setText(additionalSpecialization);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        String[] dataQualification = new String[]{"","Diploma", "Undergraduate/graduate", "Post graduate",
                "Doctorate", "Post Doctorate"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataQualification);
        spinnerQualification.setAdapter(adapter1);

        spinnerQualification.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                qualification = spinnerQualification.getSelectedItem().toString();

                Log.d(TAG, "initSpinners: Qualification : " + qualification);

                qualificationTv.setText(qualification);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
}
