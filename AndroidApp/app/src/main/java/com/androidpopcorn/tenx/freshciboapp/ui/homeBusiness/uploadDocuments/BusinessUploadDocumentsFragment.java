package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.uploadDocuments;


import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.base.BusinessBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory.BusinessCategoryFragment;
import com.androidpopcorn.tenx.freshciboapp.utils.AppConstants;
import com.androidpopcorn.tenx.freshciboapp.utils.FileUtil;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessUploadDocumentsFragment extends Fragment {


    @Inject
    HomeBusinessViewModel viewModel;

    private static final String TAG = "BusinessUploadDocuments";

    public static final int REQUEST_TRADE_PDF_KEY = 123;
    public static final int REQUEST_CERTIFICATION_PDF_KEY = 225;
    @BindView(R.id.upload_company_id)
    ImageView uploadCompanyId;
    @BindView(R.id.upload_certificates)
    ImageView uploadCertificates;
    @BindView(R.id.edit_btn_continue)
    Button editBtnContinue;
    @BindView(R.id.tv_error_message)
    TextView tvErrorMessage;
    @BindView(R.id.tv_skip)
    TextView tvSkip;
    Unbinder unbinder;

    private Uri uploadLicense;
    private Uri uploadCertificate;

    @Inject
    BusinessCategoryFragment businessCategoryFragment;


    @Inject
    public BusinessUploadDocumentsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_upload_documents, container, false);


        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");

        unbinder = ButterKnife.bind(this, view);

        subscribeObserver();
        return view;
    }

    private void subscribeObserver() {

        //TODO add animations
        viewModel.getStatusDocumentUpload().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean){
                    //TODO go to next fragment
                    Toast.makeText(getActivity(), "Documents upload successfull", Toast.LENGTH_SHORT).show();
                    goToBase();
                }else{
                    Toast.makeText(getActivity(), "Upload failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    //TODO test
    public void goToBase() {
        String backStateName = businessCategoryFragment.getClass().toString();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(R.id.container, businessCategoryFragment);
        transaction.addToBackStack(backStateName);

        transaction.commit();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.upload_company_id, R.id.upload_certificates, R.id.edit_btn_continue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.upload_company_id:
                searchPdf(REQUEST_TRADE_PDF_KEY);
                break;
            case R.id.upload_certificates:
                searchPdf(REQUEST_CERTIFICATION_PDF_KEY);
                break;
            case R.id.edit_btn_continue:
                attemptUpload();
                break;
        }
    }

    private void attemptUpload() {

        if(uploadLicense==null && uploadCertificate == null){
            Log.d(TAG, "attemptUpload: Atleast choose one option");
        }else if(uploadLicense==null){
            Log.d(TAG, "attemptUpload: License is NULL");
        }else if(uploadCertificate==null){
            Log.d(TAG, "attemptUpload: Certificate is NULL");
        }

        Dexter.withActivity(getActivity()).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {

            //TODO use viewModel
                if (uploadLicense != null)
                    viewModel.businessDocumentsUpload(getFile(uploadLicense), AppConstants.DOCTYPE_BUSINESS_LICENSE);
                if (uploadCertificate != null)
                    viewModel.businessDocumentsUpload(getFile(uploadCertificate),AppConstants.DOCTYPE_BUSINESS_CERTIFICATION);

            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(getActivity(), "Get Permission First", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }).check();



    }

    private void searchPdf(int requestTradePdfKey) {
        Intent intent= new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select a PDF"),requestTradePdfKey);
    }


    public File getFile(Uri uri){
        try {
            File file = FileUtil.from(getActivity(),uri);
            Log.d("file", "File...:::: uti - "+file .getPath()+" file -" + file + " : " + file .exists());

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {

            Uri uri = data.getData();

            File file = new File(uri.getPath());
            Log.d(TAG, "onActivityResult: abs path "+file.getParent());


            if (requestCode == REQUEST_CERTIFICATION_PDF_KEY) {
                uploadCertificate = data.getData();

            } else if (requestCode == REQUEST_TRADE_PDF_KEY) {
                uploadLicense = data.getData();
            } else {
                Log.d(TAG, "onActivityResult: No matching request code");
            }
        }
    }


}
