package com.androidpopcorn.tenx.freshciboapp.Data.model.feed;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetFeedResponse {

    @SerializedName("feeds")
    private ArrayList<FeedData> feeds;

    public ArrayList<FeedData> getFeeds() {
        return feeds;
    }

    public void setFeeds(ArrayList<FeedData> feeds) {
        this.feeds = feeds;
    }

    public class FeedData {

        @SerializedName("feed_id")
        private float feed_id;
        @SerializedName("feed_title")
        private String feed_title;
        @SerializedName("feed_content")
        private String feed_content;
        @SerializedName("feed_image_path")
        private String feed_image_path = null;
        @SerializedName("feed_category")
        private String feed_category = null;
        @SerializedName("feed_date_created")
        private String feed_date_created;
        @SerializedName("feed_date_modified")
        private String feed_date_modified;
        @SerializedName("user_name")
        private String user_name;
        @SerializedName("user_image")
        private String user_image;
        @SerializedName("user_type")
        private String user_type;
        @SerializedName("user_id")
        private float user_id;

        public float getFeed_id() {
            return feed_id;
        }

        public void setFeed_id(float feed_id) {
            this.feed_id = feed_id;
        }

        public String getFeed_title() {
            return feed_title;
        }

        public void setFeed_title(String feed_title) {
            this.feed_title = feed_title;
        }

        public String getFeed_content() {
            return feed_content;
        }

        public void setFeed_content(String feed_content) {
            this.feed_content = feed_content;
        }

        public String getFeed_image_path() {
            return feed_image_path;
        }

        public void setFeed_image_path(String feed_image_path) {
            this.feed_image_path = feed_image_path;
        }

        public String getFeed_category() {
            return feed_category;
        }

        public void setFeed_category(String feed_category) {
            this.feed_category = feed_category;
        }

        public String getFeed_date_created() {
            return feed_date_created;
        }

        public void setFeed_date_created(String feed_date_created) {
            this.feed_date_created = feed_date_created;
        }

        public String getFeed_date_modified() {
            return feed_date_modified;
        }

        public void setFeed_date_modified(String feed_date_modified) {
            this.feed_date_modified = feed_date_modified;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_image() {
            return user_image;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public String getUser_type() {
            return user_type;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public float getUser_id() {
            return user_id;
        }

        public void setUser_id(float user_id) {
            this.user_id = user_id;
        }
    }

}
