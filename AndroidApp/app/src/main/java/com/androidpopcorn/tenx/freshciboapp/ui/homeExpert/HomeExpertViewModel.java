package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.util.Log;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.Data.model.DefaultResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageBody;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPackageResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertPatchBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.Expert.ExpertProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.base.BaseViewModel;

import java.io.File;


import com.androidpopcorn.tenx.freshciboapp.Data.model.ExpertPatchResponse;

import com.androidpopcorn.tenx.freshciboapp.Data.model.NotificationResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.PostFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetCommentResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.query.GetQueryResponse;
import com.androidpopcorn.tenx.freshciboapp.base.BaseViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.paging.FeedDataFactory;
import com.androidpopcorn.tenx.freshciboapp.utils.NetworkState;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class HomeExpertViewModel extends BaseViewModel {

    private static final String TAG = "HomeExpertViewModel";
    AppDataManager appDataManager;
    HomeExpertCallback callback;



//    -------------------PAGING --------

    private Executor executor;
    private LiveData<NetworkState> networkState;
    private LiveData<PagedList<GetFeedResponse.FeedData>> feedlivedata;


//    -------------------

    //LiveData for farmer Package Details
    private MutableLiveData<ExpertPackageResponse.ExpertPackage> singlePackageObserver;

    public LiveData<ExpertPackageResponse.ExpertPackage> getSinglePackageObserver(){
        if(singlePackageObserver ==null)
            singlePackageObserver = new MutableLiveData<>();
        return singlePackageObserver;
    }


//    notificaion observer

    private MutableLiveData<NotificationResponse> onNotificationGetObs;


//    expertprofilepatch observer

   private MutableLiveData<ExpertPatchResponse> onExpertProfilePatchObs;

//    create new post || new post observer

    MutableLiveData<PostFeedResponse> onNewPostObs;

//    on get all feeds

    MutableLiveData<GetFeedResponse> onGetAllFeeds;

//    queries observer

    MutableLiveData<List<GetQueryResponse.QueryItem>> onGetSpecialistQueries;


    public HomeExpertViewModel(AppDataManager appDataManager, Application application, HomeExpertCallback callback) {
        super(appDataManager, application);
        this.callback = callback;
        this.appDataManager = appDataManager;
        init();
        
    }


    public void updateExpertProfile(ExpertPatchBody data){
        appDataManager.updateExpertProfile(data).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<ExpertPatchResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<ExpertPatchResponse> defaultResponseResponse) {
                    if(defaultResponseResponse.code() == 200){
//                        successfully updated
                            setOnExpertProfilePatchObs(defaultResponseResponse.body());
                    }else {
//                        some error occured
                        Log.d(TAG, "onNext: Error fetching expert data");

                    }
            }

            @Override
            public void onError(Throwable e) {
//                error updating data notify user
                Log.d(TAG, "onNext: Error fetching expert data");
            }

            @Override
            public void onComplete() {
//completed updating data

            }
        });
    }

    public void uploadExpertDocuments(File file , String doctype){

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("application/pdf"), file);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("doc", file.getName(), fileReqBody);
        //Create request body with text description and text media type

        appDataManager.uploadDocuments(part, fileReqBody, doctype).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {
                if(defaultResponseResponse.code() == 200){
                    Log.d(TAG, "onNext: " + doctype );
                    Log.d(TAG, "onNext: " + defaultResponseResponse.code());
//                        successfully updated
                    callback.onUploadSuccess();
                }else {
//                        some error occured
                    Log.d(TAG, "onNext: Failure");
                    Log.d(TAG, "onNext: " + defaultResponseResponse.code());
                    callback.onUploadFailure();

                }
            }

            @Override
            public void onError(Throwable e) {
//                error updating data notify user
                callback.onUploadFailure();
            }

            @Override
            public void onComplete() {
//completed updating data

            }
        });
    }


    //Saves Farmer PAckage details
    public void saveExpertPackageDetails(ExpertPackageBody body) {
        appDataManager.saveExpertPackage(body).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ExpertPackageResponse.ExpertPackage>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<ExpertPackageResponse.ExpertPackage> defaultResponseResponse) {

                        if (singlePackageObserver == null) {
                            singlePackageObserver = new MutableLiveData<>();
                        }

                        if (defaultResponseResponse.code() == 201 || defaultResponseResponse.code() == 200) {
                            try {


                                singlePackageObserver.setValue(defaultResponseResponse.body());
                                Log.d(TAG, "onNext: Success in storing the package");

                            } catch (NullPointerException e) {

                                Log.d(TAG, "onNext: ");
                            }

                        } else {

                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: ", e);
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Save farmer package details");
                    }
                });
    }

    public void uploadProfilePicture(File file){
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("profile_image", file.getName(), fileReqBody);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "This is a profile image");

        //Create request body with text description and text media type
        appDataManager.uploadProfilePicture(part, name).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<DefaultResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<DefaultResponse> defaultResponseResponse) {
                    if(defaultResponseResponse.code() == 200){
                        Log.d(TAG, "onNext: PROFILE UPLOAD SUCCESS");
                        callback.onProfilePictureUpdateSuccess(defaultResponseResponse.body());
                    }
                    else {
                        Log.d(TAG, "onNext: ERROR");
                        callback.onProfilePictureUpdateFailure();
                    }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: ERROR");
                callback.onProfilePictureUpdateFailure();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void getCurrentExpertProfile(){
        appDataManager.getCurrentExpertProfile().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<ExpertProfileResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<ExpertProfileResponse> expertProfileResponseResponse) {
//            update ui if ok
                Log.d(TAG, "onNext: of get Curretn Expert" + expertProfileResponseResponse.code());
                    if(expertProfileResponseResponse.code() == 200){
                        Log.d(TAG, "onNext: "+expertProfileResponseResponse.body().toString());
                        callback.onFetchCurrentExpertSuccess(expertProfileResponseResponse.body());


                    }else {
//                        show error
                        callback.onFetchCurrentExpertFailure();
                    }

            }

            @Override
            public void onError(Throwable e) {
//                show error
                callback.onFetchCurrentExpertFailure();

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void getCurrentExpertPackages(){
        appDataManager.getCurrentExpertPackage().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<ExpertPackageResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<ExpertPackageResponse> packageresponse) {
//            update ui if ok
                if(packageresponse.code() == 200){
                    Log.d(TAG, "onNext: "+packageresponse.body().toString());
                    callback.onFetchCurrentExpertPackagesSuccess(packageresponse.body());
                }else {
//                        show error
                    callback.onFetchCurrentExpertPackagesFailure();
                }

            }

            @Override
            public void onError(Throwable e) {
//                show error
                callback.onFetchCurrentExpertPackagesFailure();

            }

            @Override
            public void onComplete() {

            }
        });
    }



//    load notifications


    public void loadNotifications(){
        appDataManager.getNotificationsForUser().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<NotificationResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<NotificationResponse> notificationResponseResponse) {
                if(notificationResponseResponse.code() == 200){
                    Log.d(TAG, "onNext: Notifications recieved");
                    setOnNotificationGetObs(notificationResponseResponse.body());
                }else {
                    Log.d(TAG, "onNext: Notifications not available");
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: Notifications error");
            }

            @Override
            public void onComplete() {

            }
        });
    }


//    notification observer

    public MutableLiveData<NotificationResponse> getOnNotificationGetObs() {
        if(onNotificationGetObs == null){
            onNotificationGetObs = new MutableLiveData<>();
        }
        return onNotificationGetObs;
    }

    public void setOnNotificationGetObs(NotificationResponse notification) {
        if(onNotificationGetObs == null){
            onNotificationGetObs = new MutableLiveData<>();
        }
        onNotificationGetObs.setValue(notification);
    }

    public MutableLiveData<ExpertPatchResponse> getOnExpertProfilePatchObs() {
        if(onExpertProfilePatchObs == null){
            onExpertProfilePatchObs = new MutableLiveData<>();
        }
        return onExpertProfilePatchObs;
    }

    public void setOnExpertProfilePatchObs(ExpertPatchResponse data) {
        if(onExpertProfilePatchObs == null){
            onExpertProfilePatchObs = new MutableLiveData<>();
        }
        onExpertProfilePatchObs.setValue(data);
    }



//    create post


    public void createPost(String content,String category, File file){
        MultipartBody.Part part = null;
        if(file != null){
           RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
           // Create MultipartBody.Part using file request-body,file name and part name
          part = MultipartBody.Part.createFormData("feed_image", file.getName(), fileReqBody);

       }
        RequestBody contentBody = RequestBody.create(MediaType.parse("text/plain"), content);
        RequestBody titleBody = RequestBody.create(MediaType.parse("text/plain"), "");
        RequestBody categoryBody = RequestBody.create(MediaType.parse("text/plain"), category);

        //Create request body with text description and text media type
        appDataManager.postCreatePost(part,  titleBody, contentBody, categoryBody).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Observer<Response<PostFeedResponse>>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(Response<PostFeedResponse> data) {
                if(data.code() == 201 || data.code() == 200){
                    Log.d(TAG, "onNext: created new post");
                    setOnNewPostObs(data.body());
                }
                else {
                    Log.d(TAG, "onNext: error creating new post");

                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: ERROR");

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public MutableLiveData<PostFeedResponse> getOnNewPostObs() {
        if(onNewPostObs == null){
            onNewPostObs = new MutableLiveData<>();
        }

        return onNewPostObs;
    }

    public void setOnNewPostObs(PostFeedResponse data) {
        if(onNewPostObs == null){
            onNewPostObs = new MutableLiveData<>();
        }
        onNewPostObs.setValue(data);
    }

    public void getAllFeedsPosts(){
        appDataManager.getGetAllFeeds().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Observer<Response<GetFeedResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        getCompositeDisposable().add(d);
                    }

                    @Override
                    public void onNext(Response<GetFeedResponse> data) {
                        if(data.code() == 201 || data.code() == 200){
                            Log.d(TAG, "return all feeds");
                            setOnGetAllFeeds(data.body());


                        }
                        else {
                            Log.d(TAG, "onNext: error fetching feeds");

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: ERROR");

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public MutableLiveData<GetFeedResponse> getOnGetAllFeeds() {
        if(onGetAllFeeds == null){
            onGetAllFeeds = new MutableLiveData<>();
        }
        return onGetAllFeeds;
    }

    public List<GetQueryResponse.QueryItem> listQuery;




    public void setOnGetAllFeeds(GetFeedResponse data) {
        if(onGetAllFeeds == null){
            onGetAllFeeds = new MutableLiveData<>();
        }
        onGetAllFeeds.setValue(data);
    }



    public void loadQueriesForSpecialists(){

        if(listQuery != null){
            listQuery.clear();
        }
        appDataManager.getQueriesForSpecialist().subscribeOn(Schedulers.io())
                .flatMap((Function<Response<GetQueryResponse>, ObservableSource<GetQueryResponse.QueryItem>>) getQueryResponseResponse -> Observable.fromIterable(getQueryResponseResponse.body().getQueries()))
                .flatMap((Function<GetQueryResponse.QueryItem, ObservableSource<GetQueryResponse.QueryItem>>) queryItem -> {
                   Observable<GetQueryResponse.QueryItem> queryObs = Observable.create(emitter -> {
                       emitter.onNext(queryItem);
                   });

                   Observable<Response<GetCommentResponse>> commentObs = appDataManager.getCommentsForConv(String.valueOf(queryItem.getConversationID()));

                   Observable<GetQueryResponse.QueryItem> out = Observable.zip(queryObs, commentObs, (queryRes, commentRes) -> {
                       GetQueryResponse.QueryItem temp = queryRes;
                       try {
                           Log.d(TAG, "loadQueriesForSpecialists: comments found " + commentRes.body().getComments().size());
                           temp.setComments(commentRes.body().getComments());
                       }catch (NullPointerException e){
                           Log.d(TAG, "loadQueriesForSpecialists: no comments");
                       }

                       return temp;
                   });

                return out;

                }).observeOn(AndroidSchedulers.mainThread()).unsubscribeOn(Schedulers.io()).subscribe(new Observer<GetQueryResponse.QueryItem>() {
            @Override
            public void onSubscribe(Disposable d) {
                getCompositeDisposable().add(d);
            }

            @Override
            public void onNext(GetQueryResponse.QueryItem query) {
                Log.d(TAG, "onNext: "+query.getQuery_question());
                if(listQuery == null){
                    listQuery = new ArrayList<>();
                }

                listQuery.add(query);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete: completed");
                if(listQuery == null){
                    listQuery = new ArrayList<>();
                }
                setOnGetSpecialistQueries(listQuery);
            }
        });

    }

    public MutableLiveData<List<GetQueryResponse.QueryItem>> getOnGetSpecialistQueries() {
       if(onGetSpecialistQueries == null){
           onGetSpecialistQueries = new MutableLiveData<>();
       }
       return onGetSpecialistQueries;
    }

    private void setOnGetSpecialistQueries(List<GetQueryResponse.QueryItem> data) {
        if(onGetSpecialistQueries == null){
            onGetSpecialistQueries = new MutableLiveData<>();
        }

        onGetSpecialistQueries.setValue(data);
    }



//    paging initialization

    private void init() {
        executor = Executors.newFixedThreadPool(5);

        FeedDataFactory feedDataFactory = new FeedDataFactory(appDataManager);
        networkState = Transformations.switchMap(feedDataFactory.getFeedDataSourceMutableLiveData(),
                dataSource ->  dataSource.getNetworkState());

        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(5)
                        .setPageSize(5).build();

        feedlivedata = (new LivePagedListBuilder(feedDataFactory, pagedListConfig))
                .setFetchExecutor(executor)
                .build();
    }


    public LiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    public LiveData<PagedList<GetFeedResponse.FeedData>> getFeedlivedata() {
        return feedlivedata;
    }
}
