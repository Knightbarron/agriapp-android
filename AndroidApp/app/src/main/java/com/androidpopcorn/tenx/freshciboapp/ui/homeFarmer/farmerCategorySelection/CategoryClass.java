package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection;

public class CategoryClass {

    private Integer image;
    private String data;
    private boolean isSelected;

    public CategoryClass(int image, String data) {
        this.image = image;
        this.data = data;
        isSelected = false;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
