package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.uploadDocuments;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainActivity;
import com.androidpopcorn.tenx.freshciboapp.utils.AppConstants;
import com.androidpopcorn.tenx.freshciboapp.utils.FileUtil;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertUploadDocuments extends Fragment {


    private static final String TAG = "ExpertUploadDocuments";

    //    pdf find key
    public static final int REQUEST_COMPANY_PDF_KEY = 123;
    public static final int REQUEST_CERTIFICATION_PDF_KEY = 225;
    @BindView(R.id.tv_error_message)
    TextView tvErrorMessage;

//    uri store for cert and company

    private Uri companypdfuri;
    private Uri certpdfuri;


    @Inject
    HomeExpertViewModel viewModel;
    Unbinder unbinder;
    private Button uploadcompanyid;
    private Button uploadcertificates;
    private Button editbtncontinue;


    @Inject
    public ExpertUploadDocuments() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expert_upload_documents, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.upload_company_id, R.id.upload_certificates, R.id.edit_btn_continue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.upload_company_id:
                searchPdf(REQUEST_COMPANY_PDF_KEY);
                break;
            case R.id.upload_certificates:
                searchPdf(REQUEST_CERTIFICATION_PDF_KEY);
                break;
            case R.id.edit_btn_continue:
//                check if pdf was selected and try upload
                attemptUpload();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {

            Uri uri = data.getData();

            File file = new File(uri.getPath());
            Log.d(TAG, "onActivityResult: abs path "+file.getParent());


            if (requestCode == REQUEST_CERTIFICATION_PDF_KEY) {
                certpdfuri = data.getData();

            } else if (requestCode == REQUEST_COMPANY_PDF_KEY) {
                companypdfuri = data.getData();
            } else {
                Log.d(TAG, "onActivityResult: No matching request code");
            }
        }
    }

    @OnClick(R.id.tv_skip)
    public void onViewClicked() {
//        go next
    }

    public void attemptUpload() {
//

        if (companypdfuri == null && certpdfuri == null) {
//            show error
            tvErrorMessage.setText(R.string.error_select_doc);

        }

        Dexter.withActivity(getActivity()).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                if(certpdfuri != null){
                    viewModel.uploadExpertDocuments(getFile(certpdfuri), AppConstants.DOCTYPE_SPECIALIST_CERTIFICATION);
                }

                if(uploadcertificates != null){
                    viewModel.uploadExpertDocuments(getFile(companypdfuri), AppConstants.DOCTYPE_SPECIALIST_COMPANY);
                }
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                    tvErrorMessage.setText("Accept storage access permission");
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }).check();

    }
    public void searchPdf(int key) {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a PDF "), key);
    }
    public void showErrorMessage(String message){
        tvErrorMessage.setText(message);
    }


//    test

    public File getFile(Uri uri){
        try {
            File file = FileUtil.from(getActivity(),uri);
            Log.d("file", "File...:::: uti - "+file .getPath()+" file -" + file + " : " + file .exists());

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
    }





}
