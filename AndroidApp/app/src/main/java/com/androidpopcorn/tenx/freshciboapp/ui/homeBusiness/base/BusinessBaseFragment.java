package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.base;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.HomeBusinessViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.options.OptionsViewPagerAdapter;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.HomeExpertViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.options.ViewPagerAdapter;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;


public class BusinessBaseFragment extends Fragment {


    @Inject
    HomeBusinessViewModel viewModel;

    private static final String TAG = "BusinessBaseFragment";


    ViewPager viewPager;

    OptionsViewPagerAdapter adapter;

    @Inject
    public BusinessBaseFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_business_base, container, false);

        if(viewModel==null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");

        adapter = new OptionsViewPagerAdapter(getChildFragmentManager());
        viewPager = view.findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);


        return view;
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }
}
