package com.androidpopcorn.tenx.freshciboapp.utils;

public class AppConstants {

    public static String PREF_FILE_NAME = "default";
    public static String KEY_SESSION_ID = "session_id";
    // for experts -- companyid -- certification id   : comp and cert
    // for business   license and bcert
    public static final String DOCTYPE_SPECIALIST_CERTIFICATION = "cert";
    public static final String DOCTYPE_SPECIALIST_COMPANY = "comp";
    public static final String DOCTYPE_BUSINESS_CERTIFICATION = "bcert";
    public static final String DOCTYPE_BUSINESS_LICENSE = "license";


}
