package com.androidpopcorn.tenx.freshciboapp.Data.model.farmer;

import com.google.gson.annotations.SerializedName;

public class FarmerCompleteProfileBody {

    @SerializedName("farmer_full_name")
    private String fullName;
    @SerializedName("farmer_email")
    private String email;
    @SerializedName("farmer_address")
    private String address;
    @SerializedName("farmer_location")
    private String farmLocation;
    @SerializedName("farmer_nearest_market")
    private String nearestMarketLocation;
    @SerializedName("farmer_land_area")
    private String landArea;
    @SerializedName("farmer_land_owner")
    private String farmLandOwner;
    @SerializedName("farmer_about_me")
    private String farmerAboutMe;
    @SerializedName("farmer_organic_farming")
    private int organicFarm;

    public int getOrganicFarm() {
        return organicFarm;
    }

    public void setOrganicFarm(int organicFarm) {
        this.organicFarm = organicFarm;
    }

    public FarmerCompleteProfileBody( String farmLocation, String nearestMarketLocation, String landArea, String farmLandOwner,  String farmerAboutMe, int organicFarm) {
        this.farmLocation = farmLocation;
        this.nearestMarketLocation = nearestMarketLocation;
        this.landArea = landArea;
        this.farmLandOwner = farmLandOwner;
        this.farmerAboutMe = farmerAboutMe;
        this.organicFarm = organicFarm;
    }

    public FarmerCompleteProfileBody(String farmLocation, String nearestMarketLocation, String landArea, String farmLandOwner, int practiceOrganic) {
        this.farmLocation = farmLocation;
        this.nearestMarketLocation = nearestMarketLocation;
        this.landArea = landArea;
        this.farmLandOwner = farmLandOwner;
        this.organicFarm = practiceOrganic;
    }

    public FarmerCompleteProfileBody(String fullName, String email, String address, String farmLocation,
                                     String nearestMarketLocation, String landArea, String farmLandOwner, int practiceOrganic) {
        this.fullName = fullName;
        this.email = email;
        this.address = address;
        this.farmLocation = farmLocation;
        this.nearestMarketLocation = nearestMarketLocation;
        this.landArea = landArea;
        this.farmLandOwner = farmLandOwner;
        this.organicFarm = practiceOrganic;
    }

    public FarmerCompleteProfileBody(String farmerAboutMe){
        this.farmerAboutMe = farmerAboutMe;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }


    public String getFarmerAboutMe() {
        return farmerAboutMe;
    }

    public void setFarmerAboutMe(String farmerAboutMe) {
        this.farmerAboutMe = farmerAboutMe;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getFarmLocation() {
        return farmLocation;
    }

    public void setFarmLocation(String farmLocation) {
        this.farmLocation = farmLocation;
    }

    public String getNearestMarketLocation() {
        return nearestMarketLocation;
    }

    public void setNearestMarketLocation(String nearestMarketLocation) {
        this.nearestMarketLocation = nearestMarketLocation;
    }

    public String getLandArea() {
        return landArea;
    }

    public void setLandArea(String landArea) {
        this.landArea = landArea;
    }

    public String getFarmLandOwner() {
        return farmLandOwner;
    }

    public void setFarmLandOwner(String farmLandOwner) {
        this.farmLandOwner = farmLandOwner;
    }


    public FarmerCompleteProfileBody() {

    }
}