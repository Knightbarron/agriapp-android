package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.notifications.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.Data.model.NotificationResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExpertNotificationAdapter extends RecyclerView.Adapter<ExpertNotificationAdapter.ExpertNotificationViewHolder> {



    private Context mCtx;
    private List<NotificationResponse.NotificationItem> mList;

    @Inject
    public ExpertNotificationAdapter(@ActivityContext Context mCtx) {
        this.mCtx = mCtx;
        mList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ExpertNotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mCtx).inflate(R.layout.listitem_notifications, viewGroup, false);
        return new ExpertNotificationViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ExpertNotificationViewHolder expertNotificationViewHolder, int i) {

        NotificationResponse.NotificationItem current = mList.get(i);

        expertNotificationViewHolder.name.setText(current.getTitle());
        expertNotificationViewHolder.body.setText(current.getMessage());


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ExpertNotificationViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.tv_body)
        TextView body;

        public ExpertNotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void updateList(List<NotificationResponse.NotificationItem> newlist) {
        mList.clear();
        mList.addAll(newlist);
        notifyDataSetChanged();


    }
}
