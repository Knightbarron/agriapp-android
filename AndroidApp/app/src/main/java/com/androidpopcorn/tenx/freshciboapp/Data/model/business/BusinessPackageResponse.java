package com.androidpopcorn.tenx.freshciboapp.Data.model.business;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BusinessPackageResponse {


    @SerializedName("products")
    private ArrayList<BusinessPackageResponse.BusinessPackageResponseBody> productList;

    public class BusinessPackageResponseBody {

        @SerializedName("business_product_services_type")
        private String type;
        @SerializedName("business_product_services_name")
        private String name;
        @SerializedName("business_product_services_details")
        private String details;
        @SerializedName("business_product_services_collaborate")
        private int collab;
        @SerializedName("business_product_services_price")
        private String price;
        @SerializedName("business_category_business_category_id")
        private int category;


        public int getCategory() {
            return category;
        }

        public void setCategory(int category) {
            this.category = category;
        }


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public int getCollab() {
            return collab;
        }

        public void setCollab(int collab) {
            this.collab = collab;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }


    }

    public ArrayList<BusinessPackageResponseBody> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<BusinessPackageResponseBody> productList) {
        this.productList = productList;
    }
}
