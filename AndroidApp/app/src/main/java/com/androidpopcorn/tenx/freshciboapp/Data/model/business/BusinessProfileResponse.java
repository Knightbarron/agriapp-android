package com.androidpopcorn.tenx.freshciboapp.Data.model.business;

import com.google.gson.annotations.SerializedName;

public class BusinessProfileResponse {


    @SerializedName("business")
    private Business business;

    public BusinessProfileResponse(Business business) {
        this.business = business;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public BusinessProfileResponse() {
    }

    public class Business{

        @SerializedName("business_image_path")
        private String image_path;
        @SerializedName("business_tradelicense_path")
        private String tradeLicense;
        @SerializedName("business_cert_path")
        private String businessCart;
        @SerializedName("business_name")
        private String name;
        @SerializedName("business_organization")
        private String organization;
        @SerializedName("business_address")
        private String address;
        @SerializedName("business_email")
        private String email;
        @SerializedName("business_location")
        private String location ;
        @SerializedName("business_type_1")
        private String business_type_1;
        @SerializedName("business_type_2")
        private String business_type_2;
        @SerializedName("business_role")
        private String business_role;
        @SerializedName("business_about_me")
        private String business_bio;

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getBusiness_type_1() {
            return business_type_1;
        }

        public void setBusiness_type_1(String business_type_1) {
            this.business_type_1 = business_type_1;
        }

        public String getBusiness_type_2() {
            return business_type_2;
        }

        public void setBusiness_type_2(String business_type_2) {
            this.business_type_2 = business_type_2;
        }

        public String getBusiness_role() {
            return business_role;
        }

        public void setBusiness_role(String business_role) {
            this.business_role = business_role;
        }

        public String getBusiness_bio() {
            return business_bio;
        }

        public void setBusiness_bio(String business_bio) {
            this.business_bio = business_bio;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage_path() {
            return image_path;
        }

        public void setImage_path(String image_path) {
            this.image_path = image_path;
        }

        public String getOrganization() {
            return organization;
        }

        public void setOrganization(String organization) {
            this.organization = organization;
        }

        public String getTradeLicense() {
            return tradeLicense;
        }

        public void setTradeLicense(String tradeLicense) {
            this.tradeLicense = tradeLicense;
        }

        public String getBusinessCart() {
            return businessCart;
        }

        public void setBusinessCart(String businessCart) {
            this.businessCart = businessCart;
        }
    }

}
