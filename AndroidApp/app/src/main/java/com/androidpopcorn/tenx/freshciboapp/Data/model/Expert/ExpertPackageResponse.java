package com.androidpopcorn.tenx.freshciboapp.Data.model.Expert;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ExpertPackageResponse {

    @SerializedName("specialist_id")
    private int specialistID;

    @SerializedName("packages")
    private ArrayList<ExpertPackage> packages;

    public int getSpecialistID() {
        return specialistID;
    }

    public void setSpecialistID(int specialistID) {
        this.specialistID = specialistID;
    }

    public ArrayList<ExpertPackage> getPackages() {
        return packages;
    }

    public void setPackages(ArrayList<ExpertPackage> packages) {
        this.packages = packages;
    }

    public class ExpertPackage {

        @SerializedName("package_id")
        private float package_id;

        @SerializedName("package_title")
        private String package_title;

        @SerializedName("package_details")
        private String package_details;

        @SerializedName("package_category")
        private String package_category;

        @SerializedName("package_duration")
        private String package_duration;

        @SerializedName("package_price")
        private String package_price;

        @SerializedName("package_date_created")
        private String package_date_created;

        @SerializedName("package_date_modified")
        private String package_date_modified;

        @SerializedName("specialist_specialist_id")
        private float specialist_specialist_id;



        // Getter Methods

        public float getPackage_id() {
            return package_id;
        }

        public String getPackage_title() {
            return package_title;
        }

        public String getPackage_details() {
            return package_details;
        }

        public String getPackage_category() {
            return package_category;
        }

        public String getPackage_duration() {
            return package_duration;
        }

        public String getPackage_price() {
            return package_price;
        }

        public String getPackage_date_created() {
            return package_date_created;
        }

        public String getPackage_date_modified() {
            return package_date_modified;
        }

        public float getSpecialist_specialist_id() {
            return specialist_specialist_id;
        }

        // Setter Methods

        public void setPackage_id(float package_id) {
            this.package_id = package_id;
        }

        public void setPackage_title(String package_title) {
            this.package_title = package_title;
        }

        public void setPackage_details(String package_details) {
            this.package_details = package_details;
        }

        public void setPackage_category(String package_category) {
            this.package_category = package_category;
        }

        public void setPackage_duration(String package_duration) {
            this.package_duration = package_duration;
        }

        public void setPackage_price(String package_price) {
            this.package_price = package_price;
        }

        public void setPackage_date_created(String package_date_created) {
            this.package_date_created = package_date_created;
        }

        public void setPackage_date_modified(String package_date_modified) {
            this.package_date_modified = package_date_modified;
        }

        public void setSpecialist_specialist_id(float specialist_specialist_id) {
            this.specialist_specialist_id = specialist_specialist_id;
        }
    }
}
