package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory.adapter;

public class BusinessCategory {

    private Integer image;
    private String data;

    public BusinessCategory(Integer image, String data) {
        this.image = image;
        this.data = data;
    }


    public BusinessCategory() {
    }


    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
