package com.androidpopcorn.tenx.freshciboapp.ui.main.otp;


import android.arch.lifecycle.Observer;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainActivity;
import com.androidpopcorn.tenx.freshciboapp.ui.main.MainViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.main.login.LoginFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.phone.PhonenoFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.main.selection.SelectionFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;


/**
 * A simple {@link Fragment} subclass.
 */
public class OtpFragment extends Fragment {


    private static final String TAG = "OtpFragment";

    @Inject
    MainViewModel viewModel;
    @BindView(R.id.btn_verify)
    Button btnVerify;
    Unbinder unbinder;
    @BindView(R.id.btn_resend_otp)
    TextView btnResendOtp;

    @BindView(R.id.pin_entry_simple)
    EditText otpEt;




    @Inject
    public OtpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_otp, container, false);


        if (viewModel == null)
            Log.d(TAG, "onCreateView: ViewMOdel is NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");


        //Observe the livedata Verified Status
        subscribeObservers();

        unbinder = ButterKnife.bind(this, view);






        return view;
    }


    private void subscribeObservers() {

        viewModel.getVerifiedStatus().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    Toast.makeText(getActivity(), "OTP is verified", Toast.LENGTH_SHORT).show();

                    Log.d(TAG, "onChanged: OTP VERIFIED");

                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right);

                    transaction.replace(R.id.frame_main, new SelectionFragment()).commit();
                } else {
                    Log.d(TAG, "onChanged: OTP IS NOT VERIFIED");


                }
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_verify)
    public void onViewClicked() {


        Log.d(TAG, "onViewClicked: Verified Button CLicked");


        String sid = viewModel.getSession_id();

        Log.d(TAG, "onViewClicked: OTP FRAGMENT: sid : " + sid);

        String otp = otpEt.getText().toString();

        if (otp.length() < 6) {
            Toast.makeText(getActivity(), "Invalid OTP length", Toast.LENGTH_SHORT).show();
            return;
        }


        viewModel.verifyOtp(sid, otp);


    }

    @OnClick(R.id.btn_resend_otp)
    public void onButtonClicked() {

        initFrag(new PhonenoFragment());

        Toast.makeText(getActivity(), "Enter your phone no again", Toast.LENGTH_SHORT).show();

    }



    public void initFrag(Fragment fragment){
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.replace(R.id.frame_main, fragment).commit();
    }
}
