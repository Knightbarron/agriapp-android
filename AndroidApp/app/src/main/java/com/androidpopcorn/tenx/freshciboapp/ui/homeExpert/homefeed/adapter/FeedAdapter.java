package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;
import com.androidpopcorn.tenx.freshciboapp.Data.model.feed.GetFeedResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {


    private Context mCtx;
    private ArrayList<GetFeedResponse.FeedData> mList;


    @Inject
    public FeedAdapter(@ActivityContext Context mCtx) {
        this.mCtx = mCtx;
        mList = new ArrayList<>();
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mCtx).inflate(R.layout.listitem_home_items_image, viewGroup, false);

        return new FeedViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder vh, int i) {
        GetFeedResponse.FeedData currentFeed = mList.get(i);
        vh.name.setText(currentFeed.getUser_name());
        vh.bodyText.setText(currentFeed.getFeed_content());



        if(currentFeed.getUser_image() != null){
            if(!TextUtils.isEmpty(currentFeed.getUser_image())){

                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_person_24dp)
                        .error(R.drawable.ic_person_24dp)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH);


                String url = ApiService.BASE_URL + "/"+currentFeed.getUser_image();
                Glide.with(mCtx).load(url).apply(options).into(vh.circularImage);
            }
        }

        if(currentFeed.getFeed_image_path() != null){
            if(!TextUtils.isEmpty(currentFeed.getFeed_image_path())){

                String url = ApiService.BASE_URL + "/"+currentFeed.getFeed_image_path();
                Glide.with(mCtx).load(url).into(vh.bodyImage);
            }
        }



    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.circular_image)
        CircleImageView circularImage;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.body_text)
        TextView bodyText;
        @BindView(R.id.body_image)
        ImageView bodyImage;
        @BindView(R.id.share)
        ImageView share;
        @BindView(R.id.comment)
        ImageView comment;
        @BindView(R.id.react)
        ImageView react;


        public FeedViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void updateList(ArrayList<GetFeedResponse.FeedData> newlist){
        mList.clear();
        mList.addAll(newlist);
        notifyDataSetChanged();
    }
}
