package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.answers.BusinessAnswersFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.base.BusinessBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCategory.BusinessCategoryFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCompleteProfile.BusinessCompleteProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.businessCreatePost.BusinessCreatePostFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.homefeed.BusinessHomeFeedFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.notifications.BusinessNotificationsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.profile.BusinessProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness.uploadDocuments.BusinessUploadDocumentsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.answers.ExpertAnswersFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.base.ExpertBaseFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.ExpertHomeFeed;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.notifications.ExpertNotificationsFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.profile.ExpertProfileFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.settings.SettingsActivity;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;

public class HomeBusinessActivity extends AppCompatActivity implements HasSupportFragmentInjector {


    private static final String TAG = "HomeBusinessActivity";

    @Inject
    HomeBusinessViewModel viewModel;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;


    @Inject
    BusinessAnswersFragment businessAnswersFragment;

    @Inject
    BusinessBaseFragment businessBaseFragment;

    @Inject
    BusinessHomeFeedFragment businessHomeFeedFragment;

    @Inject
    BusinessNotificationsFragment businessNotificationsFragment;

    @Inject
    BusinessProfileFragment businessProfileFragment;

    @Inject
    BusinessCategoryFragment businessCategoryFragment;

    @Inject
            BusinessCompleteProfileFragment businessCompleteProfileFragment;

    Toolbar toolbar;


    @Inject
    FragmentManager fm;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:
                    initializeFragments(fm,businessBaseFragment);
                    setToolBar(R.drawable.agspert_logo_regular);
                    return true;
                case R.id.navigation_notifications:
                    initializeFragments(fm,businessHomeFeedFragment);
                    setToolBar(R.drawable.feedpage_logo);
                    return true;
                case R.id.navigation_base:
                    initializeFragments(fm,businessNotificationsFragment);
                    setToolBar(R.drawable.notifications_logo);
                    return true;
                case R.id.navigation_my_answers:
                    initializeFragments(fm,businessAnswersFragment);


                    return true;
                case R.id.navigation_my_profile:
                    initializeFragments(fm,businessProfileFragment);
                    setToolBar(R.drawable.my_profile_logo);
                    return true;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidInjection.inject(this);

        setContentView(R.layout.activity_home_business);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        changeIconSize(navView);

        ImageView ic = findViewById(R.id.ig_icon);
        ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeBusinessActivity.this, SettingsActivity.class);
                intent.putExtra("CustomerType","Business");
                startActivity(intent);
            }
        });


        if(viewModel == null){
            Log.d(TAG, "onCreate: VIEW MODEL IS NULL");
        }else {
            Log.d(TAG, "onCreate: VIEW MODEL IS NOT NULL");
        }


        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setToolBar(R.drawable.agspert_logo_regular);

        initializeFragments(fm,businessCompleteProfileFragment);

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void initializeFragments(FragmentManager fm, Fragment frag){
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, frag);
        ft.commit();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    private void changeIconSize(BottomNavigationView navView) {

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
        final View iconView = menuView.getChildAt(2).findViewById(android.support.design.R.id.icon);
        final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
        final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, displayMetrics);
        layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, displayMetrics);
        iconView.setLayoutParams(layoutParams);

    }

    private void setToolBar(int drawable){
        getSupportActionBar().setIcon(drawable);
        getSupportActionBar().setTitle("");
    }

}
