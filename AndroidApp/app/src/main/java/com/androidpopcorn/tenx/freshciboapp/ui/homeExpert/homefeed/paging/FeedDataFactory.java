package com.androidpopcorn.tenx.freshciboapp.ui.homeExpert.homefeed.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;

public class FeedDataFactory extends DataSource.Factory{


    private MutableLiveData<FeedDataSource> feedDataSourceMutableLiveData;
    private FeedDataSource  feedDataSource;
    private AppDataManager appDataManager;

    public FeedDataFactory(AppDataManager appDataManager) {
        this.appDataManager = appDataManager;
        this.feedDataSourceMutableLiveData = new MutableLiveData<>();

    }

    @Override
    public DataSource create() {
        feedDataSource = new FeedDataSource(appDataManager);
        feedDataSourceMutableLiveData.postValue(feedDataSource);
        return feedDataSource;
    }

    public MutableLiveData<FeedDataSource> getFeedDataSourceMutableLiveData() {
        return feedDataSourceMutableLiveData;
    }
}
