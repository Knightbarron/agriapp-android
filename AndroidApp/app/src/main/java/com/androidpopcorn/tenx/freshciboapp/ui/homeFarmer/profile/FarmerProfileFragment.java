package com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.profile;


import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidpopcorn.tenx.freshciboapp.Data.api.ApiService;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCategoryResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerCompleteProfileBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceBody;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProduceResponse;
import com.androidpopcorn.tenx.freshciboapp.Data.model.farmer.FarmerProfileResponse;
import com.androidpopcorn.tenx.freshciboapp.R;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.HomeFarmerViewModel;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.farmerCategorySelection.FarmerCategoryFragment;
import com.androidpopcorn.tenx.freshciboapp.ui.homeFarmer.profile.adapter.RecyclerViewAdapter;
import com.androidpopcorn.tenx.freshciboapp.utils.FileUtil;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FarmerProfileFragment extends Fragment {


    @Inject
    HomeFarmerViewModel viewModel;

    public static final int RESULT_CODE_IMAGE = 1022;

    File imageFile = null;

    private static final String TAG = "FarmerProfileFragment";
    @BindView(R.id.et_bio)
    TextView etBio;
    @BindView(R.id.add_new_package)
    Button addNewPackage;
    Unbinder unbinder;
    String farmer_bio;

    String quantityTypeTv;
    String produceCategoryTv;
    String harvestingTimeTv;
    String landCoveredTv;
    String ExpectedProduceTv;

    CircleImageView civDemo;

    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.agspert_certified)
    ImageView agspertCertified;
    @BindView(R.id.profile_name)
    TextView profileName;
    @BindView(R.id.profile_specialization)
    TextView profileSpecialization;
    @BindView(R.id.profile_specialization_tv)
    TextView farmerLocation;
    @BindView(R.id.profile_workplace)
    TextView profileWorkplace;
    @BindView(R.id.profile_workplace_tv)
    TextView profileWorkplaceTv;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.tv3)
    TextView tv3;

    @BindView(R.id.category_fragment)
    TextView toCategoryFragment;

    @Inject
    RecyclerViewAdapter adapter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    FarmerProfileResponse.Farmer farmerData = null;

    ArrayList<String> positionForProduce = new ArrayList<>();

    List<FarmerProduceResponse.FarmerProducePackBody> mList = new ArrayList<>();

    @Inject
    FarmerCategoryFragment farmerCategoryFragment;


//ALert Dialog widgets

    EditText productProducePrice;
    EditText productLand;
    EditText productDescription;
    EditText productQuantity;
    EditText productName;
    Spinner harvestingSpinner;
    Spinner expectedProduceSpinner;
    Spinner landSpinner;
    Spinner categorySpinner;
    Spinner quantitySpinner;
    RadioGroup radioGroup;
    int organicType;
    RadioButton ownerButton;
    TextView quantityTv;
    TextView categoryTv;
    TextView harvestingTv;
    TextView landsTv;
    TextView weightTv;
    Button saveBtn;
    ImageView cancelBtn;


    @Inject
    public FarmerProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        viewModel.getProfileData();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_farmer_profile, container, false);

        if (viewModel == null)
            Log.d(TAG, "onCreateView: VIEWMODEL IS NULL");
        else
            Log.d(TAG, "onCreateView: VIEWMODEL IS NOT NULL");
        unbinder = ButterKnife.bind(this, view);

        setUpRecyclerView();


        observerForProfilePicture();
        observerForProfileData();
        observerForProfileProduce();
        observerForProfileFarmCategory();
        observerForPatchProduce();

        onClickFarm();


        return view;
    }

    private void onClickFarm() {

        toCategoryFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String backStateName = farmerCategoryFragment.getClass().toString();
                //Log.d(TAG, "onBtnOtpLoginClicked: " + backStateName);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right);

                transaction.replace(R.id.container_farmer, farmerCategoryFragment);
                transaction.addToBackStack(backStateName);

                transaction.commit();
            }
        });
    }

    private void observerForPatchProduce() {
        viewModel.getStatusPatchProduce().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    Toast.makeText(getActivity(), "Updated", Toast.LENGTH_SHORT).show();
                    observerForProfileProduce();
                } else {
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void observerForProfileFarmCategory() {
        viewModel.getCurrentFarmerCategories().observe(this, new Observer<FarmerCategoryResponse>() {
            @Override
            public void onChanged(@Nullable FarmerCategoryResponse farmerCategoryResponse) {
                Log.d(TAG, "onChanged: Farm Categories" + farmerCategoryResponse.getBody().size());

                List<FarmerCategoryResponse.FarmCategory> mList = farmerCategoryResponse.getBody();

                StringBuilder strAddress = new StringBuilder();

                for (int i = 0; i < mList.size(); i++) {
                    strAddress.append(mList.get(i).getFarmCategory().toString());
                    strAddress.append(" , ");
                }
                strAddress.setLength(strAddress.length() - 2);
                profileWorkplaceTv.setText(strAddress);
                progressBar.setVisibility(View.INVISIBLE);
                updateProfile(farmerData);

            }
        });
    }

    private void setUpRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    private void observerForProfileProduce() {
        viewModel.getCurrentFarmerProduce().observe(this, new Observer<FarmerProduceResponse>() {
            @Override
            public void onChanged(@Nullable FarmerProduceResponse farmerProduceResponse) {
                Log.d(TAG, "onChanged: This is the outpt" + farmerProduceResponse.getBody().size());

                mList = farmerProduceResponse.getBody();

                Log.d(TAG, "onChanged: " + mList.size());

                adapter.updateListItems(mList);

                for (int i = 0; i < mList.size(); i++) {
                    positionForProduce.add(mList.get(i).getFarmer_produce_id());
                }

            }
        });
    }

    public void observerForProfileData() {
        viewModel.getFarmerProfileResponse().observe(this, new Observer<FarmerProfileResponse.Farmer>() {
            @Override
            public void onChanged(@Nullable FarmerProfileResponse.Farmer farmer) {

                farmerData = farmer;
                //updateProfile(farmer);
//                Log.d(TAG, "onChanged: " + farmer.getName());
//                Log.d(TAG, "onChanged: " + farmer.getLocation());
//                Log.d(TAG, "onChanged: " + farmer.getFarmer_id());
                viewModel.retriveCurrentFarmerPackages(farmer.getFarmer_id());
                viewModel.getCurrentFarmerFarmCategory(farmer.getFarmer_id());
            }
        });
    }

    public void updateProfile(FarmerProfileResponse.Farmer data) {

        Log.d(TAG, "updateProfile: BIO OF THE STUDENT::::" + data.getBio());

        if (!TextUtils.isEmpty(data.getName())) {
            profileName.setText(data.getName());
        }
        if (!TextUtils.isEmpty(data.getLocation())) {
            farmerLocation.setText(data.getLocation());
        }
        if (!TextUtils.isEmpty(data.getImage())) {
            loadImageInCircleView(data.getImage());
        }
        if (!TextUtils.isEmpty(data.getBio())) {
            etBio.setText(data.getBio());
        }
        if(data.getOrganicFarm() == 1){
            Log.d(TAG, "updateProfile: DOES ORGANIC FARMING");
            agspertCertified.setVisibility(View.VISIBLE);
        }else {
            Log.d(TAG, "updateProfile: NOT ORGANIC FARMING");
            agspertCertified.setVisibility(View.INVISIBLE);
        }

    }

    private void observerForProfilePicture() {

        viewModel.getStatusUploadPic().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    Log.d(TAG, "onChanged: PRofile pic updated");
                    Toast.makeText(getActivity(), "Profile Picture updated", Toast.LENGTH_SHORT).show();
                } else {
                    //TODO add progressbar
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {

        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.et_bio, R.id.add_new_package})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_bio:
                onClickBio();
                break;
            case R.id.add_new_package:
                onClickPackage();
                break;
        }
    }

    private void onClickPackage() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_farmer_production, null);
        dialogBuilder.setView(dialogView);

        setUpProduceAlertDialog(dialogView);

        //Building the Dialog
        AlertDialog b = dialogBuilder.create();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Done");


                String organic = ownerButton.getText().toString();
                String name = productName.getText().toString();
                String quantity = productQuantity.getText().toString();
                String description = productDescription.getText().toString();
                String land = productLand.getText().toString();
                String price = productProducePrice.getText().toString();

                //need to send produceCategoryTv harvestingTimeTv

                quantity = quantity + " " + quantityTypeTv;
                land = land + " " + landCoveredTv;
                price = price + " " + ExpectedProduceTv;

                Log.d(TAG, "onClick: Name : " + name + "Quantity : " + quantity + "Description : " + description + "Land" +
                        land + "Price : " + price + "ProduceCategory : " + produceCategoryTv + "Harvesting Time : " + harvestingTimeTv + "Organic : " + organic);


                subscribeObserverForProduceDetails(b);

                b.dismiss();

                viewModel.saveProduceDetails(new FarmerProduceBody(name, quantity, produceCategoryTv, description, harvestingTimeTv, land, organic, price));

                //TODO check this . Is it efficient???
                observerForProfileProduce();
                //TODO need to fix Radiogroup problem

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Cancelling");
                b.dismiss();
            }
        });


        b.show();


    }

    private void setUpProduceAlertDialog(View dialogView) {
        productName = dialogView.findViewById(R.id.add_a_product);
        productQuantity = dialogView.findViewById(R.id.add_produce_quantity);
        productDescription = dialogView.findViewById(R.id.produce_description);
        productLand = dialogView.findViewById(R.id.et_land);
        productProducePrice = dialogView.findViewById(R.id.produce_produce);

        quantitySpinner = dialogView.findViewById(R.id.spinner_quantity);
        categorySpinner = dialogView.findViewById(R.id.spinner_category);
        landSpinner = dialogView.findViewById(R.id.spinner_land);
        expectedProduceSpinner = dialogView.findViewById(R.id.spinner_weight);
        harvestingSpinner = dialogView.findViewById(R.id.spinner_harvesting);

        radioGroup = dialogView.findViewById(R.id.radioGroup);
        organicType = radioGroup.getCheckedRadioButtonId();
        ownerButton = dialogView.findViewById(organicType);

        quantityTv = dialogView.findViewById(R.id.quantity_tv);
        categoryTv = dialogView.findViewById(R.id.category_produce);
        harvestingTv = dialogView.findViewById(R.id.harvesting_tv);
        landsTv = dialogView.findViewById(R.id.lands_tv);
        weightTv = dialogView.findViewById(R.id.weight_type);


        saveBtn = dialogView.findViewById(R.id.btn_save);
        cancelBtn = dialogView.findViewById(R.id.imageView);


//Data for prodcue Spinner
        String[] dataProduceQuantity = new String[]{"per month", "per season"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataProduceQuantity);

        quantitySpinner.setAdapter(adapter1);

        quantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                quantityTypeTv = quantitySpinner.getSelectedItem().toString();

                Log.d(TAG, "initSpinners: Qualification : " + quantityTypeTv);

                quantityTv.setText(quantityTypeTv);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//Data for Category Spinner
        String[] dataCategory = new String[]{"Fruits", "Vegetables", "Cash Crops and spices", "Cereals",
                "Cotton and other Fibres", "Edible Oilseeds", "Roots and tubers", "Nuts", "Grams and pulses",
                "Plantation Crops", "daily Farming", "Fish Farming", "Poultry Framing",
                "Sapling and Nursery", "Others"};

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataCategory);
        categorySpinner.setAdapter(adapter2);

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                produceCategoryTv = categorySpinner.getSelectedItem().toString();

                Log.d(TAG, "initSpinners: Qualification : " + produceCategoryTv);

                categoryTv.setText(produceCategoryTv);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//Data for harvesting time
        String[] dataHarvestingTime = new String[]{"Jan-Feb", "Feb-Mar", "Mar-Apr", "Apr-May", "May-June", "June-July", "July-Aug", "Aug-Sep",
                "Sep-Oct", "Oct-Nov", "Nov-Dec", "Dec-Jan"};


        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataHarvestingTime);
        harvestingSpinner.setAdapter(adapter3);

        harvestingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                harvestingTimeTv = harvestingSpinner.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Qualification : " + harvestingTimeTv);
                harvestingTv.setText(harvestingTimeTv);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//Data for lands SPinner
        String[] dataLands = new String[]{"Kattha", "Acre", "Bigha"};
        ArrayAdapter<String> adapter4 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataLands);
        landSpinner.setAdapter(adapter4);

        landSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                landCoveredTv = landSpinner.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Qualification : " + landCoveredTv);
                landsTv.setText(landCoveredTv);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//Data for produce rate
        String[] dataProduceRate = new String[]{"Per kg", "per quintal"};
        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, dataProduceRate);
        expectedProduceSpinner.setAdapter(adapter5);

        expectedProduceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ExpectedProduceTv = expectedProduceSpinner.getSelectedItem().toString();
                Log.d(TAG, "initSpinners: Qualification : " + ExpectedProduceTv);
                weightTv.setText(ExpectedProduceTv);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void subscribeObserverForProduceDetails(AlertDialog b) {
        viewModel.getStatusProduceDetailsAlertDialog().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    Log.d(TAG, "onChanged: Saved");
                    Toast.makeText(getActivity(), "Your Produce details have been saved", Toast.LENGTH_SHORT).show();

                } else {
                    Log.d(TAG, "onChanged: Not Saved");
                    Toast.makeText(getActivity(), "Error saving your data. Please check your network connection or try again", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void onClickBio() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_bio, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        edt.setSingleLine(false);

        dialogBuilder.setTitle("Write something about yourself");

        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                farmer_bio = edt.getText().toString();
                Log.d(TAG, "onClick: " + farmer_bio);

                viewModel.saveCompleteProfileDetails(new FarmerCompleteProfileBody(edt.getText().toString()));

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();


    }

    @OnClick(R.id.profile_pic)
    public void showUpdateProfilePicDialog() {

        Log.d(TAG, "showUpdateProfilePicDialog: Button Clicked");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = this.getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.custom_dialog_update_profileimage, null);
        builder.setView(dialogView);
        AlertDialog b = builder.create();

        Button btnUpload = (Button) dialogView.findViewById(R.id.btn_upload);
        civDemo = dialogView.findViewById(R.id.civ_profile_demo);

        Button btnselectphoto = (Button) dialogView.findViewById(R.id.btn_select_photo);
        btnselectphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchImage();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageFile == null) {
                    Toast.makeText(getActivity(), "Select a image first", Toast.LENGTH_SHORT).show();
                    return;
                }
                b.dismiss();
                viewModel.uploadProfilePicture(imageFile);

            }
        });


        b.show();


    }

    public void loadImageInCircleView(String image_path) {
        Glide.with(getActivity()).load(ApiService.BASE_URL + "/" + image_path).into(profilePic);
    }

    private void searchImage() {
        Dexter.withActivity(getActivity()).withPermission(Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select a photo"), RESULT_CODE_IMAGE);

            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                Toast.makeText(getActivity(), "Enable Storage read permission", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

            }
        }).check();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == RESULT_CODE_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                imageFile = getFile(uri);

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                    if (civDemo != null) {
                        civDemo.setImageBitmap(bitmap);
                    }
                } catch (FileNotFoundException e) {
                    throw new Error("File not found");
                } catch (IOException e) {
                    throw new Error("IO exception");
                }
            }
        }

    }

    public File getFile(Uri uri) {
        try {
            File file = FileUtil.from(getActivity(), uri);
            Log.d("file", "getFile: uri : " + file.getPath() + " file - " + file + " : " + file.exists());

            return file;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case 121:
                Toast.makeText(getActivity(), "Editing", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onContextItemSelected: " + item.getGroupId());
                // Log.d(TAG, "onContextItemSelected: FARMER PRODUCE ID : " + positionForProduce.get(item.getGroupId()));
                patchFarmerProduce(positionForProduce.get(item.getGroupId()), item.getGroupId());


                return true;
//            case 122 :
//                Toast.makeText(getActivity(), "Deleting", Toast.LENGTH_SHORT).show();
//                return true;
            default:
                return super.onContextItemSelected(item);

        }

    }

    private void patchFarmerProduce(String produceId, int groupId) {

        Log.d(TAG, "patchFarmerProduce:  Alert Dialog :::::::" + produceId);

        Log.d(TAG, "patchFarmerProduce: " + groupId);


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_farmer_production, null);
        dialogBuilder.setView(dialogView);


        setUpProduceAlertDialog(dialogView);

        //Building the Dialog
        AlertDialog b = dialogBuilder.create();

        saveBtn.setOnClickListener(v -> {
            Log.d(TAG, "onClick: Done");


            String organic = ownerButton.getText().toString();
            String name = productName.getText().toString();
            String quantity = productQuantity.getText().toString();
            String description = productDescription.getText().toString();
            String land = productLand.getText().toString();
            String price = productProducePrice.getText().toString();

            //need to send produceCategoryTv harvestingTimeTv

            quantity = quantity + " " + quantityTypeTv;
            land = land + " " + landCoveredTv;
            price = price + " " + ExpectedProduceTv;

            Log.d(TAG, "onClick: Name : " + name + "Quantity : " + quantity + "Description : " + description + "Land" +
                    land + "Price : " + price + "ProduceCategory : " + produceCategoryTv + "Harvesting Time : " + harvestingTimeTv + "Organic : " + organic);


            subscribeObserverForProduceDetails(b);

            b.dismiss();

            viewModel.patchFarmerProduce(Integer.parseInt(produceId), new FarmerProduceBody(name, quantity, produceCategoryTv, description, harvestingTimeTv, land, organic, price));

            //TODO check this . Is it efficient???
            observerForProfileProduce();
            //TODO need to fix Radiogroup problem

        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Cancelling");
                b.dismiss();
            }
        });


        b.show();


    }

    @OnClick(R.id.tv3)
    public void onViewClicked() {
        Toast.makeText(getActivity(), "Feature coming soon...", Toast.LENGTH_SHORT).show();
    }
}
