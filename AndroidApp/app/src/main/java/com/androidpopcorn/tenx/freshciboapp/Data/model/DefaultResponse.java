package com.androidpopcorn.tenx.freshciboapp.Data.model;

import android.support.v4.media.session.MediaSessionCompat;

import com.google.gson.annotations.SerializedName;

public class DefaultResponse {
    @SerializedName("content")
    private String message;

//    only for uploads
    @SerializedName("image_path")
    private String imagePath;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
