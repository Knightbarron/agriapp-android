package com.androidpopcorn.tenx.freshciboapp.Data.prefs;

public interface PreferencesHelper {
    String getAccessToken();

    void setAccessToken(String token);

    void setEmail(String email);

    String getEmail();

    String getTypeUser();

    void setTypeUser(String userType);


}
