package com.androidpopcorn.tenx.freshciboapp.ui.homeBusiness;

import android.app.Application;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;

import com.androidpopcorn.tenx.freshciboapp.Data.AppDataManager;
import com.androidpopcorn.tenx.freshciboapp.base.ViewModelFactory;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityContext;
import com.androidpopcorn.tenx.freshciboapp.di.scope.ActivityScope;
import dagger.Module;
import dagger.Provides;


@Module
public class HomeBusinessModule {

    @Provides
    @ActivityContext
    @ActivityScope
    Context provideContext(HomeBusinessActivity homeBusinessActivity){
        return homeBusinessActivity;
    }


    @Provides
    @ActivityScope
    HomeBusinessViewModel provideVM(@ActivityContext Context context, Application application, AppDataManager appDataManager){

        HomeBusinessViewModel vm = new HomeBusinessViewModel(appDataManager, application);
        ViewModelFactory<HomeBusinessViewModel> factory = new ViewModelFactory<>(vm,appDataManager,application);
        return ViewModelProviders.of((HomeBusinessActivity) context, factory).get(HomeBusinessViewModel.class);

    }

}
